// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  siteURL: 'http://localhost:3000'
  // siteURL: 'http://54.71.18.74:4575'
  // siteURL: "http://3.15.112.60:3000"
  // siteURL: "https://hayatech.co/api"
};
