import * as Chartist from 'chartist';
import { ChartEvent, ChartType } from 'ng-chartist';


export interface Chart {
    type: ChartType;
    data: Chartist.IChartistData;
    options?: any;
    responsiveOptions?: any;
    events?: ChartEvent;
}

// Widget Line Chart
export let WidgetlineChart1: Chart = {
    type: 'Line',
    data: {
      series: [
        [25, 50, 30, 40, 60, 21, 20, 10, 4, 13,0, 10, 30, 40, 10, 15, 20]
      ]
    },
    options: {
        axisX: {
            showGrid: false,
            showLabel: false,
            offset: 0,
        },
        axisY: {
            showGrid: false,
            low: 0,
            showLabel: false,
            offset: 0,
        },
        chartPadding: {
            right: 0,
            left: 0,
            bottom: 0,
          },
        lineSmooth: Chartist.Interpolation.cardinal({
            tension: 0
        }),
        showArea: true,
        fullWidth: true,
        height:60,
        showPoint: false,
    },
  };

  // Widget Line Chart
  export let WidgetlineChart2: Chart = {
    type: 'Line',
    data: {
      series: [
        [1, 2, 3],
        [5, 10, 20, 14, 17, 21, 20, 10, 4, 13,0, 10, 30, 40, 10, 15, 20]
      ]
    },
    options: {
        axisX: {
            showGrid: false,
            showLabel: false,
            offset: 0,
        },
        axisY: {
            showGrid: false,
            low: 0,
            showLabel: false,
            offset: 0,
        },
        chartPadding: {
            right: 0,
            left: 0,
            bottom: 0,
          },
        lineSmooth: Chartist.Interpolation.cardinal({
            tension: 0
        }),
        showArea: true,
        fullWidth: true,
        height:60,
        showPoint: false,
    },
  };

  // Widget Line Chart
  export let  WidgetlineChart3: Chart = {
    type: 'Line',
    data: {
    series: [
      [null],
      [null],
      [5, 10, 20, 14, 17, 21, 20, 10, 4, 13,0, 10, 30, 40, 10, 15, 20]
      ]
    },
    options: {
        axisX: {
            showGrid: false,
            showLabel: false,
            offset: 0,
        },
        axisY: {
            showGrid: false,
            low: 0,
            showLabel: false,
            offset: 0,
        },
        chartPadding: {
            right: 0,
            left: 0,
            bottom: 0,
          },
        lineSmooth: Chartist.Interpolation.cardinal({
            tension: 0
        }),
        showArea: true,
        fullWidth: true,
        height:60,
        showPoint: false,
    },
 };

 // Widget Line Chart
 export let WidgetlineChart4: Chart = {
    type: 'Line',
    data: {
        series: [
          [null],
          [null],
          [null],
          [5, 10, 20, 14, 17, 21, 20, 10, 4, 13,0, 10, 30, 40, 10, 15, 20]
        ]
    },
    options: {
        axisX: {
            showGrid: false,
            showLabel: false,
            offset: 0,
        },
        axisY: {
            showGrid: false,
            low: 0,
            showLabel: false,
            offset: 0,
        },
        chartPadding: {
            right: 0,
            left: 0,
            bottom: 0,
        },
        lineSmooth: Chartist.Interpolation.cardinal({
            tension: 0
        }),
        showArea: true,
        fullWidth: true,
        height:60,
        showPoint: false,
    }
 };

 // Sales Chart
 export const salesChart: Chart = {
  type: 'Line',
  data: {
    labels: ['Q1', 'Q2', 'Q3', 'Q4', 'Q5', 'Q6', 'Q7', 'Q8', 'Q9', 'Q10', 'Q11', 'Q13', 'Q14'],
    series: [
      [4, 6, 8, 6, 8, 30, 25, 24, 27, 8, 18, 12, 8, 40, 33, 37, 25, 22, 18, 25, 2, 4, 15, 10, 11, 15, 1]
      // [24, 27, 8, 18, 12, 8, 40, 33, 37, 25, 22, 18, 25, 2, 4, 15, 10, 11, 15, 14, 6, 8, 6, 8, 30, 25],
    ]
  },
  options: {
    axisX: {
      showGrid: false,
      showLabel: false,
      offset: 0,
    },
    axisY: {
      showGrid: false,
      low: 0,
      showLabel: false,
      offset: 0,
    },
    chartPadding: {
      top: 30,
      bottom: 0,
      right: 0,
      left: 0,
    },
    lineSmooth: Chartist.Interpolation.cardinal({
      tension: 0
    }),
    showArea: true,
    fullWidth: true,
    height: 320,
    showPoint: false,
  }
};

  // Doughnut Chart (Monthlt visitor chart)
  export let monthlyChartLabels: string[] = ['Marketing', 'Sales', 'Human Resource', 'Technical Support'];
  export let monthlyChartData: number[] = [500, 600, 400, 700];
  export let monthlyChartColors: any[] = [{ backgroundColor: ['#ccbbef', '#80dde9', '#68dabe', '#4099ffa3'] }];
  export let monthlyChartType = 'doughnut';
  export let monthlyChartLegend = true;
  export let monthlyChartOptions: any = {
    animation: false,
    responsive: true,
    height: 500,
    maintainAspectRatio: false,
    legend: { position: 'right' }
  };

// Doughnut Chart (Daily visitor chart)
export let dailyChartLabels: string[] = ['India', 'USA', 'Canada', 'UK'];
export let dailyChartData: number[] = [800, 500, 200, 300];
export let dailyChartColors: any[] = [{ backgroundColor: ['#ccbbef', '#80dde9', '#68dabe', '#4099ffa3'] }];
export let dailyChartType = 'doughnut';
export let dailyChartLegend = true;
export let dailyChartOptions: any = {
  animation: false,
  responsive: true,
  height: 500,
  maintainAspectRatio: false,
  legend: { position: 'right' }
};


// LineChart (Top Sale)
export let saleChartData: Array<any> = [
  { data: [10, 20, 40, 30, 50, 20, 10, 30, 10, 9, 11, 12] },
  { data: [30, 50, 20, 30, 50, 20, 60, 25, 30, 55, 66, 42] },
];

export let saleChartLabels: Array<any> = ['Jan', 'Feb', 'March', 'April', 'May', 'June', 'July', 'August', 'Sept', 'Oct', 'Nov', 'Dec'];
export let saleChartColors: Array<any> = [
  {
    backgroundColor: 'rgba(171, 140, 228, 0.2)',
    borderColor: '#ab8ce4',
    borderWidth: 2,
    lineTension: 0,
  },
  {
    backgroundColor: 'rgba(38, 198, 218, 0.2)',
    borderColor: '#26c6da',
    borderWidth: 2,
    lineTension: 0,
  },
  {
    backgroundColor: 'rgb(64, 153, 255, 0.2)',
    borderColor: '#4099ff',
    borderWidth: 2,
    lineTension: 0,
  },
  {
    backgroundColor: 'rgb(64, 153, 255, 0.2)',
    borderColor: '#80dde9',
    borderWidth: 2,
    lineTension: 0,
  }
];
export let saleChartLegend = false;
export let saleChartType = 'bar';
export let saleChartOptions: any = {
  responsive: true,
  scaleShowVerticalLines: false,
  maintainAspectRatio: false,
};

