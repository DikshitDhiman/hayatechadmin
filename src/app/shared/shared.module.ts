import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { FooterComponent } from './footer/footer.component';
import { ChatSidebarComponent } from './chat-sidebar/chat-sidebar.component';
import { ToggleFullscreenDirective } from './directives/fullscreen.directive';
import { LoaderComponent } from './components/loader/loader.component';
import { TableComponent } from './components/table/table.component';

@NgModule({
  declarations: [
    LoaderComponent,
    FooterComponent,
    ChatSidebarComponent,
    ToggleFullscreenDirective,
    TableComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    TranslateModule,
    FormsModule,
    NgbModule
  ],
  exports: [
    LoaderComponent,
    TableComponent,
    TranslateModule,
    FooterComponent,
    ChatSidebarComponent,
    ToggleFullscreenDirective
  ],
  providers: []
})
export class SharedModule { }
