import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as chartEcomData from '../../../shared/data/deshboard/ecommerce';
import { ChartEvent, ChartType } from 'ng-chartist';

import { ToastrService } from 'ngx-toastr';
import { AppServicesService } from './../../../shared/service/app-services.service';
import { constants } from '../../../constants/const';

export interface Chart {
  type: ChartType;
  options?: any;
  responsiveOptions?: any;
  events?: ChartEvent;
}

@Component({
  selector: 'app-vendor-stats',
  templateUrl: './vendor-stats.component.html',
  styleUrls: ['./vendor-stats.component.scss']
})
export class VendorStatsComponent implements OnInit {

  featuredProducts: any = [];
  popularChallenges: any = [];
  messageHandler: any = constants.constMessages;
  currentUser: any;
  userWishlisted: any;
  isLoading: Boolean = false;

  adminStats: any = null;
  vendorData: any = {
    totalRedeemedCounter: {
      total_redeemed: 0
    },
    totalTokenCounter: {
      total_token: 0
    },
    vendorTotalProduct: {
      total_product: 0
    }
  };

  public WidgetLineChart1 = chartEcomData.WidgetLineChart1;
  public WidgetLineChart2 = chartEcomData.WidgetLineChart2;

  public totalActiveChallengeChart = chartEcomData.WidgetLineChart2;
  public totalActiveSurveyChart  = chartEcomData.WidgetLineChart2;

  constructor(
    private apiService: AppServicesService,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    this.WidgetLineChart1.data.series = [[1, 7, 8, 9, ]];
    this.WidgetLineChart2.data.series = [[12, 7, 3, 18]];
    this.currentUser = JSON.parse(localStorage.getItem('user'));
    const reqData = {
      role: this.currentUser.role,
      roleId: this.currentUser._id
    };
    this.featuredProductsStatus(reqData);
    this.sponsoredChallenges(reqData);
    this.getAdminStats(reqData);
    this.getVendorsData();
    this.getWishlistedParticipants(reqData);
    this.getTotalActiveChallengeChart('month');
    this.gettotalActiveSurveyChart('month');
  }

  getVendorsData() {
    const reqObj = { vendorId: this.currentUser._id }
    this.apiService.post('/api/v1/stat/vendor/vendor-dashboard', reqObj).subscribe((response: any) => {
      if (response.data && response.status === 200) {
        this.vendorData = response.data;
        console.log(' this.vendorData ', this.vendorData)
      } else {
        this.toastr.error(this.messageHandler[response.message], 'Error');
      }
    }, (errorResult) => {
      this.errorHandling(errorResult);
    });
  }

  getAdminStats(data: any) {
    this.apiService.post('/api/v1/stat/vendor', data).subscribe((response: any) => {
      if (response.data && response.status === 200) {
        this.adminStats = response.data;
        console.log("this.adminStats", this.adminStats)
      } else {
        this.toastr.error(this.messageHandler[response.message], 'Error');
      }
    }, (errorResult) => {
      this.errorHandling(errorResult);
    });
  }

  featuredProductsStatus(data: any) {
    this.apiService.post('/api/v1/stat/vendor/featuredProducts', data).subscribe((response: any) => {
      if (response.data && response.status === 200) {
        this.featuredProducts = response.data;
      } else {
        this.toastr.error(this.messageHandler[response.message], 'Error');
      }
    }, (errorResult) => {
      this.errorHandling(errorResult);
    });
  }

  sponsoredChallenges(data: any) {
    this.apiService.post('/api/v1/stat/vendor/getPopularChallenge', data).subscribe((response: any) => {
      if (response.data && response.status === 200) {
        this.popularChallenges = response.data;
      } else {
        this.toastr.error(this.messageHandler[response.message], 'Error');
      }
    }, (errorResult) => {
      this.errorHandling(errorResult);
    });
  }

  getWishlistedParticipants(data) {
    this.apiService.post('/api/v1/stat/vendor/wishlistedUsers', data).subscribe( (response: any) => {
      console.log("esponse.data : ", response.data);
      if (response.status === 200) {
        console.log("userWishlisted : ", response.data);
        this.userWishlisted = response.data;
      } else {
        this.toastr.error(this.messageHandler[response.message], 'Error');
      }
    }, (errorResult) => {
      this.errorHandling(errorResult);
    });
  }

  getTotalActiveChallengeChart(recordType) {
    const reqObj = { recordType: recordType };
    this.apiService.post('/api/v1/stat/admin/totalActiveChallengeChart', reqObj).subscribe((response: any) => {
      if (response.data && response.status === 200) {
        this.totalActiveChallengeChart =  {
          ...this.totalActiveChallengeChart,
          data: {
            series: [response.data]
          }
        }
      } else {
        this.toastr.error(this.messageHandler[response.message], 'Error');
      }
    }, (errorResult) => {
      this.errorHandling(errorResult);
    });
  }

  gettotalActiveSurveyChart(recordType) {
    const reqObj = { recordType: recordType };
    this.apiService.post('/api/v1/stat/admin/totalActiveSurveyChart', reqObj).subscribe((response: any) => {
      if (response.data && response.status === 200) {
        this.totalActiveSurveyChart =  {
          ...this.totalActiveSurveyChart,
          data: {
            series: [response.data]
          }
        }
      } else {
        this.toastr.error(this.messageHandler[response.message], 'Error');
      }
    }, (errorResult) => {
      this.errorHandling(errorResult);
    });
  }

  errorHandling(error) {
    try {
      this.isLoading = false;
      const errorObj = error ? JSON.parse(error) : '';
      this.toastr.error(errorObj.message, 'Error');
    } catch (error) {
      this.toastr.error(error.message, 'Error');
    }
  }
}
