import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as chartDashData from '../../data/deshboard/default';
import * as chartEcomData from '../../../shared/data/deshboard/ecommerce';
import * as chartCrmData from '../../../shared/data/deshboard/crm';
import * as Chartist from 'chartist';
import { ToastrService } from 'ngx-toastr';
import { AppServicesService } from './../../../shared/service/app-services.service';
import { ChartEvent, ChartType } from 'ng-chartist';
import { constants } from '../../../constants/const';


export interface Chart {
  type: ChartType;
  data: Chartist.IChartistData;
  options?: any;
  responsiveOptions?: any;
  events?: ChartEvent;
}

@Component({
  selector: 'app-admin-stats',
  templateUrl: './admin-stats.component.html',
  styleUrls: ['./admin-stats.component.scss']
})
export class AdminStatsComponent implements OnInit {

  currentUser: any;
  popularChallenges: any = [];
  featuredProducts: any = [];
  corporateClients: any = [];
  rewardPartners: any = [];
  totalSteps: any = [];
  adminStats: any = null;
  isLoading: Boolean = false;

  /* Define 8 custom variable for prsenting graph stat on the top */
  public totalParticipantsChart = chartEcomData.WidgetLineChart1;
  public totalStepChart = chartEcomData.WidgetLineChart2;
  public totalActiveChallengeChart = chartEcomData.WidgetLineChart2;
  public totalActiveSurveyChart  = chartEcomData.WidgetLineChart2;
  public totalCorporateClientChart  = chartEcomData.WidgetLineChart2;
  public totalRewarndPartnerChart  = chartEcomData.WidgetLineChart2;
  public totalProductListedChart  = chartEcomData.WidgetLineChart2;
  public totalRedeemedUnitChart  = chartEcomData.WidgetLineChart2;
  /************ Ended define custom variable *********************/

  public chart1 = chartEcomData.chart1;
  public comboChart = chartCrmData.comboChart;
  public salesChart = chartDashData.salesChart;
  public monthlyChartLabels = chartDashData.monthlyChartLabels;
  public monthlyChartData = chartDashData.monthlyChartData;
  public monthlyChartColors = chartDashData.monthlyChartColors;
  public monthlyChartType = chartDashData.monthlyChartType;
  public monthlyChartLegend = chartDashData.monthlyChartLegend;
  public monthlyChartOptions = chartDashData.monthlyChartOptions;

  public dailyChartLabels = chartDashData.dailyChartLabels;
  public dailyChartData = chartDashData.dailyChartData;
  public dailyChartColors = chartDashData.dailyChartColors;
  public dailyChartType = chartDashData.dailyChartType;
  public dailyChartLegend = chartDashData.dailyChartLegend;
  public dailyChartOptions = chartDashData.dailyChartOptions;

  public saleChartData = chartDashData.saleChartData;
  public saleChartLabels = chartDashData.saleChartLabels;
  public saleChartColors = chartDashData.saleChartColors;
  public saleChartLegend = chartDashData.saleChartLegend;
  public saleChartType = chartDashData.saleChartType;
  public saleChartOptions = chartDashData.saleChartOptions;

  messageHandler: any = constants.constMessages;
  monthLabels: any = ['M1', 'M2', 'M3', 'M4', 'M5', 'M6', 'M7', 'M8', 'M9', 'M10', 'M11', 'M12'];
  weekLabels: any = ['W1', 'W2', 'W3', 'W4', 'W5', 'W6', 'W7', 'W8', 'W9', 'W10', 'W11', 'W12'];
  labels: any = this.monthLabels;
  weekLabel = [];
  weekly = 15;

  public owlcarouselOptions = {
    loop: true,
    margin: 10,
    items: 1,
    nav: false,
    dots: false
  };

  public owlcarousel = [
    {
      desc: 'I  must explain to you how all this mistaken idea of denouncing pleasure.',
      img: 'assets/images/dashboard/boy-2.png',
      name: 'Mark Jecno',
    },
    {
      desc: 'I  must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born.',
      img: 'assets/images/dashboard/boy-2.png',
      name: 'Mark Jecno',
    }
  ];

  // public salesCrmChart = chartCrmData.salesChart;
  public salesCrmChart = {
      type: 'Bar',
      data: {
          labels: this.monthLabels,
          series: []
       },
       options: {
         stackBars: true,
           axisY: {
              labelInterpolationFnc: function(value) {
                  return (value / 1000) + 'k';
              }
          },
          height: 300,
       }
  }

  public usersChart = {
    type: 'Line',
    data: {
      labels: this.monthLabels,
      series: []
    },
    options: {
      height: 300,
      fullWidth: true,
      chartPadding: {
        right: 40
      }
    },
  };
  constructor(
    private apiService: AppServicesService,
    private toastr: ToastrService
  ) {
    for (let i = 1; i < this.weekly; i++) { this.weekLabel.push('W' + i); }
  }

  ngOnInit() {
    this.currentUser = JSON.parse(localStorage.getItem('user'));
    this.getAdminStats();
    this.getPopularChallenge();
    this.featuredProductsStatus();
    this.corporateClientStatus();
    this.rewardPartnerStatus();
    this.getTotalStepsChart('month');
    this.getTotalUsersChart('month');
    this.getTotalActiveChallengeChart('month');
    this.gettotalRedeemedUnitChart('month');
    this.gettotalActiveSurveyChart('month');
    this.gettotalCorporateClientChart('month');
    this.gettotalProductListedChart('month');
    this.gettotalRewardPartnerChart('month');
  }


  gettotalRewardPartnerChart(recordType) {
    const reqObj = { recordType: recordType };
    this.apiService.post('/api/v1/stat/admin/totalRewardPartnerChart', reqObj).subscribe((response: any) => {
      if (response.data && response.status === 200) {
        this.totalRewarndPartnerChart =  {
          ...this.totalRewarndPartnerChart,
          data: {
            series: [response.data]
          }
        }
      } else {
        this.toastr.error(this.messageHandler[response.message], 'Error');
      }
    }, (errorResult) => {
      this.errorHandling(errorResult);
    });
  }

  gettotalProductListedChart(recordType) {
    const reqObj = { recordType: recordType };
    this.apiService.post('/api/v1/stat/admin/totalProductListedChart', reqObj).subscribe((response: any) => {
      if (response.data && response.status === 200) {
        this.totalProductListedChart =  {
          ...this.totalProductListedChart,
          data: {
            series: [response.data]
          }
        }
      } else {
        this.toastr.error(this.messageHandler[response.message], 'Error');
      }
    }, (errorResult) => {
      this.errorHandling(errorResult);
    });
  }

  gettotalCorporateClientChart(recordType) {
    const reqObj = { recordType: recordType };
    this.apiService.post('/api/v1/stat/admin/totalCorporateClientChart', reqObj).subscribe((response: any) => {
      if (response.data && response.status === 200) {
        this.totalCorporateClientChart =  {
          ...this.totalCorporateClientChart,
          data: {
            series: [response.data]
          }
        }
      } else {
        this.toastr.error(this.messageHandler[response.message], 'Error');
      }
    }, (errorResult) => {
      this.errorHandling(errorResult);
    });
  }

  gettotalActiveSurveyChart(recordType) {
    const reqObj = { recordType: recordType };
    this.apiService.post('/api/v1/stat/admin/totalActiveSurveyChart', reqObj).subscribe((response: any) => {
      if (response.data && response.status === 200) {
        this.totalActiveSurveyChart =  {
          ...this.totalActiveSurveyChart,
          data: {
            series: [response.data]
          }
        }
      } else {
        this.toastr.error(this.messageHandler[response.message], 'Error');
      }
    }, (errorResult) => {
      this.errorHandling(errorResult);
    });
  }

  gettotalRedeemedUnitChart(recordType) {
    const reqObj = { recordType: recordType };
    this.apiService.post('/api/v1/stat/admin/totalRedeemedPointChart', reqObj).subscribe((response: any) => {
      if (response.data && response.status === 200) {
        this.totalRedeemedUnitChart =  {
          ...this.totalRedeemedUnitChart,
          data: {
            series: [response.data]
          }
        }
      } else {
        this.toastr.error(this.messageHandler[response.message], 'Error');
      }
    }, (errorResult) => {
      this.errorHandling(errorResult);
    });
  }

  getTotalActiveChallengeChart(recordType) {
    const reqObj = { recordType: recordType };
    this.apiService.post('/api/v1/stat/admin/totalActiveChallengeChart', reqObj).subscribe((response: any) => {
      if (response.data && response.status === 200) {
        this.totalActiveChallengeChart =  {
          ...this.totalActiveChallengeChart,
          data: {
            series: [response.data]
          }
        }
      } else {
        this.toastr.error(this.messageHandler[response.message], 'Error');
      }
    }, (errorResult) => {
      this.errorHandling(errorResult);
    });
  }


  /* Get all challenge
  * Later it will come according to the corporate role.
  */
  getPopularChallenge() {
    const reqData = {
      role: this.currentUser.role,
      roleId: this.currentUser._id
    };
    this.apiService.post('/api/v1/stat/admin/getPopularChallenge', reqData).subscribe((response: any) => {
      if (response.data && response.status === 200) {
        this.popularChallenges = response.data;
      } else {
        this.toastr.error(this.messageHandler[response.message], 'Error');
      }
    }, (errorResult) => {
      this.errorHandling(errorResult);
    });
  }

  getAdminStats() {
    this.apiService.fetch('/api/v1/stat/admin').subscribe((response: any) => {
      if (response.data && response.status === 200) {
        this.adminStats = response.data;
        console.log(' this.adminStats ', this.adminStats)
      } else {
        this.toastr.error(this.messageHandler[response.message], 'Error');
      }
    }, (errorResult) => {
      this.errorHandling(errorResult);
    });
  }

  corporateClientStatus() {
    this.apiService.fetch('/api/v1/stat/admin/corporateStatus').subscribe((response: any) => {
      if (response.data && response.status === 200) {
        this.corporateClients = response.data;
      } else {
        this.toastr.error(this.messageHandler[response.message], 'Error');
      }
    }, (errorResult) => {
      this.errorHandling(errorResult);
    });
  }

  rewardPartnerStatus() {
    this.apiService.fetch('/api/v1/stat/admin/rewardPartnersStatus').subscribe((response: any) => {
      if (response.data && response.status === 200) {
        this.rewardPartners = response.data;
      } else {
        this.toastr.error(this.messageHandler[response.message], 'Error');
      }
    }, (errorResult) => {
      this.errorHandling(errorResult);
    });
  }

  featuredProductsStatus() {
    this.apiService.fetch('/api/v1/stat/admin/featuredProducts').subscribe((response: any) => {
      if (response.data && response.status === 200) {
        this.featuredProducts = response.data;
      } else {
        this.toastr.error(this.messageHandler[response.message], 'Error');
      }
    }, (errorResult) => {
      this.errorHandling(errorResult);
    });
  }


  selectSteps(type, option) {
    if (type === 'total_users') {
      if (option === 'month') {
        this.usersChart.data = {labels: this.monthLabels, series: []};
      } else {
        this.usersChart.data = {labels: this.weekLabel, series: []};
      }
      this.getTotalUsersChart(option);
    } else {
      if (option === 'month') {
        this.salesCrmChart.data = {labels: this.monthLabels, series: []};
      } else {
        this.salesCrmChart.data = {labels: this.weekLabel, series: []};
      }
      this.getTotalStepsChart(option);
    }
  }

  getTotalStepsChart(type) {
    const monthLabel = [];
    const weekLabel = [];
    const reqData = {
      recordType: type
    };
    this.apiService.post('/api/v1/stat/admin/totalSteps', reqData).subscribe((response: any) => {
      if (response.data && response.status === 200) {
        this.totalSteps = response.data;
        console.log(' this.totalSteps ', this.totalSteps)
        /*Show total participants graph*/
        this.totalStepChart = {
          ...this.totalStepChart,
          data: {
            series: [this.totalSteps.steps]
          }
        };
        let chlddata = [];
        if (type === 'month') {
          if (this.totalSteps.tyepDataMonth) {
            chlddata = this.totalSteps.tyepDataMonth.reduce((acc, value, index) => {
              acc.push({month: value, steps: this.totalSteps.steps[index], year: this.totalSteps.tyepData[index]});
              return acc;
            }, []);
          }
         
          const fillOtherMonth =  this.fillOtherMonthSteps(chlddata);
          this.salesCrmChart.data.series = fillOtherMonth;
          for (let m = 0; m < fillOtherMonth[0]['month'].length; m++) {
            var stringText = fillOtherMonth[0]['month'][m].toString();
            var splitText = stringText.split("/");
           
            var labelText = splitText[1].slice(2);
              monthLabel.push(splitText[0] + '/' + labelText)
          }
         
          this.salesCrmChart.data.labels = monthLabel;
        } else {
          chlddata = this.totalSteps.tyepDataMonth.reduce((acc, value, index) => {
            acc.push({week: value, steps: this.totalSteps.steps[index], year: this.totalSteps.tyepData[index]});
            return acc;
          }, []);

          const fillOtherWeek =  this.fillOtherWeekSteps(chlddata);
          this.salesCrmChart.data.series = fillOtherWeek;
          for (let m = 0; m < fillOtherWeek[0]['week'].length; m++) {
            var stringText = fillOtherWeek[0]['week'][m].toString();
            var splitText = stringText.split("/");
            var labelText = splitText[1].slice(2);
            weekLabel.push(splitText[0] + '/' + labelText)
          }
          this.salesCrmChart.data.labels = weekLabel;
        }
      } else {
        this.toastr.error(this.messageHandler[response.message], 'Error');
      }
    }, (errorResult) => {
      this.errorHandling(errorResult);
    });
  }

  getTotalUsersChart(type) {
    const reqData = {
      adminId: this.currentUser._id,
      recordType: type
    };
    const monthLabel = [];
    const weekLabel = [];
    this.apiService.post('/api/v1/stat/admin/totalUsers', reqData).subscribe((response: any) => {
      if (response.data && response.status === 200) {
        console.log("participants ",response.data)
        /*Show total participants graph*/
        const data = response.data.map( arr => arr._id.month);
        this.totalParticipantsChart.data =  {
          series: [data]
        }
        if (type === 'month') {
          const series = response.data.reduce((acc, arr) => {
            acc.push({ month: arr._id.month, steps: arr.sum, year: arr._id.year });
            return acc;
          }, []);
          const fillOtherMonth = this.fillOtherMonth(series);
          this.usersChart.data.series = fillOtherMonth;
          for (let m = 0; m < fillOtherMonth[0]['month'].length; m++) {
            var stringText = fillOtherMonth[0]['year'][m].toString();
              monthLabel.push(fillOtherMonth[0]['month'][m] + '/' + stringText.slice(-2))
          }
          this.usersChart.data.labels = monthLabel;
          
        } else {
          const series = response.data.reduce((acc, arr) => {
            acc.push({ week: arr._id.week, steps: arr.sum, year: arr._id.year });
            return acc;
          }, []);
          const fillOtherWeek = this.fillOtherWeek(series);
         
          this.usersChart.data.series = fillOtherWeek;
          for (let m = 0; m < fillOtherWeek[0]['week'].length; m++) {
            var stringText = fillOtherWeek[0]['year'][m].toString();
            weekLabel.push(fillOtherWeek[0]['week'][m] + '/' + stringText.slice(-2))
          }
          this.usersChart.data.labels = weekLabel;
        }
        
      } else {
        this.toastr.error(this.messageHandler[response.message], 'Error');
      }
    }, (errorResult) => {
      this.errorHandling(errorResult);
    });
  }

  monthNumToName(monthnum) {
    const months = ['Jan', 'Feb', 'March', 'April', 'May', 'June', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];
    return months[monthnum - 1] || '';
  }

  fillOtherWeekSteps(customMonth) {
    const result = [];
    const customData = [];
    const resultMonth = [];
    const resultYear = [];
    //var yearVal;
    const allWeek = [];
    for (let i = 0; i < this.weekly; i++) {
      allWeek.push(i);
    }

    allWeek.forEach((week, index) => {
      // tslint:disable-next-line: no-shadowed-variable
      const item = customMonth.find(item => item.week === (index + 1));
      //yearVal = item.year;
      if (item) {
        result.push(item.steps);
        resultMonth.push(item.year);
        //resultYear.push(item.year)
      } else {
        result.push((0));
        resultMonth.push((index + 1) + '/2020');
        //resultYear.push((2020))
      }
    });
    customData.push({ data: result, week: resultMonth });
    return customData;
  }

  fillOtherMonthSteps(customMonth) {
    const result = [];
    const resultMonth = [];
    const resultYear = [];
    const customData = [];
    //var yearVal;
    const allMonths = ['Jan', 'Feb', 'March', 'April', 'May', 'June', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];
    allMonths.forEach((month, index) => {
      // tslint:disable-next-line: no-shadowed-variable
      const item = customMonth.find(item => item.month === (index + 1));
      //yearVal = item.year;
      if (item) {
        result.push(item.steps);
        resultMonth.push(item.year);
        //resultYear.push(item.year)
      } else {
        result.push((0));
        resultMonth.push((index + 1) + '/2020');
        //resultYear.push((2020))
      }
    });
    customData.push({ data: result, month: resultMonth });
    return customData;
  }

  fillOtherWeek(customMonth) {
    const result = [];
    const customData = [];
    const resultMonth = [];
    const resultYear = [];
    //var yearVal;
    const allWeek = [];
    for (let i = 0; i < this.weekly; i++) {
      allWeek.push(i);
    }

    allWeek.forEach((week, index) => {
      // tslint:disable-next-line: no-shadowed-variable
      const item = customMonth.find(item => item.week === (index + 1));
      //yearVal = item.year;
      if (item) {
        result.push(item.steps);
        resultMonth.push(item.week);
        resultYear.push(item.year)
      } else {
        result.push((0));
        resultMonth.push((index + 1));
        resultYear.push((2020))
      }
    });
    customData.push({ data: result, week: resultMonth, year: resultYear });
    return customData;
  }

  fillOtherMonth(customMonth) {
    const result = [];
    const resultMonth = [];
    const resultYear = [];
    const customData = [];
    //var yearVal;
    const allMonths = ['Jan', 'Feb', 'March', 'April', 'May', 'June', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];
    allMonths.forEach((month, index) => {
      // tslint:disable-next-line: no-shadowed-variable
      const item = customMonth.find(item => item.month === (index + 1));
      //yearVal = item.year;
      if (item) {
        result.push(item.steps);
        resultMonth.push(item.month);
        resultYear.push(item.year)
      } else {
        result.push((0));
        resultMonth.push((index + 1));
        resultYear.push((2020))
      }
    });
    customData.push({ data: result, month: resultMonth, year: resultYear });
    return customData;
  }

  errorHandling(error) {
    try {
      this.isLoading = false;
      const errorObj = error ? JSON.parse(error) : '';
      this.toastr.error(errorObj.message, 'Error');
    } catch (error) {
      this.toastr.error(error.message, 'Error');
    }
  }

}
