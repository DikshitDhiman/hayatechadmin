import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as chartData from '../../data/deshboard/default';
import * as chartDataCorporate from '../../../shared/data/deshboard/ecommerce';
import * as Chartist from 'chartist';
import { ToastrService } from 'ngx-toastr';
import { ChartEvent, ChartType } from 'ng-chartist';
import { AppServicesService } from './../../../shared/service/app-services.service';
import { constants } from '../../../constants/const';

export interface Chart {
  type: ChartType;
  data: Chartist.IChartistData;
  options?: any;
  responsiveOptions?: any;
  events?: ChartEvent;
}

@Component({
  selector: 'app-corporate-stats',
  templateUrl: './corporate-stats.component.html',
  styleUrls: ['./corporate-stats.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class CorporateStatsComponent implements OnInit {
  maxdate: any = new Date();
  todayYear = this.maxdate.getFullYear();
  startDate: any;
  endDate: any = new Date();
  adminId = '5d2732052c35df2cbe8ae59b';
  adminStats: any = null;
  isLoading: Boolean = false;
  items: any = {
    total_prtcpnt: 1,
    total_steps: 1,
  };
  currentUser: any;
  public owlcarousel = [
    {
      desc: 'I  must explain to you how all this mistaken idea of denouncing pleasure.',
      img: 'assets/images/dashboard/boy-2.png',
      name: 'Mark Jecno',
    },
    {
      desc: 'I  must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born.',
      img: 'assets/images/dashboard/boy-2.png',
      name: 'Mark Jecno',
    }
  ]
  public owlcarouselOptions = {
    loop: true,
    margin: 10,
    items: 1,
    nav: false,
    dots: false
  };


  public salesCrmChart = {
    type: 'Bar',
    data: {
        labels: [],
        series: []
     },
     options: {
       stackBars: true,
         axisY: {
            labelInterpolationFnc: function(value) {
                return (value / 1000) + 'k';
            }
        },
        height: 300,
     }
}

  public chart1 = chartDataCorporate.chart1;
  public WidgetLineChart1 = chartDataCorporate.WidgetLineChart1;
  public WidgetLineChart2 = chartDataCorporate.WidgetLineChart2;
  public overAllChart = chartDataCorporate.overAllChart;



  public WidgetlineChart2 = chartData.WidgetlineChart2;
  public WidgetlineChart3 = chartData.WidgetlineChart3;
  public WidgetlineChart4 = chartData.WidgetlineChart4;
  public salesChart = chartData.salesChart;

  public monthlyChartLabels:any = [];
  public monthlyChartData:any = [];

  public monthlyChartUsersLevel:any = [];
  public monthlyChartUsersData:any = [];

  public monthlyChartColors = chartData.monthlyChartColors;
  public monthlyChartType = chartData.monthlyChartType;
  public monthlyChartLegend = chartData.monthlyChartLegend;
  public monthlyChartOptions = chartData.monthlyChartOptions;

  public dailyChartLabels = chartData.dailyChartLabels;
  public dailyChartData = chartData.dailyChartData;
  public dailyChartColors = chartData.dailyChartColors;
  public dailyChartType = chartData.dailyChartType;
  public dailyChartLegend = chartData.dailyChartLegend;
  public dailyChartOptions = chartData.dailyChartOptions;

  //public saleChartData = chartData.saleChartData;
  public saleChartData = chartData.saleChartData;
  public saleChartLabels =  chartData.saleChartLabels;
  public stepDeptLabels: any =  [];

  public saleChartColors = chartData.saleChartColors;
  public saleChartLegend = chartData.saleChartLegend;
  public saleChartType = chartData.saleChartType;
  public saleChartOptions = chartData.saleChartOptions;

  department_stats: any = {
    departmentChart: {
      numberOfSteps: []
    }
  };
  challenges: any = [];
  surveys: any = [];
  messageHandler: any = constants.constMessages;
  currentUserData :any;
  constructor(
    private apiService: AppServicesService,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    this.currentUser = JSON.parse(localStorage.getItem('user'));
    const reqData = {
      role: this.currentUser.role,
      roleId: this.currentUser._id
    };
    this.currentUserData = reqData;
    if (this.currentUser) {
      this.initializeIndex(this.currentUser._id);
      this.getPopularChallenge();
      // this.getSurvey();
      this.getCommonDashboardData('month','','', reqData);
      this.participantChartGraph();
      this.getAdminStats(reqData);
    }
  }

  selectSteps1(type,data){
    console.log("test1", type)
    this.startDate = type;
    //const dob = this.convertDate(type)
    this.getCommonDashboardData('month', this.startDate,this.endDate,data)
  }

  selectSteps(type,data){
    console.log("test", type)
    this.endDate = type;
    //const dob = this.convertDate(type)
    this.getCommonDashboardData('month', this.startDate,this.endDate,data)
  }

  participantChartGraph() {
    this.apiService.post('/api/v1/stat/corporate/totalCorporateUserChart',
      {corporateId: this.currentUser._id, recordType: 'month'}).subscribe( (response: any) => {
      if (response.data && response.status === 200) {
          this.WidgetLineChart1.data =  {
            series: [response.data]
          }
      } else {
        this.toastr.error(this.messageHandler[response.message], 'Error');
      }
    }, (errorResult) => {
      this.errorHandling(errorResult);
    });
  }

  getAdminStats(data:any) {
    this.apiService.post('/api/v1/stat/corporate', data).subscribe( (response: any) => {
      if (response.data && response.status === 200) {
        this.adminStats = response.data;
      } else {
        this.toastr.error(this.messageHandler[response.message], 'Error');
      }
    }, (errorResult) => {
      this.errorHandling(errorResult);
    });
  }
  
  getCommonDashboardData(type, startDate, endDate, data) {
    var findQuery;
    if(endDate){
      findQuery = {
        type:type,
        data:data,
        startDate:startDate,
        endDate:endDate
      }
    }else{
      findQuery = {
        type:type,
        data:data
      }
    }
    this.apiService.post('/api/v1/stat/corporate/company-dashboard', findQuery).subscribe( (response: any) => {
      if (response.data && response.status === 200) {
        this.department_stats = response.data;
        if(this.department_stats.totalUserByDept){
          this.numberOfUsers(this.department_stats.totalUserByDept);
        }

        if(this.department_stats.departmentChart.numberOfSteps){
          this.departmentStepsGraphs(type)
        }
      } else {
        this.toastr.error(this.messageHandler[response.message], 'Error');
      }
    }, (errorResult) => {
      this.errorHandling(errorResult);
    });
  }

  convertDate(inputFormat) {
    function pad(s) { return (s < 10) ? '0' + s : s; }
    var d = new Date(inputFormat)
    return [pad(d.getDate()), pad(d.getMonth()+1), d.getFullYear()].join('/')
  }

  numberOfUsers(userData){
    var labels = userData.reduce((acc, value) => {
      acc.push(value.name);
      return acc;
    },[]); 
    var count = userData.reduce((acc, value) => {
     // var multiplyBy100 = value.count * 100;
      acc.push(value.count);
      return acc;
    },[]); 

    this.monthlyChartUsersLevel = labels;
    this.monthlyChartUsersData = count;
  }

  departmentStepsGraphs(type){
    var labels = this.department_stats.departmentChart.numberOfSteps.reduce((acc, value) => {
      acc.push(value.name);
      return acc;
    },[]); 

    var steps = this.department_stats.departmentChart.numberOfSteps.reduce((acc, value) => {
      acc.push(value.steps);
      return acc;
    },[]); 

    if(type=='month'){
      var stepsRightGraph = [];
      var deptArray= [];
      if(this.department_stats.departmentChart.numberOfStepsBar){
        stepsRightGraph = this.department_stats.departmentChart.numberOfStepsBar.map( (element) => {
          deptArray.push(element.name) 
          var chlddata = element.month.reduce((acc, value, index) => {
              acc.push({month: element.name, steps: element.steps[index]});
              return acc;
          },[]);

          var holder = {};

          chlddata.forEach(function(d) {
            if (holder.hasOwnProperty(d.month)) {
              holder[d.month] = holder[d.month] + d.steps;
            } else {
              holder[d.month] = d.steps;
            }
          });

          var obj2 = [];

          for (var prop in holder) {
            obj2.push({ month: prop, steps: holder[prop] });
          }
          var customGraphData = this.fillOtherMonth(deptArray,obj2);
          return customGraphData;
        });
      }
      var saleChartData = [];
      if(stepsRightGraph){
        saleChartData = stepsRightGraph.reduce( (acc, element) => {
            element.map( (val) => {
              acc.push(val);
              return acc;
            });
          return acc;
        }, []);
      }
      this.stepDeptLabels = deptArray//this.saleChartLabels;
      this.saleChartData = saleChartData;
    }else{
      var deptArray= [];
      var weekSteps = [];
      if(this.department_stats.departmentChart.numberOfStepsBar){
        weekSteps = this.department_stats.departmentChart.numberOfStepsBar.map( (element) => {
          deptArray.push(element.name) 
          var weekSteps = element.week.reduce((acc, value, index) => {
              acc.push({week: element.name, steps: element.steps[index]});
              return acc;
          },[]);
          var weeklyData = this.fillOtherWeek(deptArray,weekSteps);
          return weeklyData;
        });
      }
      
      var saleChartDataWeekly = [];
      if(weekSteps){
        saleChartDataWeekly = weekSteps.reduce( (acc, element) => {
          element.map( (val) => {
            acc.push(val);
            return acc;
          });
          return acc;
        }, []);
      }
      
      var weekLabel = [];
      for (var i=1; i<53; i++){ weekLabel.push(i); }
      this.stepDeptLabels = weekLabel;
      this.saleChartData = saleChartDataWeekly;
    }

    
    this.monthlyChartLabels = labels;
    this.monthlyChartData = steps;
    
  }

  fillOtherMonth(deptArray,customMonth){
    const result = [];
    const customData = [];
    const allMonths = ['Jan', 'Feb', 'March', 'April', 'May', 'June', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];
    deptArray.forEach( (month, index) => {
      //console.log('index',month)
      const item = customMonth.find(item => item.month == month) ;
      if (item) {
        
          result.push(item.steps);
      } else {
          result.push((0));
      }
    });
    //console.log("resultresult", result)
    customData.push({data: result});
    return customData;
  }

  fillOtherWeek(deptArray,customMonth){
    console.log('customMonthcustomMonth',customMonth)
    const result = [];  
    const customData = [];

    var allWeek = [];
    for (var i=0; i<52; i++){
      allWeek.push(i);
    }
    console.log('allWeek',allWeek)

    allWeek.forEach( (week, index) => {
      const item = customMonth.find(item => item.week == (index+1)) ;
      if (item) {
          result.push(item.steps);
      } else {
          result.push((0));
      }
    });
    customData.push({data: result});
    return customData;
  }


  /* Get all challenge
  * Later it will come according to the corporate role.
  */
  getPopularChallenge() {
    const reqData = {
      role: this.currentUser.role,
      roleId: this.currentUser._id
    };
    this.apiService.post('/api/v1/stat/vendor/getPopularChallenge', reqData).subscribe((response: any) => {
      if (response.status === 200) {
        this.challenges = response.data;
      } else {
        this.toastr.error(this.messageHandler[response.message], 'Error');
      }
    }, (errorResult) => {
    this.errorHandling(errorResult);
  });
}

  getSurvey() {
    const requestData = {page: 0, pageLimit: 10, adminId: this.adminId, skip: 0};
    this.apiService.post('/api/v1/surveys', requestData).subscribe((response: any) => {
      if (response.data && response.status === 200) {
        this.surveys = response.data;
      } else {
        this.toastr.error(this.messageHandler[response.message], 'Error');
      }
    }, (errorResult) => {
      this.errorHandling(errorResult);
    });
  }

  initializeIndex(user_id) {
    this.apiService.get('/api/v1/stat/corporate/totalParticipants/', user_id).subscribe((response: any) => {
      this.items = response.data;
    }, (errorResult) => {
      this.errorHandling(errorResult);
    });
  }

  errorHandling(error){
    try {
      this.isLoading = false;
      const errorObj = error ? JSON.parse(error) : '';
      this.toastr.error(errorObj.message, 'Error');
    } catch (error) {
      this.toastr.error(error.message, 'Error');
    }
  }

}
