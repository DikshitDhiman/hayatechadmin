import { NgModule } from "@angular/core";
import { SanitizeTextPipe } from "./sanitize-text";

@NgModule({
  declarations: [SanitizeTextPipe],
  exports: [SanitizeTextPipe]
})
export class SanitizeTextModule {}