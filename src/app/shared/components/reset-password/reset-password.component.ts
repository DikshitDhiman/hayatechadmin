import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { ToastrService } from 'ngx-toastr';

import { AppServicesService } from './../../service/app-services.service';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {

  resetForm: FormGroup;
  isLoading: Boolean = false;
  token: any;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private apiService: AppServicesService,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.token = params.get('token');
    });
    this.createForm();
  }

  createForm() {
    this.resetForm = this.fb.group({
      password: ['', Validators.required],
      confpass: ['', Validators.required]
    });
  }

  submitForm() {
    const reqData = {
      password: this.resetForm.value.password,
      confpass: this.resetForm.value.confpass,
      token : this.token
    }
    this.apiService.post('/api/reset_password', reqData).subscribe((response: any) => {
      if (response.status === 200) {
        localStorage.setItem('user', JSON.stringify(response.data));
        this.router.navigate(['/']);
        this.toastr.success(response.message, 'Success');
        this.resetForm.reset();
      } else {
        this.toastr.error('Something went wrong', 'Error');
      }
    }, (errorResult) => {
      this.errorHandling(errorResult);
    });
  }

  errorHandling(error) {
    try {
      this.isLoading = false;
      const errorObj = error ? JSON.parse(error) : '';
      this.toastr.error(errorObj.message, 'Error');
    } catch (error) {
      this.toastr.error(error.message, 'Error');
    }
  }

}
