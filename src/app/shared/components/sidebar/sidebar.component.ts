/* import { Component, ViewEncapsulation } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { NavService, Menu } from '../../service/nav.service';
 */


import { Component, ViewEncapsulation } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { NavService, Menu } from '../../service/nav.service';
import { environment } from './../../../../environments/environment';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class SidebarComponent {

  public menuItems: Menu[];
  public url: any;
  public fileurl: any;
  public currentUser: any;
  public imageName : any;
  public imageSrc : any;
  siteURL: String =  environment.siteURL;
  constructor(private router: Router, public navServices: NavService) {

    this.currentUser = JSON.parse(localStorage.getItem('user'));
    console.log('this.currentUser',this.currentUser)

    if(this.currentUser.role == 'vendor') {
      let name = this.currentUser.contactPerson.split(' ');
      this.imageName = name['0'].charAt(0).toUpperCase() +''+ ((name['1'] && name['1'] != undefined) ? name['1'].charAt(0).toUpperCase() : '');
      if(this.currentUser.image && this.currentUser.image!= undefined && this.currentUser.image !='') {
        this.imageSrc = this.siteURL +this.currentUser.image;
      } else {
        this.imageSrc = '';
      }
      
    } else if(this.currentUser.role == 'corporate') {
      let name = this.currentUser.contactPerson.split(' ');
      this.imageName = name['0'].charAt(0).toUpperCase() +''+ ((name['1'] && name['1'] != undefined) ? name['1'].charAt(0).toUpperCase() : '');
      if(this.currentUser.image && this.currentUser.image!= undefined && this.currentUser.image !='') {
        this.imageSrc = this.siteURL +this.currentUser.image;
      } else {
        this.imageSrc = '';
      }
    } else {
      this.imageName = this.currentUser.firstName.charAt(0).toUpperCase() +''+this.currentUser.lastName.charAt(0).toUpperCase();
      if(this.currentUser.image && this.currentUser.image!= undefined && this.currentUser.image !='') {
        this.imageSrc = this.siteURL +this.currentUser.image;
      } else {
        this.imageSrc = '';
      }
    }

    this.navServices.items.subscribe(menuItems => {
      this.menuItems = menuItems;
      this.router.events.subscribe((event) => {
        if (event instanceof NavigationEnd) {
          menuItems.filter(items => {
            if (items.path === event.url) {
              this.setNavActive(items)
            }
            if (!items.children) return false 
            items.children.filter(subItems => {
              if (subItems.path === event.url)
                this.setNavActive(subItems)
              if (!subItems.children) return false
              subItems.children.filter(subSubItems => {
                if (subSubItems.path === event.url)
                  this.setNavActive(subSubItems)
              });
            });
          });
        }
      })
    })
  }

  // Active Nave state
  setNavActive(item) {
    this.menuItems.filter(menuItem => {
      if (menuItem != item)
        menuItem.active = false;
      if (menuItem.children && menuItem.children.includes(item))
        menuItem.active = true;
      if (menuItem.children) {
        menuItem.children.filter(submenuItems => {
          if (submenuItems.children && submenuItems.children.includes(item)) {
            menuItem.active = true;
            submenuItems.active = true;
          }
        })
      }
    })
  }

  // Click Toggle menu
  toggletNavActive(item) {
    if (!item.active) {
      this.menuItems.forEach(a => {
        if (this.menuItems.includes(item))
          a.active = false;
        if (!a.children) return false;
        a.children.forEach(b => {
          if (a.children.includes(item)) {
            b.active = false;
          }
        })
      });
    }
    item.active = !item.active
  }

  //Fileupload
  readUrl(event: any) {
    if (event.target.files.length === 0)
      return;
    //Image upload validation
    var mimeType = event.target.files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      return;
    }
    // Image upload
    var reader = new FileReader();
    reader.readAsDataURL(event.target.files[0]);
    reader.onload = (_event) => {
      this.url = reader.result;
    }
  }

}
