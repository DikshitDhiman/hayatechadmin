import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl } from '@angular/forms';
import { NavService, Menu } from '../../service/nav.service';
import { TranslateService } from '@ngx-translate/core';
// import { AuthService } from '../../service/firebase/auth.service';
import { CustomizerService } from '../../service/customizer.service';
const body = document.getElementsByTagName('body')[0];

import { fromEvent } from 'rxjs';
import { debounceTime, map } from 'rxjs/operators';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  public menuItems: Menu[];
  public items: Menu[];
  public searchResult: Boolean = false;
  public searchResultEmpty: Boolean = false;
  public text: string;
  public openNav: Boolean = false;
  public right_sidebar: Boolean = false;
  public openToggle: Boolean = false;
  public open : Boolean = false;
  public openSearch : Boolean = false;

  @Output() rightSidebarEvent = new EventEmitter<Boolean>();
  @Output() toggleEvent = new EventEmitter<Boolean>();


  constructor(
    public navServices: NavService,
    private translate: TranslateService,
    private route: Router,
    // public authService: AuthService,
    public customize: CustomizerService) {
    translate.setDefaultLang('en');
  }

  ngOnInit() {
    this.navServices.items.subscribe(menuItems => {
      this.items = menuItems;
    });
    // this.keyUpSearch();
  }

/*   keyUpSearch(){
    const searchBox = document.getElementById('searchInput');
    const keyup$ = fromEvent(searchBox, 'keyup')
    keyup$.pipe(
      map((i: any) => i.currentTarget.value),
      debounceTime(1000)
    ).subscribe(data => {
      console.log("data is", data)
    });
  } */

  signOut() {
    localStorage.clear();
    this.route.navigateByUrl('/');
  }


  clickSearch(){
    this.openSearch = !this.openSearch;
  }

  right_side_bar() {
    this.right_sidebar = !this.right_sidebar;
    this.rightSidebarEvent.emit(this.right_sidebar);
  }

  ngOnDestroy() {
    this.removeFix();
  }

  openHeaderMenu(){
    this.open = !this.open;
  }

  openMobileNav() {
    this.openNav = !this.openNav;
  }

  public changeLanguage(lang) {
    this.translate.use(lang);
  }

  checkSearchResultEmpty(items) {
    if (!items.length) {
      this.searchResultEmpty = true;
    } else {
      this.searchResultEmpty = false;
    }
  }

  addFix() {
    this.searchResult = true;
    body.classList.add('offcanvas');
  }

  removeFix() {
    this.searchResult = false;
    body.classList.remove('offcanvas');
    this.text = '';
  }

  switchToggle() {
    this.openToggle = !this.openToggle;
    this.toggleEvent.emit(this.openToggle);
  }

}
