import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class DataService {

  private dataSource = new BehaviorSubject('');

  private editFilterSub = new BehaviorSubject<any>(null);

  currentMessage = this.dataSource.asObservable();

  constructor() { }

  changeMessage(data: any) {
    this.dataSource.next(data);
  }
/*
  onEditFilter() {
    return this.editFilterSub;
  }

  editFilter(filter){
    this.editFilterSub.next(filter)
  } */
}
