import { Injectable, HostListener } from '@angular/core';
import { BehaviorSubject, Observable, Subscriber } from 'rxjs';

// Menu
export interface Menu {
  path?: string;
  title?: string;
  icon?: string;
  type?: string;
  headTitle?: string;
  badgeType?: string;
  badgeValue?: string;
  active?: boolean;
  children?: Menu[];
}

// @Injectable()
@Injectable({
  providedIn: 'root'
})
export class NavService {

  constructor() {
    this.onResize();
    if (this.screenWidth < 1199) {
      this.openToggle = true;
    }
  }
  public screenWidth: any;
  public openToggle: Boolean = false;

  // tslint:disable-next-line:member-ordering
  MENUITEMS: Menu[] = [
    {
      headTitle: 'Super Admin'
    },
    {
      path: '/admin', title: 'Dashboard', icon: 'fa fa-dashboard', active: false, type: 'admin | corporate | vendor'
    },
    {
      title: 'Company', icon: 'icon-briefcase', type: 'admin', active: false, children: [
        { path: '/admin/company/', title: 'List Company', type: 'link' },
        { path: '/admin/company/add-company', title: 'Add Company', type: 'link' }
      ]
    },
    {
      title: 'Department', icon: 'fa fa-ticket', type: 'admin', active: false, children: [
        { path: '/admin/department/', title: 'List Department', type: 'link' },
        { path: '/admin/department/add-department', title: 'Add Department', type: 'link' }
      ]
    },
    {
      title: 'Vendor', icon: 'icofont icofont-deal', type: 'admin', active: false, children: [
        { path: '/admin/vendor/', title: 'List Vendor', type: 'link' },
        { path: '/admin/vendor/add-vendor', title: 'Add Vendor', type: 'link' }
      ]
    },
    {
      title: 'Reward Categories', icon: 'icon-gift', type: 'admin | vendor', active: false, children: [
        { path: '/admin/reward/', title: 'List Reward Categories', type: 'link' },
        { path: '/admin/reward/add-reward', title: 'Add Reward Categories', type: 'link' }
      ]
    },
    {
      title: 'Marketplace', icon: 'icofont icofont-cart-alt', type: 'admin | vendor', active: false, children: [
        { path: '/admin/marketplace/', title: 'List Marketplace', type: 'link' },
        { path: '/admin/marketplace/add-marketplace', title: 'Add Marketplace', type: 'link' }
      ]
    },
    {
      path: '/admin/inventory/', title: 'Inventory', icon: 'fa fa-ticket', active: false, type: 'vendor'
    },
    {
      title: 'Users', icon: 'icon-user', type: 'admin | corporate', active: false, children: [
        { path: '/admin/users/', title: 'List Users', type: 'link' },
        { path: '/admin/users/add-users', title: 'Add Users', type: 'link' },
        { path: '/admin/users/inactive-users', title: ' Inactive Users', type: 'link' }
      ]
    },
    {
      title: 'Survey', icon: 'icofont icofont-worker', type: 'admin | corporate', active: false, children: [
        { path: '/admin/survey/', title: 'List Survey Question', type: 'link' },
        { path: '/admin/survey/add-survey', title: 'Add Survey Question', type: 'link' },
        { path: '/admin/survey/list-survey-category', title: 'List Survey Type', type: 'link' },
        { path: '/admin/survey/add-survey-category', title: 'Add Survey Type', type: 'link' },
        { path: '/admin/survey/result', title: 'View Survey Results', type: 'link' }
      ]
    },
    {
      title: 'Challenges', icon: 'icon-cup', type: 'admin', active: false, children: [
        { path: '/admin/challenge/', title: 'List Challenges', type: 'link' },
        { path: '/admin/challenge/add-challenge', title: 'Add Challenges', type: 'link' }
      ]
    },
    /* {      title: 'Promo', icon: 'fa fa-gift', type: 'admin', active: false, children: [
        { path: '/admin/promo/', title: 'List Promo', type: 'link' },
        { path: '/admin/promo/add-promo', title: 'Add Promo', type: 'link' }
      ]
    }, */
    {
      title: 'Group', icon: 'fa fa-users', type: 'admin', active: false, children: [
        { path: '/admin/group/', title: 'List Group', type: 'link' },
        { path: '/admin/group/add-group', title: 'Add Group', type: 'link' }
      ]
    }
/*     {
      path: '/admin/invite/', title: 'Invite User', icon: 'icofont icofont-worker-group', active: false, type: 'admin'
    } */
  ];

  items = new BehaviorSubject<Menu[]>(this.MENUITEMS);

  // Windows width
  @HostListener('window:resize', ['$event'])
  onResize(event?) {
    this.screenWidth = window.innerWidth;
  }
}
