import { Injectable } from '@angular/core';
import { ConfigDB } from '../data/config/config';
@Injectable({
  providedIn: 'root'
})
export class CustomizerService {

  constructor() {
  }

      // Configration Layout
      public data = ConfigDB.data;
      // Set Customize layout Version
      setLayoutType(layout: string) {
        document.body.setAttribute('main-theme-layout', layout);
        this.data.mainThemeLayout = layout;
      }

      menuLayoutType(layout: any){
          this.data.menuLayout = layout;
      }

      sidebarLayoutType(layout: any) {
        this.data.scroll = layout;
      }

      sidebarNavLayoutType(layout: any) {
        this.data.borderNavigation = layout;
      }

      rightSidebarIconType(layout: any) {
        this.data.rightSidebarIcon = layout;
      }

      backgroungImageType(layout: any) {
        this.data.backgroungImage = layout;
      }

      defaultLayoutType(layout: any) {
        this.data.defaultNavigation = layout;
        this.data.scroll = 'custom-scrollbar',
        this.data.borderNavigation = '',
        this.data.rightSidebarIcon = '',
        this.data.backgroungImage = ''
      }

      headerLayoutType(val: any){
        this.data.header.semiLightbgColor = val;
        this.data.header.headerBgColor = val;
      }

      brandLayoutType(val: any){
        this.data.header.semiLightbgColor = val;
        this.data.header.headerBgColor = '';
      }

      navLayoutType(val: any){
        this.data.header.semiLightbgColor = '';
        this.data.header.headerBgColor = val;
      }
}
