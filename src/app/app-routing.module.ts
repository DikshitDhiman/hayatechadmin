import { NgModule } from '@angular/core';
import { RouterModule, Routes, PreloadAllModules } from '@angular/router';

// Components
import { LoginComponent } from './auth/login/login.component';
import { ForgotPasswordComponent } from './shared/components/forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './shared/components/reset-password/reset-password.component';
import { FaqComponent } from './pages/faq/faq.component';

import { AdminGuard } from './shared/guard/admin.guard';

const appRoutes: Routes = [
  {
    path: '',
    component: LoginComponent
  },
  {
    path : 'forgot-password',
    component : ForgotPasswordComponent
  },
  {
    path : 'reset-password/:token',
    component : ResetPasswordComponent
  },
  {
    path : 'faq',
    component : FaqComponent
  },
  {
    path : 'admin',
    loadChildren : './layout/layout.module#LayoutModule',
    canActivate: [AdminGuard]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes,
    {
      preloadingStrategy: PreloadAllModules,
      anchorScrolling: 'enabled',
      scrollPositionRestoration: 'enabled'
    })],
  exports: [RouterModule]
})

export class AppRoutingModule {
}