import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ImageCroppedEvent } from 'ngx-image-cropper';

import { ToastrService } from 'ngx-toastr';

import { AppServicesService } from './../../../shared/service/app-services.service';
import { environment } from './../../../../environments/environment';
import { constants } from '../../../constants/const';

@Component({
  selector: 'app-add-reward',
  templateUrl: './add-reward.component.html',
  styleUrls: ['./add-reward.component.css']
})
export class AddRewardComponent implements OnInit {
  imageChangedEvent: any = '';
  croppedImage: any = '';
  base64Data:any;
  addRewardForm: FormGroup;
  isLoading: Boolean = false;
  currenUser: any;
  base64textString: any = [];
  isEdit: Boolean = false;
  isSubmitted: Boolean = false;
  isSelectedImg: Boolean = false;
  messageHandler: any = constants.constMessages;
  isImageEdit: Boolean = false;
  rewardId: any;
  siteURL: String = environment.siteURL;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private apiService: AppServicesService,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    this.createForm();
    this.currenUser = JSON.parse(localStorage.getItem('user'));
    this.route.paramMap.subscribe(paramsData => {
      if (paramsData.get('id')) {
        this.isEdit = true;
        this.rewardId = paramsData.get('id');
        this.getReward();
      } else {
        this.isEdit = false;
      }
    });
  }

  createForm() {
    this.addRewardForm = this.fb.group({
      name: ['', [Validators.required, Validators.pattern('[a-zA-Z][a-zA-Z ]+[a-zA-Z]$')]],
      base64Data: ['']
    });
  }

  getReward() {
    this.apiService.get('/api/v1/category/', this.rewardId).subscribe((response: any) => {
      if (response && response.status === 200) {
        this.addRewardForm.setValue({
          name: response.data.name,
          base64Data: this.siteURL + response.data.image
        });
      } else {
        this.toastr.error(this.messageHandler[response.message], 'Error');
      }
    }, (errorResult) => {
      this.errorHandling(errorResult);
    });
  }

  resetForm() {
    this.addRewardForm = this.fb.group({});
    this.createForm();
  }

  onUploadChange(event: any): void {
    this.imageChangedEvent = event;
  }

  imageCropped(event: ImageCroppedEvent) {
    this.croppedImage = event.base64;

    if (this.croppedImage) {
      this.isSelectedImg = true;
      this.isImageEdit = true;
      this.base64Data = this.croppedImage;
    }
  }
  
  /* onUploadChange(evt: any) {
    const file = evt.target.files[0];
    if (file) {
      console.log("file :", file);
      const reader = new FileReader();
      reader.onload = this.handleReaderLoaded.bind(this);
      console.log("reader.onload----",reader.onload);
      reader.readAsBinaryString(file);
      this.isSelectedImg = true;
    }
  } */

  handleReaderLoaded(e: any) {
    this.isImageEdit = true;
    this.addRewardForm.controls.base64Data.setValue('data:image/png;base64,' + btoa(e.target.result));
  }

  submitForm() {
    this.isSubmitted = true;
    const reqData = this.addRewardForm.value;
    reqData['role'] = this.currenUser.role;
    if (this.currenUser.role === 'admin') {
      reqData['adminId'] = this.currenUser._id;
    } else {
      reqData['vendorId'] = this.currenUser._id;
    }
    if (this.isEdit) {
      reqData.rewardId = this.rewardId;
    }
    if (this.isImageEdit) {
      // reqData.base64Data = reqData.base64Data;
      reqData.base64Data = this.base64Data;
    } else {
      reqData.base64Data = '';
    }

    this.apiService.post('/api/v1/category/add', reqData).subscribe((response: any) => {
      if (response.status === 200) {
        this.toastr.success(this.messageHandler[response.message], 'Success');
        this.addRewardForm.reset();
        this.router.navigate(['/admin/reward/']);
      } else {
        this.toastr.error(this.messageHandler[response.message], 'Error');
      }
    }, (errorResult) => {
      this.errorHandling(errorResult);
    });
  }

  errorHandling(error) {
    try {
      this.isLoading = false;
      const errorObj = error ? JSON.parse(error) : '';
      this.toastr.error(errorObj.message, 'Error');
    } catch (error) {
      this.toastr.error(error.message, 'Error');
    }
  }
}
