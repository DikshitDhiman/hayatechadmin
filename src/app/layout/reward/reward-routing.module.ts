import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListRewardComponent } from './list-reward/list-reward.component';
import { AddRewardComponent } from './add-reward/add-reward.component';

const routes: Routes = [{
  path: '',
  children: [
    {
      path: '',
      component: ListRewardComponent
    },
    {
      path: 'add-reward',
      component: AddRewardComponent
    },
    {
      path: 'edit-reward/:id',
      component: AddRewardComponent
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RewardRoutingModule { }
