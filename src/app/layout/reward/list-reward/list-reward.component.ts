import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import Swal from 'sweetalert2';
import { fromEvent } from 'rxjs';
import { debounceTime, map } from 'rxjs/operators';
import { AppServicesService } from './../../../shared/service/app-services.service';
import { environment } from './../../../../environments/environment';
import { constants } from './../../../constants/const';

@Component({
  selector: 'app-list-reward',
  templateUrl: './list-reward.component.html',
  styleUrls: ['./list-reward.component.css']
})
export class ListRewardComponent implements OnInit {

  rewards: any = [];
  searchQuery: String = '';
  isLoading: Boolean = false;
  siteURL: String = environment.siteURL;
  currentUser: any;
  openToggle: Boolean = false;
  pageLimit: any = constants.recordsPageLimit;
  requestData: any;
  page = 1;
  search: String;
  totalItems = 0;
  itemsPerPage: any = constants.recordsPageLimit;
  sort: String = 'companyName';
  actions: Array<any>;
  status: Array<any>;
  messageHandler: any = constants.constMessages;
  items = [];
  column: Array<any> = [
    { id: 'image', name: 'Image', sorting: false, type: 'image', display: 'left' },
    { id: 'name', name: 'Name', sorting: true, type: 'string', display: 'left', order: 'neutral' },
    { name: 'Status', type: 'status', sorting: false, display: 'center' },
    { name: 'Action', sorting: false, type: 'string', display: 'center' }
  ];

  constructor(
    private router: Router,
    private apiService: AppServicesService,
    private toastr: ToastrService
  ) { }


  ngOnInit() {
    this.currentUser = JSON.parse(localStorage.getItem('user'));
    this.requestData = {
      role: this.currentUser.role,
      roleId: this.currentUser._id,
      page: 0,
      pageLimit: this.pageLimit,
      skip: 0
    };
    this.getReward(this.requestData);

    this.status = [{
      action: (item: any) => this.changeStatus(item['_id'], item['is_active']),
    }];
    console.log('currentUser.role tt ', this.currentUser.role)
    if (this.currentUser.role === 'vendor') {
      this.actions = [{
        parentClass: 'edit',
        action: (item) => this.editItem(item['_id']),
        title: 'Edit',
        childClass: 'icofont-ui-edit'
      }];
    } else {
      this.actions = [{
        parentClass: 'edit',
        action: (item) => this.editItem(item['_id']),
        title: 'Edit',
        childClass: 'icofont-ui-edit'
      }, {
        parentClass: 'delete',
        action: (item) => this.deleteReward(item['_id']),
        title: 'Delete',
        childClass: 'icofont-ui-delete'
      }];
    }

    setTimeout(() => {
      this.keyUpSearch();
    }, 1000);
  }

  keyUpSearch() {
    const searchBox = document.getElementById('searchInputForReward');
    if (searchBox) {
      const keyup$ = fromEvent(searchBox, 'keyup');
      keyup$.pipe(
        map((i: any) => i.currentTarget.value),
        debounceTime(1000)
      ).subscribe(searchQuery => {
        this.searchRewards(searchQuery);
      });
    }
  }

  searchRewards(searchQuery) {
    this.searchQuery = searchQuery;
    this.requestData = {
      role: this.currentUser.role,
      roleId: this.currentUser._id,
      searchQuery: searchQuery,
      page: 0,
      pageLimit: this.pageLimit,
      skip: 0
    };
    this.getReward(this.requestData);
  }

  getReward(data: any) {
    this.apiService.post('/api/v1/categories', data).subscribe((response: any) => {
      if (response.data && response.status === 200) {
        this.rewards = response.data;
      } else {
        this.toastr.error(this.messageHandler[response.message], 'Error');
      }
    }, (errorResult) => {
      this.errorHandling(errorResult);
    });
  }

  paginate(event: any) {
    const reqData = {
      role: this.currentUser.role,
      roleId: this.currentUser._id,
      page: event.page,
      searchQuery: this.searchQuery,
      pageLimit: this.pageLimit,
      pageCount: event.pageCount,
      skip: 0
    };
    this.getReward(reqData);
  }


  sortData(field: any, order: any) {
    this.column = this.column.map(x => field === x['id'] ? { ...x, 'order': order } : x);
    if (order === 'asc') {
      this.rewards.data = this.rewards.data.sort((a, b) => a[field].localeCompare(b[field]));
    } else {
      this.rewards.data = this.rewards.data.sort((a, b) => b[field].localeCompare(a[field]));
    }
  }



  editItem(id) {
    this.router.navigate([`/reward/edit-reward/${id}`]);
  }

  changeStatus(id, status) {
    status = status ? false : true;
    const reqData = {
      id: id,
      status: status
    };
    this.apiService.post('/api/v1/category/change_status', reqData).subscribe((response: any) => {
      if (response.status === 200) {
        this.toastr.success('Status changed successfully', 'Success');
        this.getReward(this.requestData);
      } else {
        this.toastr.error(this.messageHandler[response.message], 'Error');
      }
    });
  }

  deleteReward(id: any) {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        this.apiService.delete('/api/v1/category/delete/', id).subscribe((response: any) => {
          if (response && response.status === 200) {
            this.toastr.success(this.messageHandler[response.message], 'Success');
            this.getReward(this.requestData);
          } else {
            this.toastr.error(this.messageHandler[response.message], 'Error');
          }
        }, (errorResult) => {
          this.errorHandling(errorResult);
        });
      }
    });
  }

  switchToggle() {
    this.openToggle = !this.openToggle;
  }

  errorHandling(error) {
    try {
      this.isLoading = false;
      const errorObj = error ? JSON.parse(error) : '';
      this.toastr.error(errorObj.message, 'Error');
    } catch (error) {
      this.toastr.error(error.message, 'Error');
    }
  }

}
