import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ImageCropperModule } from 'ngx-image-cropper';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PaginatorModule } from 'primeng/paginator';
import { SharedModule } from '../../shared/shared.module';
import { RewardRoutingModule } from './reward-routing.module';
import { ListRewardComponent } from './list-reward/list-reward.component';
import { AddRewardComponent } from './add-reward/add-reward.component';

@NgModule({
  declarations: [ListRewardComponent, AddRewardComponent],
  imports: [
    CommonModule,
    RewardRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    PaginatorModule,
    SharedModule,
    ImageCropperModule
  ]
})
export class RewardModule { }
