import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout.component';

const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    children: [
      {
          path: '',
          loadChildren: './dashboard/dashboard.module#DashboardModule'
      },
      {
        path: 'company',
        loadChildren: './corporate/corporate.module#CorporateModule'
      },
      {
        path: 'users',
        loadChildren: './users/users.module#UsersModule'
      },
      {
        path: 'vendor',
        loadChildren: './vendor/vendor.module#VendorModule'
      },
      {
        path: 'department',
        loadChildren: './department/department.module#DepartmentModule'
      },
      {
        path: 'marketplace',
        loadChildren: './marketplace/marketplace.module#MarketplaceModule'
      },
      {
        path: 'inventory',
        loadChildren: './inventory/inventory.module#InventoryModule'
      },
      {
        path: 'reward',
        loadChildren: './reward/reward.module#RewardModule'
      },
      {
        path: 'challenge',
        loadChildren: './challenge/challenge.module#ChallengeModule'
      },
      {
        path: 'survey',
        loadChildren: './survey/survey.module#SurveyModule'
      },
      {
        path: 'promo',
        loadChildren: './promoCode/promoCode.module#promoCodeModule'
      },
      {
        path: 'group',
        loadChildren: './group/group.module#GroupModule'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LayoutRoutingModule { }
