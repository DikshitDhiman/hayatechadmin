import { Component, OnInit } from '@angular/core';
import { CustomizerService } from '../shared/service/customizer.service';
import { NavService } from 'src/app/shared/service/nav.service';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css']
})
export class LayoutComponent implements OnInit {

  public toggle;

  openToggle: boolean;
  constructor(public customize: CustomizerService, public navService: NavService) {
    if (this.navService.openToggle === true) {
      this.openToggle = !this.openToggle;
      this.toggle = this.openToggle;
    }
  }

  receiveToggle($event: any) {
    this.openToggle = $event;
    this.toggle = this.openToggle;
  }

  ngOnInit() { }
}
