import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import Swal from 'sweetalert2';
import { fromEvent } from 'rxjs';
import { debounceTime, map } from 'rxjs/operators';
import { AppServicesService } from './../../../shared/service/app-services.service';
import { constants } from './../../../constants/const';

@Component({
  selector: 'app-list-department',
  templateUrl: './list-department.component.html',
  styleUrls: ['./list-department.component.css']
})
export class ListDepartmentComponent implements OnInit {
  page = 1;
  search: String;
  searchQuery: String = '';
  department: any = [];
  itemsPerPage: any = constants.recordsPageLimit;
  isLoading: Boolean = false;
  sort: String = 'companyName';
  currentUser: any;
  actions: Array<any>;
  status: Array<any>;
  openToggle: Boolean = false;
  messageHandler: any = constants.constMessages;
  items = [];
  pageLimit: any = constants.recordsPageLimit;
  requestData: any;
  column: Array<any> = [
  { id: 'name', name: 'Name', sorting: true, type: 'string', display: 'left', order: 'neutral' },
  { id: 'corporateId', child: 'name', name: 'Company', sorting: false, type: 'object', display: 'left', order: 'neutral' },
    { name: 'Status', type: 'status', sorting: false, display: 'center' },
    { name: 'Action', sorting: false, type: 'string', display: 'center' }
  ];

  constructor(
    private router: Router,
    private apiService: AppServicesService,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    this.currentUser = JSON.parse(localStorage.getItem('user'))._id;
    this.requestData = {
      adminId: this.currentUser,
      page: 0,
      pageLimit: this.pageLimit,
      skip: 0
    };

    this.getDepartments(this.requestData);

    this.status = [{
      action: (item: any) => this.changeStatus(item['_id'], item['is_active']),
    }];

    this.actions = [{
      parentClass: 'edit',
      action: (item) => this.editItem(item['_id']), 
      title: 'Edit',
      childClass: 'icofont-ui-edit'
    }, {
      parentClass: 'delete',
      action: (item) => this.deleteItem(item['_id']),
      title: 'Delete',
      childClass: 'icofont-ui-delete'
    }];

    setTimeout(() => {
      this.keyUpSearch();
    }, 1000);
  }


  keyUpSearch(){
    const searchBox = document.getElementById('searchInputForDepartment');
    if(searchBox){
      const keyup$ = fromEvent(searchBox, 'keyup')
      keyup$.pipe(
        map((i: any) => i.currentTarget.value),
        debounceTime(1000)
      ).subscribe(searchQuery => {
        this.searchDepartments(searchQuery);
      });
    }
    
  }

  searchDepartments(searchQuery) {
    this.searchQuery = searchQuery;
    this.requestData = {
      adminId: this.currentUser,
      searchQuery: searchQuery,
      page: 0,
      pageLimit: this.pageLimit,
      skip: 0
    }
    this.getDepartments(this.requestData);
  }

  getDepartments(data) {
    this.apiService.post('/api/v1/departments', data).subscribe( (response: any) => {
      if (response.data && response.status === 200) {
        this.department = response.data;
      } else {
        this.toastr.error(this.messageHandler[response.message], 'Error');
      }
    }, (errorResult) => {
      this.errorHandling(errorResult);
    });
  }

  paginate(event) {
    const reqData = {
      adminId: this.currentUser,
      page: event.page,
      searchQuery: this.searchQuery,
      pageLimit: this.pageLimit,
      pageCount: event.pageCount,
      skip: 0
    };
    this.getDepartments(reqData);
  }

  sortData(field, order) {
    this.column = this.column.map(x => field === x['id'] ? { ...x, 'order': order } : x);
    if (order === 'asc') {
      this.department.data = this.department.data.sort((a, b) => a[field].localeCompare(b[field]));
    } else {
      this.department.data = this.department.data.sort((a, b) => b[field].localeCompare(a[field]))
    }
  }

  editItem(id) {
    this.router.navigate([`/department/edit-department/${id}`]);
  }

  changeStatus(id, status) {
    status = status ? false : true;
    const reqData = {
      departmentId: id,
      status: status
    };
    this.apiService.post('/api/v1/department/change_status', reqData).subscribe((response: any) => {
      if (response.status === 200) {
        this.toastr.success('Status changed successfully', 'Success');
        this.getDepartments(this.requestData);
      } else {
        this.toastr.error(this.messageHandler[response.message], 'Error');
      }
    });
  }

  deleteItem(id: any) {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        this.apiService.delete('/api/v1/department/delete/', id).subscribe((response: any) => {
          if (response.status === 200) {
            this.toastr.success(this.messageHandler[response.message], 'Success');
            this.getDepartments(this.requestData);
          } else {
            this.toastr.error(this.messageHandler[response.message], 'Error');
          }
        }, (errorResult) => {
          this.errorHandling(errorResult);
        });
      }
    });
  }

  errorHandling(error: any) {
    try {
      this.isLoading = false;
      const errorObj = error ? JSON.parse(error) : '';
      this.toastr.error(errorObj.message, 'Error');
    } catch (error) {
      this.toastr.error(error.message, 'Error');
    }
  }

}
