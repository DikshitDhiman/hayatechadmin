import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { ToastrService } from 'ngx-toastr';

import { AppServicesService } from './../../../shared/service/app-services.service';
import { constants } from './../../../constants/const';
@Component({
  selector: 'app-add-department',
  templateUrl: './add-department.component.html',
  styleUrls: ['./add-department.component.css']
})
export class AddDepartmentComponent implements OnInit {

  addDepartmentForm: FormGroup;
  isLoading: Boolean = false;
  currenUser: any;
  isEdit: Boolean = false;
  departmentId: any;
  departments: any = [];
  corporates: any = [];
  messageHandler: any = constants.constMessages;

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private apiService: AppServicesService,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    this.currenUser = JSON.parse(localStorage.getItem('user'));
    this.createForm();
    this.route.paramMap.subscribe(paramsData => {
      this.getCorporates();
      if (paramsData.get('id')) {
        this.isEdit = true;
        this.departmentId = paramsData.get('id');
        this.getDepartment();
      } else {
        this.isEdit = false;
      }
    });
  }

  createForm() {
    this.addDepartmentForm = this.fb.group({
      name: ['', [Validators.required, Validators.pattern('[a-zA-Z][a-zA-Z ]+[a-zA-Z]$')]],
      corporate: ['', [Validators.required]]
    });
  }

  getCorporates() {
    const reqData = {
      adminId: this.currenUser._id
    };
    this.apiService.post('/api/v1/corporates', reqData).subscribe((response: any) => {
      if (response.status === 200 && response.data) {
        this.corporates = response.data.data;
        this.addDepartmentForm.controls.corporate.setValue(this.corporates[0]._id);
      } else {
        this.toastr.error(this.messageHandler[response.message], 'Error');
      }
    }, (errorResult) => {
      this.errorHandling(errorResult);
    });
  }

  getDepartment() {
    this.apiService.get('/api/v1/department/', this.departmentId).subscribe((response: any) => {
      if (response && response.status === 200) {
        this.addDepartmentForm.setValue({
          name: response.data.name,
          corporate: (response.data.corporateId) ? response.data.corporateId._id : ''
        });
      } else {
        this.toastr.error(this.messageHandler[response.message], 'Error');
      }
    }, (errorResult) => {
      this.errorHandling(errorResult);
    });
  }

  resetForm() {
    this.addDepartmentForm = this.fb.group({});
    this.createForm();
  }

  submitForm() {
    const reqData = {
      adminId : this.currenUser._id,
      name : this.addDepartmentForm.value.name,
      corporateId : this.addDepartmentForm.value.corporate
    };

    if (this.isEdit) {
      reqData['departmentId'] = this.departmentId;
    }

    this.apiService.post('/api/v1/department/add', reqData).subscribe( (response: any) => {
      if (response.status === 200) {
        this.toastr.success(this.messageHandler[response.message], 'Success');
        this.router.navigate(['/admin/department/']);
        this.addDepartmentForm.reset();
      } else {
        this.toastr.error(this.messageHandler[response.message], 'Error');
      }
    }, (errorResult) => {
      this.errorHandling(errorResult);
    });
  }

  errorHandling(error){
    try {
      this.isLoading = false;
      const errorObj = error ? JSON.parse(error) : '';
      this.toastr.error(errorObj.message, 'Error');
    } catch (error) {
      this.toastr.error(error.message, 'Error');
    }
  }

}
