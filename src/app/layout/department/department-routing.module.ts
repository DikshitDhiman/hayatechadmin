import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddDepartmentComponent } from './add-department/add-department.component';
import { ListDepartmentComponent } from './list-department/list-department.component';


const routes: Routes = [{
  path: '',
  children: [
    {
      path: '',
      component : ListDepartmentComponent
    },
    {
      path: 'add-department',
      component : AddDepartmentComponent
    },
    {
      path: 'edit-department/:id',
      component : AddDepartmentComponent
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DepartmentRoutingModule { }
