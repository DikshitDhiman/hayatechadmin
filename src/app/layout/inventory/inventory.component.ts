import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { ToastrService } from 'ngx-toastr';
import { AppServicesService } from './../../shared/service/app-services.service';
import { constants } from './../../constants/const';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-inventory',
  templateUrl: './inventory.component.html',
  styleUrls: ['./inventory.component.scss']
})
export class InventoryComponent implements OnInit {
  searchInvoiceProductForm: FormGroup;
  messageHandler: any = constants.constMessages;
  product: any = [];
  currenUser: any;
  siteURL: String = environment.siteURL;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private apiService: AppServicesService,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    this.currenUser = JSON.parse(localStorage.getItem('user'))._id;
    this.createForm();
  }


  createForm() {
    this.searchInvoiceProductForm = this.fb.group({
      invoiceNumber: ['', Validators.required],
    });
  }

  getInvoiceProduct() {
    const reqData = {
      invoiceNo: this.searchInvoiceProductForm.value.invoiceNumber,
      vendorId: this.currenUser
    };
    this.apiService.post('/api/v1/invoiceProduct', reqData).subscribe((response: any) => {

      if (response.status === 200) {
        this.product = response.data;
        if (this.product) {
          this.toastr.error('This item has already been redeemed !', 'Error');
        }
      } else {
        this.toastr.error('You are not authorized to view this invoice', 'Error');
      }
    });
  }

  accept() {
    const reqData = {
      invoiceNo: this.searchInvoiceProductForm.value.invoiceNumber
    };
    this.apiService.post('/api/v1/acceptInvoice', reqData).subscribe((response: any) => {
      if (response.status === 200) {
        this.product = [];
        this.searchInvoiceProductForm.setValue({ invoiceNumber: '' });
        this.toastr.success(this.messageHandler[response.message], 'Success');
      } else {
        this.toastr.error(this.messageHandler[response.message], 'Error');
      }
    });
  }

  decline() {
    const reqData = {
      invoiceNo: this.searchInvoiceProductForm.value.invoiceNumber
    };
    this.apiService.post('/api/v1/declineInvoice', reqData).subscribe((response: any) => {
      if (response.status === 200) {
        this.product = [];
        this.searchInvoiceProductForm.setValue({ invoiceNumber: '' });
      } else {
        this.toastr.error(this.messageHandler[response.message], 'Error');
      }
    });
  }
}
