import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';

import { ToastrService } from 'ngx-toastr';
import { AppServicesService } from './../../../shared/service/app-services.service';
import { constants } from './../../../constants/const';

@Component({
  selector: 'app-add-survey',
  templateUrl: './add-survey.component.html',
  styleUrls: ['./add-survey.component.css']
})
export class AddSurveyComponent implements OnInit {

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private apiService: AppServicesService,
    private toastr: ToastrService
  ) { }

  addSurveyForm: FormGroup;
  myFormValueChanges$;
  isLoading: Boolean = false;
  currenUser: any;
  currenUserRole: any;
  isEdit: Boolean = false;
  messageHandler: any = constants.constMessages;
  surveyCategories: any = [];
  surveyId: any;
  categories: any = [];

  ngOnInit() {
    this.currenUser = JSON.parse(localStorage.getItem('user'));
    this.currenUserRole = JSON.parse(localStorage.getItem('user')).role;
    this.getSurveyCategory(this.currenUser);
    this.route.paramMap.subscribe(paramsData => {
      if (paramsData.get('id')) {
        this.isEdit = true;
        this.surveyId = paramsData.get('id');
        this.getSurvey();
      } else {
        this.isEdit = false;
      }
      this.createForm();
    });
  }

  createForm() {
    let formFields = {
      question: ['', [Validators.required]],
      surveyCategory: ['', Validators.required],
      questionType: ['multipleChoice', Validators.required],
      option: this.fb.array([])
    }
    if (!this.isEdit) {  
      formFields['option']= this.fb.array([this.getOptions('')]);
    }
    this.addSurveyForm = this.fb.group(formFields);
  }

  /**
   * Create form Options
   */
  private getOptions(label) {
    return this.fb.group({
      label: [label, [Validators.required]]
    });
  }

    /**
   * Add new unit row into form
   */
  addOptions(value) {
    const control = <FormArray>this.addSurveyForm.controls['option'];
    control.push(this.getOptions(value));
  }
  /**
   * Remove Options row from form on click delete button
   */
  removeOptions() {
    const control = <FormArray>this.addSurveyForm.controls['option'];
    control.removeAt(control.value.length - 1);
  }

  getSurvey() {
    this.apiService.get('/api/v1/survey/', this.surveyId).subscribe((response: any) => {
      if (response.data && response.status === 200) {
        if(response.data.options && response.data.options.length) {          
          response.data.options.map(item => {
            this.addOptions(item.label);
          })
        }
        this.addSurveyForm.setValue({
          question: response.data.question,
          questionType: response.data.questionType,
          option: response.data.options,
          surveyCategory: response.data.surveyCategoryId._id
        });
      } else {
        this.toastr.error(this.messageHandler[response.message], 'Error');
      }
    }, (errorResult) => {
      this.errorHandling(errorResult);
    });
  }

  getSurveyCategory(adminId: any) {
    const reqData = { adminId: this.currenUser, role:this.currenUserRole}
    this.apiService.post('/api/v1/survey_category', reqData).subscribe( (response: any) => {
      if (response.data && response.status === 200) {
        this.surveyCategories = response.data.data;
        this.addSurveyForm.controls.surveyCategory.setValue(this.surveyCategories[0]._id);
      } else {
        this.toastr.error(this.messageHandler[response.message], 'Error');
      }
    }, (errorResult) => {
      this.errorHandling(errorResult);
    });
  }


  resetForm() {
    this.addSurveyForm = this.fb.group({});
    this.createForm();
  }

  submitForm() {
    const reqData = this.addSurveyForm.value;
    reqData.adminId = this.currenUser._id;
    if (this.isEdit) {
      reqData['surveyId'] = this.surveyId;
    }
    this.apiService.post('/api/v1/survey/add', reqData).subscribe( (response: any) => {
      if (response.status === 200) {
        this.toastr.success(this.messageHandler[response.message], 'Success');
        if (this.isEdit) {  
          this.router.navigate(['/admin/survey']);
        }
        this.addSurveyForm.reset();
      } else {
        this.toastr.error(this.messageHandler[response.message], 'Error');
      }
    }, (errorResult) => {
      this.errorHandling(errorResult);
    });
  }

  errorHandling(error) {
    try {
      this.isLoading = false;
      const errorObj = error ? JSON.parse(error) : '';
      this.toastr.error(errorObj.message, 'Error');
    } catch (error) {
      this.toastr.error(error.message, 'Error');
    }
  }

}
