import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListSurveyCategoryComponent } from './list-survey-category/list-survey-category.component';
import { ListSurveyComponent } from './list-survey/list-survey.component';
import { AddSurveyCategoryComponent } from './add-survey-category/add-survey-category.component';
import { AddSurveyComponent } from './add-survey/add-survey.component';
import { ResultComponent } from './result/result.component';

const routes: Routes = [{
  path: '',
  children : [{
    path: '',
    component: ListSurveyComponent
  }, {
    path : 'add-survey',
    component: AddSurveyComponent
  }, {
    path : 'edit-survey/:id',
    component: AddSurveyComponent
  },
  {
    path : 'list-survey-category',
    component : ListSurveyCategoryComponent
  }, {
    path : 'add-survey-category',
    component : AddSurveyCategoryComponent
  }, {
    path : 'edit-survey-category/:id',
    component : AddSurveyCategoryComponent
  }, {
    path : 'result',
    component : ResultComponent
  }]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SurveyRoutingModule { }
