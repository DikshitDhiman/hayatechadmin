import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartsModule } from 'ng2-charts';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CalendarModule } from 'primeng/calendar';
import { PaginatorModule } from 'primeng/paginator';
import { SharedModule } from '../../shared/shared.module';

import { SurveyRoutingModule } from './survey-routing.module';
import { AddSurveyComponent } from './add-survey/add-survey.component';
import { ListSurveyComponent } from './list-survey/list-survey.component';
import { AddSurveyCategoryComponent } from './add-survey-category/add-survey-category.component';
import { ListSurveyCategoryComponent } from './list-survey-category/list-survey-category.component';
import { ResultComponent } from './result/result.component';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';

@NgModule({
  declarations: [
    AddSurveyComponent, 
    ListSurveyComponent, 
    AddSurveyCategoryComponent, 
    ListSurveyCategoryComponent,
    ResultComponent
  ],
  imports: [
    CommonModule,
    SurveyRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    CalendarModule,
    PaginatorModule,
    SharedModule,
    ChartsModule,
    AngularMultiSelectModule
  ]
})
export class SurveyModule { }
