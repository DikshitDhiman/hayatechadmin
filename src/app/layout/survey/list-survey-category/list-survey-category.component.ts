import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { ToastrService } from 'ngx-toastr';
import Swal from 'sweetalert2';
import { fromEvent } from 'rxjs';
import { debounceTime, map } from 'rxjs/operators';
import { AppServicesService } from './../../../shared/service/app-services.service';
import { environment } from './../../../../environments/environment';
import { constants } from './../../../constants/const';

@Component({
  selector: 'app-list-survey-category',
  templateUrl: './list-survey-category.component.html',
  styleUrls: ['./list-survey-category.component.css']
})
export class ListSurveyCategoryComponent implements OnInit {

  data: any[];
  search: String;
  totalItems = 0;
  page = 1;
  isLoading: Boolean = false;
  siteURL: String =  environment.siteURL;
  currentUser: any;
  currenUserRole: any;
  itemsPerPage: any = constants.recordsPageLimit;
  totalRecords: any;
  openToggle: Boolean = false;
  sort: String = 'name';
  actions: Array<any>;
  status: Array<any>;
  messageHandler: any = constants.constMessages;
  items = [];
  column: Array<any> = [
    { id: 'name', name: 'Name', sorting: true, type: 'string', display: 'left', order: 'neutral' },
    { id: 'corporateId', child: 'name', name: 'Company', sorting: false, type: 'object', display: 'left', order: 'neutral' },
    { id: 'earningToken', name: 'Earning Token', sorting: false, type: 'number', display: 'left', order: 'neutral' },
    { id: 'expireOn', name: 'Expire On', sorting: false, type: 'date', display: 'left', order: 'neutral' },
    { name: 'Status', type: 'status', sorting: false, display: 'center' },
    { name: 'Action', sorting: false, type: 'string', display: 'center' }
  ];
  pageLimit: any = constants.recordsPageLimit;
  requestDataCorporate: any;

  constructor(
    private router: Router,
    private apiService: AppServicesService,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    this.currentUser = JSON.parse(localStorage.getItem('user'))._id;
    this.currenUserRole = JSON.parse(localStorage.getItem('user')).role;
    this.requestDataCorporate = {
      role: this.currenUserRole,
      adminId: this.currentUser,
      page: 0,
      pageLimit: this.pageLimit,
      skip: 0
    }
    this.getSurveyCategory(this.requestDataCorporate);

    this.status = [{
      action: (item) => this.changeStatus(item['_id'], item['is_active'])
    }];

    this.actions = [{
      parentClass: 'edit',
      action: (item) => this.editItem(item['_id']),
      title: 'Edit',
      childClass: 'icofont-ui-edit'
    }, {
      parentClass: 'delete',
      action: (item) => this.deleteSurveyCategory(item['_id']),
      title: 'Delete',
      childClass: 'icofont-ui-delete'
    }];

    setTimeout( () => {
      this.keyUpSearch();
    }, 1000)
  }

  keyUpSearch(){
    const searchBox = document.getElementById('searchInputForSurveyCat');
    const keyup$ = fromEvent(searchBox, 'keyup');
    keyup$.pipe(
      map((i: any) => i.currentTarget.value),
      debounceTime(1000)
    ).subscribe(searchQuery => {
      this.searchSurveyCat(searchQuery);
    });
  }

  searchSurveyCat(searchQuery){
    this.requestDataCorporate = {
      role: this.currenUserRole,
      adminId: this.currentUser,
      searchQuery: searchQuery,
      page: 0, 
      pageLimit: this.itemsPerPage, 
      skip: 0
    };
    this.getSurveyCategory(this.requestDataCorporate);
  }

  getSurveyCategory(data) {
    this.apiService.post('/api/v1/survey_category', data).subscribe( (response: any) => {
      if (response.data && response.status === 200) {
        console.log(response.data.data);
        if (response.data.data && (response.data.data.length) > 0) {
          this.data = response.data.data;
        } else {
          this.data = [];
        }
        console.log(this.data.length);
        console.log(this.data)
        this.totalItems = this.data.length;
        this.sliceList(this.page, this.data);
      } else {
        this.toastr.error(this.messageHandler[response.message], 'Error');
      }
    }, (errorResult) => {
      this.errorHandling(errorResult);
    });
  }


  sliceList(page, data) {
    const start = (page - 1) * this.itemsPerPage;
    const end = start + this.itemsPerPage;
    this.items = data.slice(start, end);
  }

  sortData(field, order) {
    this.column = this.column.map(x => field === x['id'] ? { ...x, 'order': order } : x);
    if (order === 'asc') {
      this.items = this.items.sort((a, b) => a[field].localeCompare(b[field]))
    } else {
      this.items = this.items.sort((a, b) => b[field].localeCompare(a[field]))
    }
  }

  onPageChange(page) {
    if (this.search && this.search.length) {
      this.sliceList(page, this.items);
    } else {
      this.sliceList(page, this.data);
    }
  }

  paginate(event) {
    const page = event.page + 1;
    if (this.search && this.search.length) {
      this.sliceList(page, this.items);
    } else {
      this.sliceList(page, this.data);
    }
  }

  editItem(id) {
    this.router.navigate([`/survey/edit-survey-category/${id}`]);
  }

  changeStatus(id, status) {
    status = status ? false : true;
    const reqData = {
      id: id,
      status: status
    };
    this.apiService.post('/api/v1/survey_category/change_status', reqData).subscribe((response: any) => {
      if (response.status === 200) {
        this.toastr.success('Status changed successfully', 'Success');
        this.getSurveyCategory(this.requestDataCorporate);
      } else {
        this.toastr.error(this.messageHandler[response.message], 'Error');
      }
    });
  }

  deleteSurveyCategory(id: any) {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        this.apiService.delete('/api/v1/survey_category/delete/', id).subscribe((response: any) => {
          if (response.status === 200) {
            this.toastr.success(this.messageHandler[response.message], 'Success');
            this.getSurveyCategory(this.requestDataCorporate);
          } else {
            this.toastr.error(this.messageHandler[response.message], 'Error');
          }
        }, (errorResult) => {
          this.errorHandling(errorResult);
        });
      }
    });
  }


  switchToggle() {
    this.openToggle = !this.openToggle;
  }

  errorHandling(error) {
    try {
      this.isLoading = false;
      const errorObj = error ? JSON.parse(error) : '';
      this.toastr.error(errorObj.message, 'Error');
    } catch (error) {
      this.toastr.error(error.message, 'Error');
    }
  }

}
