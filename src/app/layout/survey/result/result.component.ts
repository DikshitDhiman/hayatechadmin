import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

import { ChartType, ChartOptions } from 'chart.js';
import { SingleDataSet, Label, monkeyPatchChartJsLegend, monkeyPatchChartJsTooltip } from 'ng2-charts';
//import * as pluginDataLabels from 'chartjs-plugin-datalabels';

import jsPDF from 'jspdf';
import htmlToImage from 'html-to-image';

import { AppServicesService } from './../../../shared/service/app-services.service';
import { constants } from './../../../constants/const';




@Component({
  selector: 'survey-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.css']
})
export class ResultComponent implements OnInit {
  @ViewChild('pdfTable', { static: false }) pdfTable: ElementRef;


  // Pie
  public pieChartOptions: ChartOptions = {
    responsive: true,
  };
  public pieChartLabels: Label[] = [];
  public pieChartData = [];
  public pieChartType: ChartType = 'pie';
  public pieChartLegend = true;
  public pieChartPlugins = [];

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private apiService: AppServicesService,
    private toastr: ToastrService
  ) {
    monkeyPatchChartJsTooltip();
    monkeyPatchChartJsLegend();
  }

  resultForm: FormGroup;
  isLoading: Boolean = true;
  displayForm: Boolean = true;
  displayResult: Boolean = false;
  showDepartment: Boolean = false;
  printBtn: Boolean = false;
  printMsg: Boolean = false;
  noData: Boolean = false;
  messageHandler: any = constants.constMessages;
  results: any = [];
  chartData: any = [];
  chartLabel: any = [];
  companies: any = [];
  categories: any = [];
  currentUser: any;
  departments: any = [];
  selectedDept: String = '';
  selectedCorp: String = '';
  selectedComp: String = '';
  selectedSurvey: String = '';
  //compDepartments: any = [{ id: '', itemName: 'Select corporate first' }];
  compDepartments: any = [];
  selectedDepartments = [];



  ngOnInit() {
    this.currentUser = JSON.parse(localStorage.getItem('user'));

    this.getDepartments();
    this.getCategory();
    this.createForm();
  }

  createForm() {
    this.resultForm = this.fb.group({
      survey: ['', Validators.required],
      company: [this.currentUser._id, Validators.required],
      department: ['', Validators.required]
    });

    if (this.currentUser.role === "corporate") {
      this.resultForm.get('company').disable();
    } else {
      this.resultForm.get('company').enable();
    }
  }

  getDepartments() {
    this.apiService.post('/api/v1/groupCompanyDepartments', { adminId: this.currentUser._id }).subscribe((response: any) => {
      if (response.data && response.status === 200) {
        this.departments = response.data;
        console.log('depar ', this.departments)
        this.companies = (this.departments).map(function (val, index) {
          return val;
        })
        console.log("companies--", this.companies);
        //(this.companies).unshift({ label: "Select company", _id: "" });
        //this.selectedComp = this.companies[0]._id
        this.selectedComp = this.currentUser._id
        this.onCompanySelect(this.selectedComp);
      } else {
        this.toastr.error(this.messageHandler[response.message], 'Error');
      }
    }, (errorResult) => {
      this.errorHandling(errorResult);
    });
  }

  getCategory() {
    console.log('here n get catefogur');

    this.apiService.get('/api/v1/survey_category_active', '').subscribe((response: any) => {
      if (response.data && response.status === 200) {
        if (response.data && (response.data.length) > 0) {
          this.categories = response.data;
          this.resultForm.controls.survey.setValue(this.categories[0]._id);
          // this.selectedSurvey = this.categories[0]._id;
          // this.resultForm.value.survey = this.selectedSurvey;
        } else {
          this.categories = [];
        }
        //(this.categories).unshift({ name: "Select survey", _id: "" })
      } else {
        this.toastr.error(this.messageHandler[response.message], 'Error');
      }
    }, (errorResult) => {
      this.errorHandling(errorResult);
    });
  }

  setSurvey(option: any) {
    this.resultForm.controls.survey.setValue(option.value);
  }

  onCompanySelect(option: any) {
    this.compDepartments = [];
    this.selectedComp = option;
    console.log('this.selectedComp ', this.selectedComp)
    const found = (this.departments).find(element => element._id == option);
    if (found && found.items) {
      this.compDepartments = (found.items).map(function (val, index) {
        return { id: val.value, itemName: val.label };
      });

      this.selectedDepartments = (found.items).map(function (val, index) {
        return { id: val.value, itemName: val.label };
      });


      //(this.compDepartments).unshift({ itemName: "Select department", id: "" })
    }
    this.showDepartment = true;
  }

  submitForm() {
    this.resultForm.value.company = this.selectedComp;

    // Hayatech - Call center - Hayatech engagement survey
    console.log(">>>>>>>>>>>>>>>", this.resultForm.value);
    // console.log(this.resultForm.value.department);

    this.displayForm = false;
    this.displayResult = true;

    this.apiService.get('/api/v1/corporate/' + this.resultForm.value.company, '').subscribe((corp: any) => {
      if (corp.data && corp.status === 200) {
        this.selectedCorp = corp.data.name;
        if (this.resultForm.value.department.length > 0) {
          let departmentIds = [];
          this.resultForm.value.department.forEach((element, index) => {
            departmentIds.push(element.id);

            this.selectedDept += element.itemName;
            if (this.resultForm.value.department.length > index + 1)
              this.selectedDept += ', ';
          });

          this.apiService.post('/api/v1/survey_result', { departments: departmentIds, surveyCatId: this.resultForm.value.survey }).subscribe((response: any) => {
            if (response.data && response.status === 200) {
              this.results = response.data;
              console.log("this.results.questions", this.results.questions)
              if (this.results.questions && this.results.questions.length > 0) {
                this.results.questions.forEach(element => {
                  let data = []
                  let label = [];
                  element.options.forEach(ele => {
                    label.push(ele.label);
                    data.push(ele.percent);
                  });
                  this.pieChartData.push(data);
                  this.pieChartLabels.push(label);

                  this.noData = false;
                  this.printBtn = true;
                });
              } else {
                this.noData = true;
                this.printBtn = false;
              }
            } else {
              this.toastr.error(this.messageHandler[response.message], 'Error');
            }
          }, (errorResult) => {
            this.errorHandling(errorResult);
          });
        }
      }
    }, (errorResult) => {
      this.errorHandling(errorResult);
    });
  }

  showForm() {
    this.displayForm = true;
  }

  hideForm() {
    this.displayForm = false;
  }


  public async downloadAsPDF() {
    this.printBtn = false;
    this.printMsg = true;
    const doc = new jsPDF();
    doc.setFont("helvetica");
    doc.setFontType("bold");
    doc.setFontSize(15);

    let textYxis = 60;
    let imgyaxis = 65;
    let count = 3;

    doc.text(this.results.name.toUpperCase(), 10, 15);
    doc.setFontType("normal");
    doc.setFontSize(12);

    let selection = await htmlToImage.toPng(document.getElementById('selected'));
    doc.addImage(selection, 'JPEG', 10, 25, 75, 30);

    const index = this.results.questions.length;
    for (let i = 0; i < index; i++) {
      console.log("countcount", count)
      if ((i + 1) <= count) {
        doc.text("Question " + (i + 1) + ': ' + this.results.questions[i].question, 10, textYxis);

        let table = await htmlToImage.toPng(document.getElementById('mytable' + i));
        doc.addImage(table, 'JPEG', 10, imgyaxis, 75, 50);

        let image = await htmlToImage.toPng(document.getElementById('graph' + i));
        doc.addImage(image, 'JPEG', 100, imgyaxis, 110, 57);

        textYxis = textYxis + 70;
        imgyaxis = imgyaxis + 70;
        //console.log('indexindex', index)
        if ((i + 1) % 2 == 0) {

          doc.addPage();
          textYxis = 10;
          imgyaxis = 15;
        }
      } else {
        //
        count = count + 3;



      }
    }

    doc.autoPrint();
    this.printMsg = false;
    doc.save('table.pdf');

    /* setTimeout(function(){
      this.printMsg = false;
    }, 3000); */
  }

  errorHandling(error) {
    try {
      this.isLoading = false;
      const errorObj = error ? JSON.parse(error) : '';
      this.toastr.error(errorObj.message, 'Error');
    } catch (error) {
      this.toastr.error(error.message, 'Error');
    }
  }

}
