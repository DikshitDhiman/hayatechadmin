import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

import { AppServicesService } from './../../../shared/service/app-services.service';
import { constants } from './../../../constants/const';
@Component({
  selector: 'app-add-survey-category',
  templateUrl: './add-survey-category.component.html',
  styleUrls: ['./add-survey-category.component.css']
})
export class AddSurveyCategoryComponent implements OnInit {

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private apiService: AppServicesService,
    private toastr: ToastrService
  ) { }

  addSurveyCategoryForm: FormGroup;
  isLoading: Boolean = false;
  isEdit: Boolean = false;
  messageHandler: any = constants.constMessages;
  currenUser: any;
  currenUserRole: any;
  surveyCategoryId: any;
  categories: any = [];
  mindate: any = new Date();
  todayYear = this.mindate.getFullYear();
  corporates: any = [];

  ngOnInit() {
    this.currenUser = JSON.parse(localStorage.getItem('user'))._id;
    this.currenUserRole = JSON.parse(localStorage.getItem('user')).role;
    this.createForm();
    this.route.paramMap.subscribe(paramsData => {
      this.getCorporates();
      if (paramsData.get('id')) {
        this.isEdit = true;
        this.surveyCategoryId = paramsData.get('id');
        this.getSurveyCategory();
      } else {
        this.isEdit = false;
      }
    });
  }
  
  createForm() {
    this.addSurveyCategoryForm = this.fb.group({
      name: ['', Validators.required],
      corporate: ['', Validators.required],
      earningToken: ['', Validators.required],
      is_featured: [''],
      expireOn: [new Date(), Validators.required]
    });
  }

  getCorporates() {
    if (this.currenUserRole === 'corporate') {
      this.apiService.get('/api/v1/corporate/', this.currenUser).subscribe((response: any) => {
        if (response.status === 200 && response.data) {
          this.corporates.push(response.data);
          this.addSurveyCategoryForm.controls.corporate.setValue(this.corporates[0]._id);
        } else {
          this.toastr.error(this.messageHandler[response.message], 'Error');
        }
      }, (errorResult) => {
        this.errorHandling(errorResult);
      });
    } else {
      const reqData = { adminId: this.currenUser }
      this.apiService.post('/api/v1/corporates', reqData).subscribe((response: any) => {
        if (response.status === 200 && response.data) {
          this.corporates = response.data.data;
          this.addSurveyCategoryForm.controls.corporate.setValue(this.corporates[0]._id);
        } else {
          this.toastr.error(this.messageHandler[response.message], 'Error');
        }
      }, (errorResult) => {
        this.errorHandling(errorResult);
      });
    }
  }

  getSurveyCategory() {
    this.apiService.get('/api/v1/survey_category/', this.surveyCategoryId).subscribe((response: any) => {
      if (response && response.status === 200) {
        console.log(response.data);
        const expiryDate = new Date(response.data.expireOn);
        this.addSurveyCategoryForm.setValue({
          name: response.data.name,
          corporate: response.data.corporateId,
          earningToken: response.data.earningToken,
          is_featured: response.data.is_featured,
          expireOn: expiryDate
        });
      } else {
        this.toastr.error(this.messageHandler[response.message], 'Error');
      }
    }, (errorResult) => {
      this.errorHandling(errorResult);
    });
  }

  keyPress(event: any) {
    const pattern = /[0-9\+\-\ ]/;
    const inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode !== 3 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }


  resetForm() {
    this.addSurveyCategoryForm = this.fb.group({});
    this.createForm();
  }

  submitForm() {
    const reqData =  this.addSurveyCategoryForm.value;
    //reqData['adminId'] = this.currenUser;
    if (this.isEdit) {
      reqData['surveyCategoryId'] = this.surveyCategoryId;
    }
    this.apiService.post('/api/v1/survey_category/add', reqData).subscribe( (response: any) => {
      if (response.status === 200) {
        this.toastr.success(this.messageHandler[response.message], 'Success');
        this.router.navigate(['/admin/survey/list-survey-category/']);
        this.addSurveyCategoryForm.reset();
      } else {
        this.toastr.error(this.messageHandler[response.message], 'Error');
      }
    }, (errorResult) => {
      this.errorHandling(errorResult);
    });
  }

  errorHandling(error){
    try {
      this.isLoading = false;
      const errorObj = error ? JSON.parse(error) : '';
      this.toastr.error(errorObj.message, 'Error');
    } catch (error) {
      this.toastr.error(error.message, 'Error');
    }
  }

}
