import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PasswordComponent } from './password/password.component';
import { AddUsersComponent } from './add-users/add-users.component';
import { ListUsersComponent } from './list-users/list-users.component';
import { InactiveUsersComponent } from './inactive-users/inactive-users.component';


const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        component: ListUsersComponent
      },
      {
        path: 'add-users',
        component: AddUsersComponent
      },
      {
        path: 'edit-users/:id',
        component: AddUsersComponent
      },
      {
        path: 'company-users/:id',
        component: ListUsersComponent
      },
      {
        path: 'password',
        component: PasswordComponent
      },
      {
        path: 'inactive-users',
        component: InactiveUsersComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersRoutingModule { }
