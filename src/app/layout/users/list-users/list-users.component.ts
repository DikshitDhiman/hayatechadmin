import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import Swal from 'sweetalert2';
import { fromEvent } from 'rxjs';
import { debounceTime, map } from 'rxjs/operators';
import { AppServicesService } from './../../../shared/service/app-services.service';
import { constants } from './../../../constants/const';

@Component({
  selector: 'app-list-users',
  templateUrl: './list-users.component.html',
  styleUrls: ['./list-users.component.css']
})
export class ListUsersComponent implements OnInit {

  users: any = [];
  page = 1;
  search: String;
  totalItems = 0;
  corporateId: any;
  searchQuery: String = '';
  corporateUser: Boolean = false;
  itemsPerPage: any = constants.recordsPageLimit;
  isLoading: Boolean = false;
  sort: String = 'companyName';
  currentUser: any;
  actions: Array<any>;
  status: Array<any>;
  openToggle: Boolean = false;
  messageHandler: any = constants.constMessages;
  items = [];
  column: Array<any> = [
    { id: 'image', name: 'Image', sorting: false, type: 'image', display: 'center' },
    { id: 'firstName', name: 'Firstname', sorting: true, type: 'string', display: 'left', order: 'neutral' },
    { id: 'lastName', name: 'Lastname', sorting: true, type: 'string', display: 'left', order: 'neutral' },
    { id: 'email', name: 'Email', sorting: true, type: 'email', display: 'left', order: 'neutral' },
    { id: 'corporateId', child: 'name', name: 'Company', sorting: false, type: 'object', display: 'left', order: 'neutral' },
    { id: 'department', child: 'name', name: 'Department', sorting: false, type: 'object', display: 'left', order: 'neutral' },
    { id: 'earnedToken', name: 'Earned Tokens', sorting: true, type: 'number', display: 'left', order: 'neutral' },
    { id: 'dob', name: 'DOB', sorting: false, type: 'string', display: 'center' },
    { id: 'mobile', name: 'Mobile', sorting: false, type: 'number', display: 'center' },
    { name: 'Status', type: 'status', sorting: false, display: 'center' },
    { name: 'Action', sorting: false, type: 'string', display: 'center' }
  ];

  pageLimit: any = constants.recordsPageLimit;
  requestData: any;


  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private apiService: AppServicesService,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    this.currentUser = JSON.parse(localStorage.getItem('user'));
    this.requestData = {
      page: 0,
      pageLimit: this.pageLimit,
      skip: 0
    };
    this.route.paramMap.subscribe(paramsData => {
      if (paramsData.get('id')) {
        this.corporateUser = true;
        this.requestData['role'] = 'corporate';
        this.requestData['roleId'] = paramsData.get('id');
      } else {
        this.corporateUser = false;
        this.requestData['role'] = this.currentUser.role;
        this.requestData['roleId'] = this.currentUser._id;
      }
      this.getUser(this.requestData);
    });

    this.status = [{
      action: (item) => this.changeStatus(item['_id'], item['is_active']),
    }];

    this.actions = [{
      parentClass: 'edit',
      action: (item) => this.editItem(item['_id']),
      title: 'Edit',
      childClass: 'icofont-ui-edit'
    }, {
      parentClass: 'delete',
      action: (item) => this.deleteItem(item['_id']),
      title: 'Delete',
      childClass: 'icofont-ui-delete'
    }];

    setTimeout(() => {
      this.keyUpSearch();
    }, 1000);
  }

  getUser(data) {
    this.apiService.post('/api/v1/users', data).subscribe((response: any) => {
      if (response.status === 200 && response.data) {
        this.users = response.data;
        console.log('  this.users ',  this.users)
        /* this.users.data.map(user => {
          console.log('new Date(user.dob) : ', new Date(user.dob), typeof new Date(user.dob), typeof user.dob, user._id);
          user.dob = new Date(user.dob);
        }); */
      } else {
        this.toastr.error(this.messageHandler[response.message], 'Error');
      }
    }, (errorResult) => {
      this.errorHandling(errorResult);
    });
  }

  keyUpSearch() {
    const searchBox = document.getElementById('searchInputForUsers');
    if (searchBox){
      const keyup$ = fromEvent(searchBox, 'keyup');
      keyup$.pipe(
        map((i: any) => i.currentTarget.value),
        debounceTime(1000)
      ).subscribe(searchQuery => {
        this.searchRewards(searchQuery);
      });
    }
  }

  searchRewards(searchQuery) {
    this.searchQuery = searchQuery;
    this.requestData = {
      role: this.currentUser.role,
      roleId: this.currentUser._id,
      searchQuery: searchQuery,
      page: 0,
      pageLimit: this.pageLimit,
      skip: 0
    }
    this.getUser(this.requestData);
  }

  sortData(field, order) {
    console.log('field : ', field);
    this.column = this.column.map(x => field === x['id'] ? { ...x, 'order': order } : x);
    if (order === 'asc') {
      this.users.data = this.users.data.sort((a, b) => {
        console.log('Here a and b : ', a , b);
        a[field].localeCompare(b[field]);
      });
    } else {
      this.users.data = this.users.data.sort((a, b) => b[field].localeCompare(a[field]));
    }
  }



  paginate(event) {
    const reqData = {
      role: this.currentUser.role,
      roleId: this.currentUser._id,
      page: event.page,
      searchQuery: this.searchQuery,
      pageLimit: this.pageLimit,
      pageCount: event.pageCount,
      skip: 0
    };
    this.getUser(reqData);
  }

  editItem(id) {
    this.router.navigate([`/users/edit-users/${id}`]);
  }

  changeStatus(id, status) {
    status = status ? false : true;
    const reqData = {
      id: id,
      status: status
    };
    this.apiService.post('/api/v1/user/change_status', reqData).subscribe((response: any) => {
      if (response.status === 200) {
        this.toastr.success('Status changed successfully', 'Success');
        this.getUser(this.requestData);
      } else {
        this.toastr.error('Error while deleting', 'Error');
      }
    });
  }

  deleteItem(id: any) {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        this.apiService.delete('/api/v1/user/delete/', id).subscribe((response: any) => {
          if (response.status === 200) {
            this.toastr.success(this.messageHandler[response.message], 'Success');
            this.getUser(this.requestData);
          } else {
            this.toastr.error(this.messageHandler[response.message], 'Error');
          }
        }, (errorResult) => {
          this.errorHandling(errorResult);
        });
      }
    });
  }

  errorHandling(error) {
    try {
      this.isLoading = false;
      const errorObj = error ? JSON.parse(error) : '';
      this.toastr.error(errorObj.message, 'Error');
    } catch (error) {
      this.toastr.error(error.message, 'Error');
    }
  }

}
