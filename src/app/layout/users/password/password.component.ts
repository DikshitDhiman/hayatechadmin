import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { CustomValidation } from '../../../shared/service/custom-validators';
import { AppServicesService } from './../../../shared/service/app-services.service';
import { constants } from './../../../constants/const';

@Component({
  selector: 'change-password-users',
  templateUrl: './password.component.html',
  styleUrls: ['./password.component.css']
})
export class PasswordComponent implements OnInit {

  passForm: FormGroup;
  isLoading: Boolean = false;
  isEdit: Boolean = false;
  messageHandler: any = constants.constMessages;
  currentUser: any;
  userId: any;
  email: any;
  departments: any = [];
  companyDepartments: any = [];
  corporates: any = [];
  maxdate: any = new Date();
  todayYear = this.maxdate.getFullYear();

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private apiService: AppServicesService,
    private toastr: ToastrService,
  ) { }

  ngOnInit() {
    this.createForm();
    
    this.currentUser = JSON.parse(localStorage.getItem('user'));
    console.log(this.currentUser);
    
  }

  createForm() {
    this.passForm = this.fb.group({
      curpass: ['', Validators.required],
      newpass: ['', Validators.required],
      verpass: ['', Validators.required]
    });
  }

  get check() { return this.passForm.controls; }

  keyPress(event: any) {
    const pattern = /[0-9\+\-\ ]/;
    const inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode !== 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }

  resetForm() {
    this.passForm = this.fb.group({});
    this.createForm();
  }

  submitForm() {
    const reqData = {
      curpass: this.passForm.value.curpass,
      newpass: this.passForm.value.newpass
    };

    reqData['userId'] = this.currentUser._id;
    reqData['role'] = this.currentUser.role;
    console.log("reqData--",reqData);

    if (this.passForm.value.newpass != this.passForm.value.verpass) {
      this.toastr.error('New password and confirm password does not match.', 'Error');
    }

    this.apiService.post(`/api/v1/admin-change-password`, reqData).subscribe((response: any) => {
      if (response && response.status === 200) {
        this.passForm.reset();
        this.toastr.success(response.message, 'Success');

        localStorage.clear();
        this.router.navigateByUrl('/');
      } else {
        this.toastr.error(response.message, 'Error');
      }
    });
  }

  errorHandling(error) {
    try {
      this.isLoading = false;
      const errorObj = error ? JSON.parse(error) : '';
      this.toastr.error(errorObj.message, 'Error');
    } catch (error) {
      this.toastr.error(error.message, 'Error');
    }
  }
}
