import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { CustomValidation } from '../../../shared/service/custom-validators';
import { AppServicesService } from './../../../shared/service/app-services.service';
import { constants } from './../../../constants/const';

@Component({
  selector: 'app-add-users',
  templateUrl: './add-users.component.html',
  styleUrls: ['./add-users.component.css']
})
export class AddUsersComponent implements OnInit {

  addUserForm: FormGroup;
  isLoading: Boolean = false;
  isEdit: Boolean = false;
  messageHandler: any = constants.constMessages;
  currentUser: any;
  userId: any;
  email: any;
  departments: any = [];
  selectedDept: String = '';
  selectedCorp: any;
  companyDepartments: any = [];
  corporates: any = [];
  maxdate: any = new Date();
  todayYear = this.maxdate.getFullYear();
  wearableDevice: String = null;
  wearableDeviceChange: Boolean = false;

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private apiService: AppServicesService,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    this.createForm();
    
    this.currentUser = JSON.parse(localStorage.getItem('user'));
    if (this.currentUser.role === 'admin') {
      this.getCorporates();
      this.addUserForm.controls['corporate'].setValidators([Validators.required]);
    } else {
      this.selectedCorp = this.currentUser._id;
    }
    this.getDepartments();
    this.route.paramMap.subscribe(paramsData => {
      if (paramsData.get('id')) {
        this.isEdit = true;
        this.userId = paramsData.get('id');
        this.getEditUser();
        this.addUserForm.controls['email'].disable();
      } else {
        this.isEdit = false;
      }
    });
  }

  createForm() {
    this.addUserForm = this.fb.group({
      email: ['', [Validators.required]],
      mobile: ['',  [Validators.required, Validators.pattern(/^[0-9]*$/)]],
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      dob: [new Date(), Validators.required],
      department: ['', Validators.required],
      corporate: ['']
    }, {
        validator: CustomValidation.isValidEmail
    });
  }

  get check() { return this.addUserForm.controls; }

  keyPress(event: any) {
    const pattern = /[0-9\+\-\ ]/;
    const inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode !== 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }

  getCorporates() {
    const reqData = {
      adminId: this.currentUser._id
    };
    this.apiService.post('/api/v1/corporates', reqData).subscribe((response: any) => {
      if (response.status === 200 && response.data) {
        this.corporates = response.data.data;
        this.addUserForm.controls.corporate.setValue(this.corporates[0]._id);
      } else {
        this.toastr.error(this.messageHandler[response.message], 'Error');
      }
    }, (errorResult) => {
      this.errorHandling(errorResult);
    });
  }

  getDepartments() {
    this.apiService.post('/api/v1/departments', { adminId: '' }).subscribe((response: any) => {
      if (response.data && response.status === 200) {
        this.departments = response.data.data;
        console.log("this.departments", this.departments);
        this.handleCompanyChange();
      } else {
        this.toastr.error(this.messageHandler[response.message], 'Error');
      }
    }, (errorResult) => {
      this.errorHandling(errorResult);
    });
  }

  setDepartment(option){
    this.selectedDept = option.value;
  }

  getUser() {
    this.apiService.get('/api/v1/user/', this.userId).subscribe((response: any) => {
      if (response.data && response.status === 200) {
        this.email = response.data.email;
        const myDate = new Date(response.data.dob);
        this.addUserForm.setValue({
          email: response.data.email,
          firstName: response.data.firstName,
          lastName: response.data.lastName,
          mobile: response.data.mobile,
          dob: myDate,
          department: (response.data.department) ? response.data.department._id : '',
          corporate: (response.data.corporateId) ? response.data.corporateId._id : ''
        });
      } else {
        this.toastr.error(this.messageHandler[response.message], 'Error');
      }
    }, (errorResult) => {
      this.errorHandling(errorResult);
    });
  }

  getEditUser() {
    this.apiService.get('/api/v1/user/', this.userId).subscribe((response: any) => {
      if (response.data && response.status === 200) {
        console.log("response.data", response.data);
        const dob = response.data.dob.split('/');
        const myDate = dob[1]+'/'+dob[0]+'/'+dob[2];

        this.email = response.data.email;
        const department = (response.data.department) ? response.data.department._id : '';
        const corporate = (response.data.corporateId) ? response.data.corporateId._id : '';
        this.wearableDeviceChange = (response.data.wearableDevice) ? false : true;
        this.wearableDevice = (response.data.wearableDevice) ? response.data.wearableDevice : null;
        this.addUserForm.setValue({
          email: response.data.email,
          firstName: response.data.firstName,
          lastName: response.data.lastName,
          mobile: response.data.mobile,
          dob: myDate,
          department: department,
          corporate: corporate
        });
        this.handleCompanyChange();
      } else {
        this.toastr.error(this.messageHandler[response.message], 'Error');
      }
    }, (errorResult) => {
      this.errorHandling(errorResult);
    });
  }

  resetForm() {
    this.addUserForm = this.fb.group({});
    this.createForm();
  }


  handleCompanyChange() {
    console.log(">>>>>>>>>", this.selectedCorp);
    this.companyDepartments = this.departments.filter(item => {
      if (this.selectedCorp) {
        if (item.corporateId._id === this.selectedCorp) {
          return item;
        }
      } else {
        if (item.corporateId._id === this.addUserForm.value.corporate) {
          return item;
        }
      }
    });

    if (this.addUserForm.value.department) {
      this.addUserForm.controls.department.setValue(this.addUserForm.value.department);
    } else {
      this.addUserForm.controls.department.setValue(this.companyDepartments[0]._id);
    }
  }

  submitForm() {
    const dob = this.convertDate(this.addUserForm.value.dob);

    const reqData = {
      firstName: this.addUserForm.value.firstName,
      lastName: this.addUserForm.value.lastName,
      mobile: this.addUserForm.value.mobile,
      dob: dob,
      department: this.addUserForm.value.department,
    };
    
    if (this.addUserForm.value && this.addUserForm.value.email) {
      reqData['email'] = this.addUserForm.value.email.toLowerCase();
    }

    if (this.currentUser.role === 'admin') {
      reqData['adminId'] = this.currentUser._id;
      reqData['corporateId'] = this.addUserForm.value.corporate;
      reqData['addedBy'] = 'admin';
    } else {
      reqData['corporateId'] = this.currentUser._id;
    }

    if (this.isEdit) {
      reqData['userId'] = this.userId;
      reqData['email'] = this.email;
      if (this.wearableDeviceChange){
        reqData['wearableDevice'] = null;
      }
    }
    console.log('reqData ',reqData)
    this.apiService.post('/api/v1/user/add', reqData).subscribe((response: any) => {
      console.log('add user res ',response)
      if (response && response.status === 200) {
        this.router.navigate(['/admin/users']);
        this.toastr.success(this.messageHandler[response.message], 'Success');
        this.addUserForm.reset();
      } else {
        this.toastr.error(this.messageHandler[response.message], 'Error');
      }
    }, (errorResult) => {
      this.errorHandling(errorResult);
    });
  }

  // convert mm/dd/yyyy to dd/mm/yyyy
  convertDate(inputFormat) {
    function pad(s) { return (s < 10) ? '0' + s : s; }
    var d = new Date(inputFormat)
    return [pad(d.getDate()), pad(d.getMonth()+1), d.getFullYear()].join('/')
  }

  errorHandling(error) {
    try {
      this.isLoading = false;
      const errorObj = error ? JSON.parse(error) : '';
      this.toastr.error(errorObj.message, 'Error');
    } catch (error) {
      this.toastr.error(error.message, 'Error');
    }
  }

  wearableChange() {
    console.log('-----', this.wearableDeviceChange);
    if (this.wearableDeviceChange){
      this.wearableDeviceChange = false;
    } else {
      this.wearableDeviceChange = true;
      /* const reqData = {
        userId: this.userId,
        email: this.email,
        firstName: this.addUserForm.value.firstName,
        lastName: this.addUserForm.value.lastName,
        wearableDevice: null
      };
      this.apiService.post('/api/v1/user/add', reqData).subscribe((response: any) => {
        if (response && response.status === 200) {
          console.log("updated");
          
        } else {
          this.toastr.error(this.messageHandler[response.message], 'Error');
        }
      }, (errorResult) => {
        this.errorHandling(errorResult);
      }); */
    }
    console.log(this.wearableDeviceChange);
  }
}
