import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Ng2GoogleChartsModule } from 'ng2-google-charts';
import { ChartistModule } from 'ng-chartist';
import { CarouselModule } from 'ngx-owl-carousel-o';
import { ChartsModule } from 'ng2-charts';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { CountToModule } from 'angular-count-to';
import { DashboardComponent } from './dashboard.component';

import { CorporateStatsComponent } from '../../shared/stats/corporate-stats/corporate-stats.component';
import { AdminStatsComponent } from '../../shared/stats/admin-stats/admin-stats.component';
import { VendorStatsComponent } from '../../shared/stats/vendor-stats/vendor-stats.component';
import { PreloaderModule } from '../../shared/components/preloader/preloader.module';
import { CalendarModule } from 'primeng/calendar';

@NgModule({
  declarations: [
    DashboardComponent, 
    CorporateStatsComponent,
    AdminStatsComponent,
    VendorStatsComponent,
  ],
  imports: [
    CommonModule,
    Ng2GoogleChartsModule,
    ChartistModule,
    ChartsModule,
    CarouselModule,
    CountToModule,
    DashboardRoutingModule,
    PreloaderModule,
    CalendarModule
  ]
})
export class DashboardModule { }
