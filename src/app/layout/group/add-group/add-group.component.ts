import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { AppServicesService } from '../../../shared/service/app-services.service';
import { constants } from './../../../constants/const';

@Component({
  selector: 'app-add-group',
  templateUrl: './add-group.component.html',
  styleUrls: ['./add-group.component.scss']
})
export class AddGroupComponent implements OnInit {

  groupUserForm: FormGroup;
  isLoading: Boolean = false;
  isEdit: Boolean = false;
  currenUser: any;
  isSubmitted: Boolean = false;
  groupId: String;
  messageHandler: any = constants.constMessages;

  userList: any = [];
  selectedUsers = [];
  settings = {};

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private apiService: AppServicesService,
    private toastr: ToastrService
  ) {
  }

  ngOnInit() {
    this.currenUser = JSON.parse(localStorage.getItem('user'));
    this.createForm();
    this.multiDropdownSetting();
    this.getCorpsUsers();
    this.route.paramMap.subscribe(paramsData => {
      if (paramsData.get('id')) {
        this.isEdit = true;
        this.groupId = paramsData.get('id');
        this.getSingleGroup(this.groupId);
      } else {
        this.isEdit = false;
      }
    });
  }

  getSingleGroup(groupId){
    this.apiService.get(`/api/v1/group/${groupId}`, []).subscribe( (response: any) => {
      if (response.data && response.status === 200) {
        console.log("response.data", response.data)
        this.groupUserForm.setValue({
          name: response.data[0].name,
          userId: '',
        });
        this.selectedUsers = response.data[0].userId
      }
    }, (errorResult) => {
      this.errorHandling(errorResult);
    });
  }



  createForm() {
    this.groupUserForm = this.fb.group({
      name: ['', Validators.required],
      userId: [[], Validators.required]
    });
  }

  getCorpsUsers() {
    return new Promise( (resolve, reject) => {
      this.apiService.post('/api/v1/group/getcorpusers', {adminId: this.currenUser._id}).subscribe( (response: any) => {
        if (response.data && response.status === 200) {
          console.log('corp data ',response)
          resolve(response.data);
        }
      }, (errorResult) => {
        this.errorHandling(errorResult);
      });
    }).then( (data: any) => { 
      this.userList = data;
      console.log("this.userList ",this.userList)
    });
  }

  multiDropdownSetting(){
    this.settings = {
      singleSelection: false,
      labelKey: 'name',
      text: "Select companie's users",
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      searchPlaceholderText: 'Search Fields',
      enableSearchFilter: false,
      badgeShowLimit: 6,
      groupBy: "companyName"
    };
  }

  resetForm() {
    this.groupUserForm = this.fb.group({});
    this.createForm();
  }

  onOpen(env: any) {
    this.userList = [...this.userList];
  }
  onClose(env: any) {
    this.userList = [...this.userList];
  }


  submitForm() {
    const reqData = this.groupUserForm.value;
    const user_id = [];
    this.selectedUsers.forEach(element => {
      user_id.push(element.id);
    });
    reqData.adminId = this.currenUser._id;
    reqData.userId = user_id;

    if (this.isEdit) {
      reqData.groupId = this.groupId;
    }
    if (reqData.userId && reqData.userId.length) {
      this.apiService.post('/api/v1/group/add', reqData).subscribe( (response: any) => {
        if (response.status === 200) {
          this.toastr.success(this.messageHandler[response.message], 'Success');
          this.router.navigate(['/admin/group/']);
          this.groupUserForm.reset();
        } else {
          this.toastr.error(this.messageHandler[response.message], 'Error');
        }
      }, (errorResult) => {
        this.errorHandling(errorResult);
      });
    }
  }

  errorHandling(error: any) {
    try {
      this.isLoading = false;
      const errorObj = error ? JSON.parse(error) : '';
      this.toastr.error(errorObj.message, 'Error');
    } catch (error) {
      this.toastr.error(error.message, 'Error');
    }
  }

}

