import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GroupComponent } from './group.component';
import { AddGroupComponent } from './add-group/add-group.component';

const routes: Routes = [{
  path: '',
  children: [
    {
      path: '',
      component : GroupComponent
    },
    {
      path: 'add-group',
      component : AddGroupComponent
    },
    {
      path: 'edit-group/:id',
      component : AddGroupComponent
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GroupRoutingModule { }
