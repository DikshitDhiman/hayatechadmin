import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { fromEvent } from 'rxjs';
import { debounceTime, map } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import Swal from 'sweetalert2';

import { AppServicesService } from '../../shared/service/app-services.service';
import { environment } from '../../../environments/environment';
import { constants } from '../../constants/const';

@Component({
  selector: 'app-group',
  templateUrl: './group.component.html',
  styleUrls: ['./group.component.scss']
})
export class GroupComponent implements OnInit {
  groups: any = [];
  searchQuery: String = '';
  search: String;
  totalItems = 0;
  page = 1;
  isLoading: Boolean = false;
  currentUser: any;
  itemsPerPage: any = constants.recordsPageLimit;
  messageHandler: any = constants.constMessages;
  totalRecords: any = 0;
  openToggle: Boolean = false;
  sort: String = 'name';
  actions: Array<any>;
  status: Array<any>;

  column: Array<any> = [
    { id: 'name', name: 'Group Name', sorting: true, type: 'string', display: 'left', order: 'neutral' },
    { id: 'users', name: 'Users', sorting: false, type: 'string', display: 'left', order: 'neutral' },
    { name: 'Status', type: 'status', sorting: false, display: 'center' },
    { name: 'Action', sorting: false, type: 'string', display: 'center' }
  ];
  pageLimit: any = constants.recordsPageLimit;
  requestDataGroup: any;

  constructor(
    private apiService: AppServicesService,
    private router: Router,
    private toastr: ToastrService,
  ) { }

  ngOnInit() {
    this.currentUser = JSON.parse(localStorage.getItem('user'))._id;
    this.requestDataGroup = {
      adminId: this.currentUser,
      page: 0,
      pageLimit: this.pageLimit,
      skip: 0
    };
    this.getGroups(this.requestDataGroup);
    this.status = [{
      action: (item) => this.changeStatus(item['_id'], item['is_active'])
    }];
    this.actions = [
      {
      parentClass: 'edit',
      action: (item) => this.editItem(item['_id']),
      title: 'Edit',
      childClass: 'icofont-ui-edit'
      },{
      parentClass: 'delete',
      action: (item) => this.deleteGroup(item['_id']),
      title: 'Delete',
      childClass: 'icofont-ui-delete'
    }];

    setTimeout(() => {
      this.keyUpSearch();
    });
  }

  keyUpSearch() {
    const searchBox = document.getElementById('searchInputForGroups');
    if (searchBox) {
      const keyup$ = fromEvent(searchBox, 'keyup');
      keyup$.pipe(
        map((i: any) => i.currentTarget.value),
        debounceTime(1000)
      ).subscribe(searchQuery => {
        this.searchPromocode(searchQuery);
      });
    }
  }

  searchPromocode(searchQuery) {
    this.requestDataGroup = {
      adminId: this.currentUser,
      searchQuery: searchQuery,
      page: 0,
      pageLimit: this.pageLimit,
      skip: 0
    };
    this.getGroups(this.requestDataGroup);
  }

  getGroups(data) {
    this.apiService.post('/api/v1/group', data).subscribe( (response: any) => {
      if (response.data && response.status === 200) {
        const group = response.data.data;
        let users = group.map(g => {
          users = g.userId.reduce((user, x, i) => {
            if (i <= 3) {
              user.push(`${x.firstName} ${x.lastName}`);
            }
          return user;
        }, []);
        if (g.userId.length > 3) {
          g.users = users.join(', ') + '...';
        } else {
          g.users = users.join(', ');
        }
        return g;
      });
      this.groups =  users;
      } else {
        this.toastr.error(this.messageHandler[response.message], 'Error');
      }
    }, (errorResult) => {
      this.errorHandling(errorResult);
    });
  }

  sortData(field, order) {
    this.column = this.column.map(x => field === x['id'] ? { ...x, 'order': order } : x);
    if (order === 'asc') {
      this.groups.data = this.groups.sort((a, b) => a[field].localeCompare(b[field]));
    } else {
      this.groups.data = this.groups.sort((a, b) => b[field].localeCompare(a[field]));
    }
  }

  paginate(event: any) {
    const reqData = {
      adminId: this.currentUser,
      page: event.page,
      searchQuery: this.searchQuery,
      pageLimit: this.pageLimit,
      pageCount: event.pageCount,
      skip: 0
    };
    this.getGroups(reqData);
  }

  editItem(id) {
    this.router.navigate([`/group/edit-group/${id}`]);
  }

  deleteGroup(id: any) {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        this.apiService.delete('/api/v1/group/delete/', id).subscribe((response: any) => {
          if (response.status === 200) {
            this.getGroups(this.requestDataGroup);
            this.toastr.success(this.messageHandler[response.message], 'Success');
          } else {
            this.toastr.error(this.messageHandler[response.message], 'Error');
          }
        }, (errorResult) => {
          this.errorHandling(errorResult);
        });
      }
    });
  }

  changeStatus(id, status) {
    status = status ? false : true;
    const reqData = {
      id: id,
      status: status
    };
    this.apiService.post('/api/v1/group/change_status', reqData).subscribe((response: any) => {
      if (response.status === 200) {
        this.toastr.success('Status changed successfully', 'Success');
        this.getGroups(this.requestDataGroup);
      } else {
        this.toastr.error(this.messageHandler[response.message], 'Error');
      }
    });
  }

  errorHandling(error){
    try {
      this.isLoading = false;
      const errorObj = error ? JSON.parse(error) : '';
      this.toastr.error(errorObj.message, 'Error');
    } catch (error) {
      this.toastr.error(error.message, 'Error');
    }
  }
}
