import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListpromoCodeComponent } from './list-promoCode/list-promoCode.component';
import { AddpromoCodeComponent } from './add-promoCode/add-promoCode.component';

const routes: Routes = [{
  path: '',
  children: [
    {
      path: '',
      component: ListpromoCodeComponent
    },
    {
      path: 'add-promo',
      component: AddpromoCodeComponent
    },
    {
      path: 'edit-promo/:id',
      component: AddpromoCodeComponent
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class promoCodeRoutingModule { }
