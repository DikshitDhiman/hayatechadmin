import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { fromEvent } from 'rxjs';
import { debounceTime, map } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import Swal from 'sweetalert2';

import { AppServicesService } from '../../../shared/service/app-services.service';
import { environment } from '../../../../environments/environment';
import { constants } from '../../../constants/const';

@Component({
  selector: 'app-list-promo',
  templateUrl: './list-promoCode.component.html',
  styleUrls: ['./list-promoCode.component.css']
})
export class ListpromoCodeComponent implements OnInit {

  promo: any = [];
  items = [];
  searchQuery: String = '';
  search: String;
  totalItems = 0;
  page = 1;
  isLoading: Boolean = false;
  siteURL: String =  environment.siteURL;
  currentUser: any;
  itemsPerPage: any = constants.recordsPageLimit;
  messageHandler: any = constants.constMessages;
  totalRecords: any = 0;
  openToggle: Boolean = false;
  sort: String = 'companyName';
  actions: Array<any>;
  status: Array<any>;

  column: Array<any> = [
    { id: 'name', name: 'Promo Name', sorting: true, type: 'string', display: 'left', order: 'neutral' },
    { id: 'code', name: 'Promo Code', sorting: false, type: 'string', display: 'left', order: 'neutral' },
    { id: 'discount', name: 'Promo Discount', sorting: false, type: 'number', display: 'left', order: 'neutral' },
    { id: 'expiry', name: 'Expiry', sorting: false, type: 'date', display: 'left', order: 'neutral' },
    { name: 'Status', type: 'status', sorting: false, display: 'center' },
    { name: 'Action', sorting: false, type: 'string', display: 'center' }
  ];
  pageLimit: any = constants.recordsPageLimit;
  requestDataCorporate: any;

  constructor(
    private apiService: AppServicesService,
    private router: Router,
    private toastr: ToastrService,
  ) { }

  ngOnInit() {
    this.currentUser = JSON.parse(localStorage.getItem('user'))._id;
    this.requestDataCorporate = {
      adminId: this.currentUser,
      page: 0,
      pageLimit: this.pageLimit,
      skip: 0
    };
    this.getpromoCode(this.requestDataCorporate);
    this.status = [{
      action: (item) => this.changeStatus(item['_id'], item['is_active'])
    }];

    this.actions = [{
      parentClass: 'edit',
      action: (item) => this.editItem(item['_id']),
      title: 'Edit',
      childClass: 'icofont-ui-edit'
    }, {
      parentClass: 'delete',
      action: (item) => this.deletepromoCode(item['_id']),
      title: 'Delete',
      childClass: 'icofont-ui-delete'
    }];

    setTimeout(() => {
      this.keyUpSearch();
    });
  }

  keyUpSearch() {
    const searchBox = document.getElementById('searchInputForPromos');
    if (searchBox) {
      const keyup$ = fromEvent(searchBox, 'keyup');
      keyup$.pipe(
        map((i: any) => i.currentTarget.value),
        debounceTime(1000)
      ).subscribe(searchQuery => {
        this.searchPromocode(searchQuery);
      });
    }
  }

  searchPromocode(searchQuery) {
    this.requestDataCorporate = {
      adminId: this.currentUser,
      searchQuery: searchQuery,
      page: 0,
      pageLimit: this.pageLimit,
      skip: 0
    };
    this.getpromoCode(this.requestDataCorporate);
  }

  getpromoCode(data) {
    this.apiService.post('/api/v1/promoCode', data).subscribe( (response: any) => {
      if (response.data && response.status === 200) {
        this.promo = response.data;
      } else {
        this.toastr.error(this.messageHandler[response.message], 'Error');
      }
    }, (errorResult) => {
      this.errorHandling(errorResult);
    });
  }

  sortData(field, order) {
    this.column = this.column.map(x => field === x['id'] ? { ...x, 'order': order } : x);
    if (order === 'asc') {
      this.promo.data = this.promo.data.sort((a, b) => a[field].localeCompare(b[field]))
    } else {
      this.promo.data = this.promo.data.sort((a, b) => b[field].localeCompare(a[field]))
    }
  }

  paginate(event: any) {
    const reqData = {
      adminId: this.currentUser,
      page: event.page,
      searchQuery: this.searchQuery,
      pageLimit: this.pageLimit, 
      pageCount: event.pageCount, 
      skip: 0
    };
    this.getpromoCode(reqData);
  }


  editItem(id) {
    this.router.navigate([`/promo/edit-promo/${id}`]);
  }

  changeStatus(id, status) {
    status = status ? false : true;
    const reqData = {
      id: id,
      status: status
    };
    this.apiService.post('/api/v1/promoCode/change_status', reqData).subscribe((response: any) => {
      if (response.status === 200) {
        this.toastr.success('Status changed successfully', 'Success');
        this.getpromoCode(this.requestDataCorporate);
      } else {
        this.toastr.error(this.messageHandler[response.message], 'Error');
      }
    });
  }

  deletepromoCode(id: any) {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        this.apiService.delete('/api/v1/promoCode/delete/', id).subscribe((response: any) => {
          if (response.status === 200) {
            this.getpromoCode(this.requestDataCorporate);
            this.toastr.success(this.messageHandler[response.message], 'Success');
          } else {
            this.toastr.error(this.messageHandler[response.message], 'Error');
          }
        }, (errorResult) => {
          this.errorHandling(errorResult);
        });
      }
    });
  }

  switchToggle() {
    this.openToggle = !this.openToggle;
    // this.toggleEvent.emit(this.openToggle);
  }

  errorHandling(error){
    try {
      this.isLoading = false;
      const errorObj = error ? JSON.parse(error) : '';
      this.toastr.error(errorObj.message, 'Error');
    } catch (error) {
      this.toastr.error(error.message, 'Error');
    }
  }

}
