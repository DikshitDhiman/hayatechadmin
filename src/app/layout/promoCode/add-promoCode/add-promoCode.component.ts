import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { ToastrService } from 'ngx-toastr';

import { AppServicesService } from '../../../shared/service/app-services.service';
import { constants } from './../../../constants/const';

@Component({
  selector: 'app-add-promo',
  templateUrl: './add-promoCode.component.html',
  styleUrls: ['./add-promoCode.component.css']
})
export class AddpromoCodeComponent implements OnInit {

  addpromoCodeForm: FormGroup;
  isLoading: Boolean = false;
  currenUser: any;
  isSubmitted: Boolean = false;
  isSelectedDateTime: Boolean = false;
  uploadedFiles: any[] = [];
  promoId: any;
  isEdit: Boolean = false;
  messageHandler: any = constants.constMessages;

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private apiService: AppServicesService,
    private toastr: ToastrService
  ) { }

  onSelectMethod(event) {
    this.isSelectedDateTime = true;
  }

  ngOnInit() {
    this.createForm();
    this.currenUser = JSON.parse(localStorage.getItem('user'));
    this.route.paramMap.subscribe(paramsData => {
      if (paramsData.get('id')) {
        this.isEdit = true;
        this.promoId = paramsData.get('id');
        this.getPromo();
      } else {
        this.isEdit = false;
      }
    });
  }

  createForm() {
    this.addpromoCodeForm = this.fb.group({
      name: ['', Validators.required],
      code: ['', Validators.required],
      discount: ['', Validators.required],
      expiry: ['', Validators.required]
    });
  }

  getPromo() {
    this.apiService.get('/api/v1/promoCode/', this.promoId).subscribe((response: any) => {
      if (response && response.status === 200 && response.data) {
        const myDate = new Date(response.data.expiry);
        this.addpromoCodeForm.setValue({
          name: response.data.name,
          code: response.data.code,
          discount: response.data.discount,
          expiry: myDate
        });
      } else {
        this.toastr.error(this.messageHandler[response.message], 'Error');
      }
    }, (errorResult) => {
      this.errorHandling(errorResult);
    });
  }
  selectStartTime() {
    this.isSelectedDateTime = true;
  }

  resetForm() {
    this.addpromoCodeForm = this.fb.group({});
    this.createForm();
  }

  submitForm() {
    const reqData = {
      adminId : this.currenUser,
      name : this.addpromoCodeForm.value.name,
      code : this.addpromoCodeForm.value.code,
      discount: this.addpromoCodeForm.value.discount,
      expiry: this.addpromoCodeForm.value.expiry,
    };
    if (this.isEdit) {
      reqData['promoId'] = this.promoId;
    }
    this.apiService.post('/api/v1/promocode/add', reqData).subscribe( (response: any) => {
      if (response.status === 200) {
        this.toastr.success(this.messageHandler[response.message], 'Success');
        this.router.navigate(['/admin/promo/']);
        this.addpromoCodeForm.reset();
      } else {
        this.toastr.error(this.messageHandler[response.message], 'Error');
      }
    }, (errorResult) => {
      this.errorHandling(errorResult);
    });
  }

  errorHandling(error){
    try {
      this.isLoading = false;
      const errorObj = error ? JSON.parse(error) : '';
      this.toastr.error(errorObj.message, 'Error');
    } catch (error) {
      this.toastr.error(error.message, 'Error');
    }
  }

}
