import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CalendarModule } from 'primeng/calendar';
import { PaginatorModule } from 'primeng/paginator';
import { FileUploadModule } from 'primeng/fileupload';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { SharedModule } from '../../shared/shared.module';

import { promoCodeRoutingModule } from './promoCode-routing.module';
import { AddpromoCodeComponent } from './add-promoCode/add-promoCode.component';
import { ListpromoCodeComponent } from './list-promoCode/list-promoCode.component';

@NgModule({
  declarations: [AddpromoCodeComponent, ListpromoCodeComponent],
  imports: [
    CommonModule,
    promoCodeRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    CalendarModule,
    FileUploadModule,
    PaginatorModule,
    InputTextareaModule,
    SharedModule
  ]
})
export class promoCodeModule { }
