import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListVendorComponent } from './list-vendor/list-vendor.component';
import { AddVendorComponent } from './add-vendor/add-vendor.component';


const routes: Routes = [
  {
    path: '',
    children: [{
      path: '',
      component: ListVendorComponent
    }, {
      path: 'add-vendor',
      component: AddVendorComponent
    }, {
      path: 'edit-vendor/:id',
      component: AddVendorComponent
    }]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VendorRoutingModule { }
