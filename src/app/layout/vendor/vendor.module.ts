import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CKEditorModule } from 'ngx-ckeditor';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PaginatorModule } from 'primeng/paginator';
import { SharedModule } from '../../shared/shared.module';
import { VendorRoutingModule } from './vendor-routing.module';
import { ListVendorComponent } from './list-vendor/list-vendor.component';
import { AddVendorComponent } from './add-vendor/add-vendor.component';
@NgModule({
  declarations: [
    ListVendorComponent,
    AddVendorComponent
  ],
  imports: [
    CommonModule,
    VendorRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    PaginatorModule,
    SharedModule,
    CKEditorModule
  ]
})
export class VendorModule { }
