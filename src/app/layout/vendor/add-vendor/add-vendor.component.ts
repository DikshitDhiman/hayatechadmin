import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CustomValidation } from '../../../shared/service/custom-validators';
import { ToastrService } from 'ngx-toastr';
import { AppServicesService } from './../../../shared/service/app-services.service';
import { environment } from './../../../../environments/environment';
import { constants } from './../../../constants/const';

@Component({
  selector: 'app-add-vendor',
  templateUrl: './add-vendor.component.html',
  styleUrls: ['./add-vendor.component.css']
})
export class AddVendorComponent implements OnInit {

  addVendorForm: FormGroup;
  isLoading: Boolean = false;
  isEdit: Boolean = false;
  isSubmitted: boolean = false;
  isSelectedImg: boolean = false;
  isImageEdit: Boolean = false;
  currenUser: any;
  vendorId: any;
  email: any;
  siteURL: String =  environment.siteURL;
  messageHandler: any = constants.constMessages;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private apiService: AppServicesService,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    this.currenUser = JSON.parse(localStorage.getItem('user'))._id;
    this.createForm();
    this.route.paramMap.subscribe(paramsData => {
      if (paramsData.get('id')) {
        this.isEdit = true;
        this.vendorId = paramsData.get('id');
        this.getVendor();
        this.addVendorForm.controls['email'].disable();
      } else {
        this.isEdit = false;
      }
    });
  }
  get check() { return this.addVendorForm.controls; }

  createForm() {
    this.addVendorForm = this.fb.group({
      companyName: ['', [Validators.required, Validators.pattern('^[a-zA-Z \-\']+')]],
      contactPerson: ['', [Validators.required, Validators.pattern('^[a-zA-Z \-\']+')]],
      email: ['', [Validators.required]],
      mobile: ['',  [Validators.required, Validators.pattern(/^[\d\+]*$/gm)]],
      websiteUrl: ['', Validators.required],
      description: [''],
      address: ['', [Validators.required]],
      base64Data: ['', Validators.required]
    }, {
      validator: [CustomValidation.isValidEmail, CustomValidation.isValidWebsite],
    });
  }

  keyPress(event: any) {
    const pattern = /[0-9\+\-\ ]/;
    const inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode !== 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }

  getVendor() {
    this.apiService.get('/api/v1/vendor/', this.vendorId).subscribe((response: any) => {
      if (response && response.status === 200) {
        this.email = response.data.email;
        this.addVendorForm.setValue({
          companyName: response.data.companyName,
          contactPerson: response.data.contactPerson,
          email: response.data.email,
          websiteUrl: response.data.websiteUrl,
          description: '',
          mobile: response.data.mobile,
          address: response.data.address,
          base64Data: this.siteURL + response.data.image
        });
        /* I am frustated because ckeditor take some time to initialize edit content*/
        setTimeout( () => {
          this.addVendorForm.controls['description'].setValue(response.data.description);
        }, 1000);
      } else {
        this.toastr.error(this.messageHandler[response.message], 'Error');
      }
    }, (errorResult) => {
      this.errorHandling(errorResult);
    });
  }


  resetForm() {
    this.addVendorForm = this.fb.group({});
    this.createForm();
  }

  onUploadChange(evt: any) {
    const file = evt.target.files[0];
    if (file) {
      const reader = new FileReader();
      reader.onload = this.handleReaderLoaded.bind(this);
      reader.readAsBinaryString(file);
      this.isSelectedImg = true;
    }
  }

  handleReaderLoaded(e: any) {
    this.isImageEdit = true;
    this.addVendorForm.controls.base64Data.setValue('data:image/png;base64,' + btoa(e.target.result));
  }


  submitForm() {
    this.isSubmitted = true;
    const reqData = this.addVendorForm.value;
    this.addVendorForm.value.adminId = this.currenUser;
    if (this.isEdit) {
      reqData['vendorid'] = this.vendorId;
      reqData['email'] = this.email.toLowerCase();
    }
    if (!this.isImageEdit) {
      this.addVendorForm.value.base64Data = '';
    }
    this.apiService.post('/api/v1/vendor/add', reqData).subscribe( (response: any) => {
      if (response.status === 200) {
        this.router.navigate(['/admin/vendor']);
        this.toastr.success(this.messageHandler[response.message], 'Success');
        this.addVendorForm.reset();
      } else {
        this.toastr.error(this.messageHandler[response.message], 'Error');
      }
    }, (errorResult) => {
      this.errorHandling(errorResult);
    });
  }

  errorHandling(error) {
    try {
      this.isLoading = false;
      const errorObj = error ? JSON.parse(error) : '';
      this.toastr.error(errorObj.message, 'Error');
    } catch (error) {
      this.toastr.error(error.message, 'Error');
    }
  }

}
