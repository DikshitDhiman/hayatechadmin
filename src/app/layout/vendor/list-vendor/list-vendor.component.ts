import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import Swal from 'sweetalert2';
import { fromEvent } from 'rxjs';
import { debounceTime, map } from 'rxjs/operators';
import { AppServicesService } from './../../../shared/service/app-services.service';
import { constants } from './../../../constants/const';

@Component({
  selector: 'app-list-vendor',
  templateUrl: './list-vendor.component.html',
  styleUrls: ['./list-vendor.component.css']
})
export class ListVendorComponent implements OnInit {
  data: any;
  page = 1;
  search: String;
  vendors: any = [];
  searchQuery: String = '';
  itemsPerPage: any = constants.recordsPageLimit;
  isLoading: Boolean = false;
  sort: String = 'companyName';
  currentUser: any;
  actions: Array<any>;
  status: Array<any>;
  openToggle: Boolean = false;
  messageHandler: any = constants.constMessages;
  items = [];

  pageLimit: any = constants.recordsPageLimit;
  requestDataCorporate: any;

  column: Array<any> = [
    { id: 'companyName', name: 'Company', sorting: true, type: 'string', display: 'left', order: 'neutral' },
    { id: 'contactPerson', name: 'Person', sorting: true, type: 'string', display: 'left', order: 'neutral' },
    { id: 'email', name: 'Email', sorting: true, type: 'email', display: 'left', order: 'neutral' },
    { id: 'mobile', name: 'Mobile', sorting: false, type: 'number', display: 'center' },
    { id: 'address', name: 'Address', sorting: false, type: 'string', display: 'left' },
    { id: 'websiteUrl', name: 'WebsiteUrl', sorting: false, type: 'email', display: 'left' },
    { id: 'image', name: 'Logo', sorting: false, type: 'image', display: 'left' },
    { name: 'Status', type: 'status', sorting: false, display: 'center' },
    { name: 'Action', sorting: false, type: 'string', display: 'center' }
  ];

  /* vendors: any = [];
  pageLimit: any = constants.recordsPageLimit;
  totalRecords: any; */

  constructor(
    private router: Router,
    private apiService: AppServicesService,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    this.currentUser = JSON.parse(localStorage.getItem('user'));

    this.requestDataCorporate = {
      role: this.currentUser.role,
      adminId: this.currentUser._id,
      page: 0,
      pageLimit: this.pageLimit,
      skip: 0
    };

    this.getVendors(this.requestDataCorporate);

    this.status = [{
      action: (item) => this.changeStatus(item['_id'], item['is_active']),
    }];

    this.actions = [{
      parentClass: 'edit',
      action: (item) => this.editItem(item['_id']),
      title: 'Edit',
      childClass: 'icofont-ui-edit'
    }, {
      parentClass: 'delete',
      action: (item) => this.deleteItem(item['_id']),
      title: 'Delete',
      childClass: 'icofont-ui-delete'
    }];

    this.keyUpSearch();
  }


  keyUpSearch(){
    const searchBox = document.getElementById('searchInputForVendor');
    if(searchBox){
      const keyup$ = fromEvent(searchBox, 'keyup')
      keyup$.pipe(
        map((i: any) => i.currentTarget.value),
        debounceTime(1000)
      ).subscribe(searchQuery => {
        this.searchVendor(searchQuery);
      });
    }
  }

  searchVendor(searchQuery) {
    this.searchQuery = searchQuery;
    this.requestDataCorporate = {
      role: this.currentUser.role,
      adminId: this.currentUser._id,
      searchQuery: searchQuery,
      page: 0,
      pageLimit: this.pageLimit,
      skip: 0
    }
    this.getVendors(this.requestDataCorporate);
  }

  getVendors(data) {
    this.apiService.post('/api/v1/vendors', data).subscribe( (response: any) => {
      if (response.data && response.data.status === 200) {
        this.data = response.data.data;
        this.vendors = response.data;        
      } else {
        this.toastr.error(this.messageHandler[response.message], 'Error');
      }
    }, (errorResult) => {
      this.errorHandling(errorResult);
    });
  }


  paginate(event) {
    const reqData = {
      adminId: this.currentUser,
      page: event.page,
      searchQuery: this.searchQuery,
      pageLimit: this.pageLimit, 
      pageCount: event.pageCount, 
      skip: 0
    };
    this.getVendors(reqData);
  }

  sortData(field, order) {
    this.column = this.column.map(x => field === x['id'] ? { ...x, 'order': order } : x);
    if (order === 'asc') {
      this.vendors.data = this.vendors.data.sort((a, b) => a[field].localeCompare(b[field]))
    } else {
      this.vendors.data = this.vendors.data.sort((a, b) => b[field].localeCompare(a[field]))
    }
  }

  editItem(id) {
    this.router.navigate([`/vendor/edit-vendor/${id}`]);
  }

  changeStatus(id, status) {
    status = status ? false : true;
    const reqData = {
      id: id,
      status: status
    };
    this.apiService.post('/api/v1/vendor/change_status', reqData).subscribe((response: any) => {
      if (response.status === 200) {
        this.toastr.success('Status changed successfully', 'Success');
        this.getVendors(this.requestDataCorporate);
      } else {
        this.toastr.error(this.messageHandler[response.message], 'Error');
      }
    });
  }

  deleteItem(id: any) {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        this.apiService.delete('/api/v1/vendor/delete/', id).subscribe((response: any) => {
          if (response.status === 200) {
            this.toastr.success(this.messageHandler[response.message], 'Success');
            this.getVendors(this.requestDataCorporate);
          } else {
            this.toastr.error(this.messageHandler[response.message], 'Error');
          }
        }, (errorResult) => {
          this.errorHandling(errorResult);
        });
      }
    });
  }

  errorHandling(error){
    try {
      this.isLoading = false;
      const errorObj = error ? JSON.parse(error) : '';
      this.toastr.error(errorObj.message, 'Error');
    } catch (error) {
      this.toastr.error(error.message, 'Error');
    }
  }

}
