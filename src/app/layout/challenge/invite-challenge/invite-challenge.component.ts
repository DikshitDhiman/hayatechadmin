import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { AppServicesService } from './../../../shared/service/app-services.service';
import { constants } from './../../../constants/const';

@Component({
  selector: 'app-invite-challenge',
  templateUrl: './invite-challenge.component.html',
  styleUrls: ['./invite-challenge.component.scss']
})
export class InviteChallengeComponent implements OnInit {
  inviteUserForm: FormGroup;
  isLoading: Boolean = true;
  currentUser: any;
  messageHandler: any = constants.constMessages;
  groups: any = [];
  companies: any = [];
  challengeId: any = [];
  selected: any;
  selectedCompany: any;
  selectedDepartments = [];
  compDepartments: any = [];
  departments: any = [];
  groupedCars: any = [];
  settings = {};

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private apiService: AppServicesService,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    this.createForm();
    this.multiDropdownSetting();
    this.currentUser = JSON.parse(localStorage.getItem('user'));

    this.route.paramMap.subscribe(paramsData => {
      if (paramsData.get('id')) {
        this.challengeId = paramsData.get('id');
      }
    });

    this.getDepartments();
    const reqData = {
      _id: this.challengeId
    };
    // this.getChallenge(reqData);
    this.getGroups();
  }

  createForm() {
    this.inviteUserForm = this.fb.group({
      invitationType: ['custom_team', Validators.required],
      singleDepartment: ['', Validators.required],
      department1: ['', Validators.required],
      department2:  ['', Validators.required],
      group1: ['', Validators.required],
      group2: ['', Validators.required],
      company: ['', Validators.required],
      multipleDepartment: ['', Validators.required],
    });
  }

  multiDropdownSetting(){
    this.settings = {
      singleSelection: false,
      labelKey: 'itemName',
      text: "Select companie's departments",
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      searchPlaceholderText: 'Search Fields',
      enableSearchFilter: false,
      badgeShowLimit: 6
    };
  }

  onTypeSelect(type: String) {
    if (type === 'custom_team') {
      this.inviteUserForm.get('group1').setValidators([Validators.required]);
      this.inviteUserForm.get('group2').setValidators([Validators.required]);
    } else {
      this.inviteUserForm.get('department1').setValidators([]);
      this.inviteUserForm.get('department2').setValidators([]);
      this.inviteUserForm.get('singleDepartment').setValidators([]);
    }
    if (type === 'twoDepartments') {
      this.inviteUserForm.get('department1').setValidators([Validators.required]);
      this.inviteUserForm.get('department2').setValidators([Validators.required]);
    } else {
      this.inviteUserForm.get('group1').setValidators([]);
      this.inviteUserForm.get('group2').setValidators([]);
      this.inviteUserForm.get('singleDepartment').setValidators([]);
    }
    if (type === 'singleDepartment') {
      this.inviteUserForm.get('singleDepartment').setValidators([Validators.required]);
    } else {
      this.inviteUserForm.get('group1').setValidators([]);
      this.inviteUserForm.get('group2').setValidators([]);
      this.inviteUserForm.get('department1').setValidators([]);
      this.inviteUserForm.get('department2').setValidators([]);
    }
    this.inviteUserForm.get('singleDepartment').updateValueAndValidity();
    this.inviteUserForm.get('department1').updateValueAndValidity();
    this.inviteUserForm.get('department2').updateValueAndValidity();
    this.inviteUserForm.get('group1').updateValueAndValidity();
    this.inviteUserForm.get('group2').updateValueAndValidity();
  }

  onOptionSelect(option: any) {
    this.selected = option;
  }

  onCompanySelect(option: any) {
    this.compDepartments = [];
    this.selectedCompany = option;
    
    const found = (this.departments).find(element => element._id == this.selectedCompany);
    if (found && found.items) {
      this.compDepartments = (found.items).map(function(val, index){ 
        return {id:val.value, itemName:val.label}; 
      })
    }
  }

  onOpen(env: any) {
    //this.compDepartments = [...this.compDepartments];
    console.log("onOpen", this.compDepartments);
  }
  onClose(env: any) {
    //this.compDepartments = [...this.compDepartments];
    console.log("onClose", this.compDepartments);
  }

  getDepartments() {
    this.apiService.post('/api/v1/groupCompanyDepartments', { adminId: this.currentUser._id }).subscribe((response: any) => {
      if (response.data && response.status === 200) {
        this.departments = response.data;
        this.inviteUserForm.controls.singleDepartment.setValue(this.departments[0].items[0]._id);
        this.inviteUserForm.controls.department1.setValue(this.departments[0].items[0]._id);
        this.inviteUserForm.controls.department2.setValue(this.departments[1].items[0]._id);

        this.companies = (this.departments).map(function(val, index){ 
          return val; 
        })
        console.log("this.companies", this.companies);
        (this.companies).unshift({label: "Select a company", _id: ""})
      } else {
        this.toastr.error(this.messageHandler[response.message], 'Error');
      }
    }, (errorResult) => {
      this.errorHandling(errorResult);
    });
  }

  getChallenge(date) {
    this.apiService.get('/api/v1/challenge/', this.challengeId).subscribe((response: any) => {
      if (response.data && response.status === 200) {
        // console.log('Here challegne data : ', response.data);
      } else {
        this.toastr.error(this.messageHandler[response.message], 'Error');
      }
    }, (errorResult) => {
      this.errorHandling(errorResult);
    });
  }

  getGroups() {
    this.apiService.post('/api/v1/group', { adminId: this.currentUser._id }).subscribe( (response: any) => {
      if (response.data && response.status === 200) {
        this.groups = response.data;
        this.inviteUserForm.controls.group1.setValue(this.groups.data[0]._id);
        this.inviteUserForm.controls.group2.setValue(this.groups.data[1]._id);
      } else {
        this.toastr.error(this.messageHandler[response.message], 'Error');
      }
    }, (errorResult) => {
      this.errorHandling(errorResult);
    });
  }

  submitForm() {
    const reqData = this.inviteUserForm.value;
    reqData.adminId = this.currentUser._id;
    reqData.challengeId = this.challengeId;
    this.apiService.post('/api/v1/invite/add', reqData).subscribe( (response: any) => {
      if (response.status === 200) {
        this.toastr.success(this.messageHandler[response.message], 'Success');
        this.router.navigate(['/admin']);
        this.inviteUserForm.reset();
      } else {
        this.toastr.error(this.messageHandler[response.message], 'Error');
      }
    }, (errorResult) => {
      this.errorHandling(errorResult);
    });
  }

  resetForm() {
    this.inviteUserForm = this.fb.group({
    });
  }

  errorHandling(error) {
    try {
      this.isLoading = false;
      const errorObj = error ? JSON.parse(error) : '';
      this.toastr.error(errorObj.message, 'Error');
    } catch (error) {
      this.toastr.error(error.message, 'Error');
    }
  }


  

}
