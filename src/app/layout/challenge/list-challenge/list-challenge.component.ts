import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { fromEvent } from 'rxjs';
import { debounceTime, map } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import Swal from 'sweetalert2';

import { AppServicesService } from './../../../shared/service/app-services.service';
import { environment } from './../../../../environments/environment';
import { constants } from './../../../constants/const';

@Component({
  selector: 'app-list-challenge',
  templateUrl: './list-challenge.component.html',
  styleUrls: ['./list-challenge.component.css']
})
export class ListChallengeComponent implements OnInit {

  challenges: any = [];
  searchQuery: String = '';
  totalItems = 0;
  page = 1;
  isLoading: Boolean = false;
  siteURL: String =  environment.siteURL;
  currentUser: any;
  itemsPerPage: any = constants.recordsPageLimit;
  messageHandler: any = constants.constMessages;
  totalRecords: any = 0;
  openToggle: Boolean = false;
  sort: String = 'name';
  actions: Array<any>;
  status: Array<any>;
  items = [];
  column: Array<any> = [
    { id: 'image', name: 'Image', sorting: false, type: 'image', display: 'left' },
    { id: 'name', name: 'Name', sorting: true, type: 'string', display: 'left', order: 'neutral' },
    { id: 'shortDesc', name: 'Summary', sorting: false, type: 'string', display: 'left', order: 'neutral' },
    { id: 'points', name: 'Rewards', sorting: false, type: 'number', display: 'left', order: 'neutral' },
    { id: 'steps', name: 'Steps', sorting: false, type: 'number', display: 'left', order: 'neutral' },
    { id: 'startDateTime', name: 'Start Time', sorting: true, type: 'datetime', display: 'left', order: 'neutral'},
    { id: 'endDateTime', name: 'End Time', sorting: true, type: 'datetime', display: 'left', order: 'neutral'},
    { name: 'Status', type: 'status', sorting: false, display: 'center' },
    { name: 'Action', sorting: false, type: 'string', display: 'center' }
  ];
  pageLimit: any = constants.recordsPageLimit;
  requestData: any;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private apiService: AppServicesService,
    private toastr: ToastrService,
  ) { }

  ngOnInit() {
    this.currentUser = JSON.parse(localStorage.getItem('user'));

    this.requestData = {
      page: 0,
      pageLimit: this.pageLimit,
      skip: 0,
      role: this.currentUser.role,
      roleId: this.currentUser._id
    };
    this.getChallenge(this.requestData);
    this.status = [{
      action: (item) => this.changeStatus(item['_id'], item['is_active'])
    }];

    this.actions = [{
      parentClass: 'edit',
      action: (item) => this.editItem(item['_id']),
      title: 'Edit',
      childClass: 'icofont-ui-edit'
    }, {
      parentClass: 'delete',
      action: (item) => this.deleteChallenge(item['_id']),
      title: 'Delete',
      childClass: 'icofont-ui-delete'
    }];

    setTimeout( () => {
      this.keyUpSearch();
    }, 1000);
  }

  keyUpSearch() {
    const searchBox = document.getElementById('searchInputForChallenge');
    const keyup$ = fromEvent(searchBox, 'keyup');
    keyup$.pipe(
      map((i: any) => i.currentTarget.value),
      debounceTime(1000)
    ).subscribe(searchQuery => {
      this.searchChallenges(searchQuery);
    });
  }

  searchChallenges(searchQuery) {
    this.searchQuery = searchQuery;
    this.requestData = {
      searchQuery: searchQuery,
      page: 0,
      pageLimit: this.pageLimit,
      skip: 0,
      role: this.currentUser.role,
      roleId: this.currentUser._id
    };
    this.getChallenge(this.requestData);
  }

  getChallenge(data) {
    this.apiService.post('/api/v1/challenges', data).subscribe( (response: any) => {
      if (response.data && response.status === 200) {
        this.challenges = response.data;
        this.challenges.data.map(item => {
          if (new Date(item.endDateTime).getTime() < new Date().getTime()) {
            item['expired'] = true;
            return item;
          } else {
            item['expired'] = false;
            return item;
          }
        });
      } else {
        this.toastr.error(this.messageHandler[response.message], 'Error');
      }
    }, (errorResult) => {
      this.errorHandling(errorResult);
    });
  }

  sortData(field, order) {
    this.column = this.column.map(x => field === x['id'] ? { ...x, 'order': order } : x);
    if (order === 'asc') {
      this.challenges.data = this.challenges.data.sort((a, b) => a[field].localeCompare(b[field]))
    } else {
      this.challenges.data = this.challenges.data.sort((a, b) => b[field].localeCompare(a[field]))
    }
  }

  paginate(event: any) {
    const reqData = {
      role: this.currentUser.role,
      roleId: this.currentUser._id,
      page: event.page,
      searchQuery: this.searchQuery,
      pageLimit: this.pageLimit,
      pageCount: event.pageCount,
      skip: 0
    };
    this.getChallenge(reqData);
  }

  editItem(id) {
    this.router.navigate([`/challenge/edit-challenge/${id}`]);
  }

  changeStatus(id, status) {
    status = status ? false : true;
    const reqData = {
      id: id,
      status: status
    };
    this.apiService.post('/api/v1/challenge/change_status', reqData).subscribe((response: any) => {
      if (response.status === 200) {
        this.toastr.success('Status changed successfully', 'Success');
        this.getChallenge(this.requestData);
      } else {
        this.toastr.error(this.messageHandler[response.message], 'Error');
      }
    });
  }

  deleteChallenge(id: any) {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        this.apiService.delete('/api/v1/challenge/delete/', id).subscribe((response: any) => {
          if (response.status === 200) {
            this.toastr.success(this.messageHandler[response.message], 'Success');
            this.getChallenge(this.requestData);
          } else {
            this.toastr.error(this.messageHandler[response.message], 'Error');
          }
        }, (errorResult) => {
          this.errorHandling(errorResult);
        });
      }
    });
  }

  switchToggle() {
    this.openToggle = !this.openToggle;
    // this.toggleEvent.emit(this.openToggle);
  }

  errorHandling(error){
    try {
      this.isLoading = false;
      const errorObj = error ? JSON.parse(error) : '';
      this.toastr.error(errorObj.message, 'Error');
    } catch (error) {
      this.toastr.error(error.message, 'Error');
    }
  }

}
