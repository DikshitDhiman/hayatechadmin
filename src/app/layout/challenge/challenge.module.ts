import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CKEditorModule } from 'ngx-ckeditor';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ImageCropperModule } from 'ngx-image-cropper';
import { CalendarModule } from 'primeng/calendar';
import { PaginatorModule } from 'primeng/paginator';
import { FileUploadModule } from 'primeng/fileupload';
import { DropdownModule } from 'primeng/dropdown';
import { SharedModule } from '../../shared/shared.module';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { SanitizeTextModule } from './../../shared/pipes/sanitize-text.module';
import { ChallengeRoutingModule } from './challenge-routing.module';
import { AddChallengeComponent } from './add-challenge/add-challenge.component';
import { ListChallengeComponent } from './list-challenge/list-challenge.component';
import { InviteChallengeComponent } from './invite-challenge/invite-challenge.component';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';

@NgModule({
  declarations: [
    AddChallengeComponent, 
    ListChallengeComponent, 
    InviteChallengeComponent
  ],
  imports: [
    CommonModule,
    ChallengeRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    CalendarModule,
    FileUploadModule,
    DropdownModule,
    PaginatorModule,
    InputTextareaModule,
    CKEditorModule,
    SanitizeTextModule,
    SharedModule,
    AngularMultiSelectModule,
    ImageCropperModule
  ]
})
export class ChallengeModule { }
