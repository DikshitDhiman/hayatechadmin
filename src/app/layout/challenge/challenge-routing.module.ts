import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListChallengeComponent } from './list-challenge/list-challenge.component';
import { AddChallengeComponent } from './add-challenge/add-challenge.component';
import { InviteChallengeComponent } from './invite-challenge/invite-challenge.component';

const routes: Routes = [{
  path: '',
  children: [
    {
      path: '',
      component: ListChallengeComponent
    },
    {
      path: 'add-challenge',
      component: AddChallengeComponent
    },
    {
      path: 'edit-challenge/:id',
      component: AddChallengeComponent
    },
    {
      path: 'invite-challenge/:id',
      component: InviteChallengeComponent
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ChallengeRoutingModule { }
