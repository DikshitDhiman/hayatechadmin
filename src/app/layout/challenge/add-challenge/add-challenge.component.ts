import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { ImageCroppedEvent } from 'ngx-image-cropper';
import { AppServicesService } from './../../../shared/service/app-services.service';
import { environment } from './../../../../environments/environment';
import { constants } from './../../../constants/const';

@Component({
  selector: 'app-add-challenge',
  templateUrl: './add-challenge.component.html',
  styleUrls: ['./add-challenge.component.css']
})
export class AddChallengeComponent implements OnInit {
  imageChangedEvent: any = '';
  croppedImage: any = '';
  addChallengeForm: FormGroup;
  isLoading: Boolean = false;
  currentUser: any;
  uploadedFiles: any[] = [];
  messageHandler: any = constants.constMessages;
  isEdit: Boolean = false;
  isSubmitted: Boolean = false;
  isSelectedImg: Boolean = false;
  isSelectedDateTime: Boolean = false;
  isImageEdit: Boolean = false;
  challengeId: any;
  siteURL: String =  environment.siteURL;
  todayDate: Date = new Date();
  newStartDate: Date = this.todayDate;
  startDateValidation = [];
  endDateValidation = [];
  weekendBoost = false;

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private apiService: AppServicesService,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    this.createForm();
    this.currentUser = JSON.parse(localStorage.getItem('user'));
    this.route.paramMap.subscribe(paramsData => {
      if (paramsData.get('id')) {
        this.isEdit = true;
        this.challengeId = paramsData.get('id');
        this.getChallenge();
      } else {
        this.isEdit = false;
      }
    });
  }

  createForm() {
    this.addChallengeForm = this.fb.group({
      name: ['', Validators.required],
      steps: [],
      points: ['', Validators.required],
      is_featured: [''],
      bonusPoint1: ['', Validators.required],
      bonusPoint2: ['', Validators.required],
      bonusPoint3: ['', Validators.required],
      shortDesc: ['', Validators.required],
      description: ['', Validators.required],
      base64Data: ['', Validators.required],
      startDateTime: [this.todayDate, Validators.required],
      endDateTime: [this.addDays(new Date(), 10), Validators.required],
      challengeType: ['timed-bound', Validators.required],
      averageDailySteps: [],
      increaseSteps: []
    });
  }

  addDays(theDate, days) {
    return new Date(theDate.getTime() + days * 24 * 60 * 60 * 1000);
  }

  getChallenge() {
    this.apiService.get('/api/v1/challenge/', this.challengeId).subscribe((response: any) => {
      if (response && response.status === 200 && response.data) {
        const startDate = new Date(response.data.startDateTime);
        const endDate = new Date(response.data.endDateTime);
        this.addChallengeForm.patchValue({
          name: response.data.name,
          steps: response.data.steps,
          points: response.data.points,
          bonusPoint1: response.data.bonusPoint1,
          bonusPoint2: response.data.bonusPoint2,
          bonusPoint3: response.data.bonusPoint3,
          challengeType: response.data.challengeType,
          shortDesc: response.data.shortDesc,
          description: '',
          base64Data: this.siteURL + response.data.image,
          startDateTime: startDate,
          is_featured: response.data.is_featured,
          endDateTime: endDate
        });
        setTimeout( () => {
          this.addChallengeForm.controls['description'].patchValue(response.data.description);
        }, 1000);
      } else {
        this.toastr.error(this.messageHandler[response.message], 'Error');
      }
    }, (errorResult) => {
      this.errorHandling(errorResult);
    });
  }

  challengeType (type: String) {
    if (type === 'weekend') {
      this.weekendBoost = true;
      this.startDateValidation = [0, 1, 2, 3, 4, 6];
      this.endDateValidation = [0, 1, 2, 3, 4, 5];
      this.addChallengeForm.controls['averageDailySteps'].setValidators([Validators.required]);
      this.addChallengeForm.controls['increaseSteps'].setValidators([Validators.required]);
      this.addChallengeForm.controls['steps'].setValue('');
      this.addChallengeForm.get('steps').clearValidators();
      this.addChallengeForm.get('bonusPoint1').clearValidators();
      this.addChallengeForm.get('bonusPoint2').clearValidators();
      this.addChallengeForm.get('bonusPoint3').clearValidators();
      this.addChallengeForm.get('averageDailySteps').updateValueAndValidity();
      this.addChallengeForm.get('increaseSteps').updateValueAndValidity();
      this.addChallengeForm.get('steps').updateValueAndValidity();
      this.addChallengeForm.get('bonusPoint1').updateValueAndValidity();
      this.addChallengeForm.get('bonusPoint2').updateValueAndValidity();
      this.addChallengeForm.get('bonusPoint3').updateValueAndValidity();
    } else {
      this.weekendBoost = false;
      this.startDateValidation = [];
      this.endDateValidation = [];
      this.addChallengeForm.get('averageDailySteps').clearValidators();
      this.addChallengeForm.get('increaseSteps').clearValidators();
      this.addChallengeForm.controls['steps'].setValidators([Validators.required]);
      this.addChallengeForm.get('averageDailySteps').updateValueAndValidity();
      this.addChallengeForm.get('increaseSteps').updateValueAndValidity();
      this.addChallengeForm.get('steps').updateValueAndValidity();
      this.addChallengeForm.controls['bonusPoint1'].setValidators([Validators.required]);
      this.addChallengeForm.controls['bonusPoint2'].setValidators([Validators.required]);
      this.addChallengeForm.controls['bonusPoint3'].setValidators([Validators.required]);
      this.addChallengeForm.get('bonusPoint1').updateValueAndValidity();
      this.addChallengeForm.get('bonusPoint2').updateValueAndValidity();
      this.addChallengeForm.get('bonusPoint3').updateValueAndValidity();
    }
  }

  resetForm() {
    this.addChallengeForm = this.fb.group({});
    this.createForm();
  }

  keyPress(event: any) {
    const pattern = /[0-9\+\-\ ]/;
    const inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode !== 3 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }

  onUploadChange(event: any): void {
    this.imageChangedEvent = event;
  }

  imageCropped(event: ImageCroppedEvent) {
    this.croppedImage = event.base64;

    if (this.croppedImage) {
      this.isSelectedImg = true;
      this.isImageEdit = true;
      this.addChallengeForm.controls.base64Data.setValue(this.croppedImage)
    }
  }

  /* onUploadChange(evt: any) {
    const file = evt.target.files[0];
    if (file) {
      const reader = new FileReader();
      reader.onload = this.handleReaderLoaded.bind(this);
      reader.readAsBinaryString(file);
      this.isSelectedImg = true;
    }
  } */

  handleReaderLoaded(e) {
    this.isImageEdit = true;
    this.addChallengeForm.controls.base64Data.setValue('data:image/png;base64,' + btoa(e.target.result));
  }

  selectStartTime() {
    this.newStartDate = new Date(this.addChallengeForm.value.startDateTime);
    this.isSelectedDateTime = true;
  }

  submitForm() {
    this.isSubmitted = true;
    if (this.currentUser.role === 'admin') {
      this.addChallengeForm.value.adminId = this.currentUser._id;
    } else {
      this.addChallengeForm.value.addedBy = 'vendor';
      this.addChallengeForm.value.vendorId = this.currentUser._id;
    }

    if (this.isEdit) {
      this.addChallengeForm.value.challengeId = this.challengeId;
      this.isSelectedDateTime = true;
      this.isSelectedImg = true;
    }
    if (this.isImageEdit) {
      this.addChallengeForm.value.base64Data = this.addChallengeForm.value.base64Data;
    } else {
      this.addChallengeForm.value.base64Data = '';
    }
    this.apiService.post('/api/v1/challenge/add', this.addChallengeForm.value).subscribe((response: any) => {
      if (response.status === 200) {
        this.toastr.success(this.messageHandler[response.message], 'Success');
        if (response.data.challengeType === 'timed-bound' && !this.isEdit) {
          this.router.navigateByUrl(`/admin/challenge/invite-challenge/${response.data._id}`);
        } else {
          this.router.navigate(['/admin/challenge/']);
        }
        this.addChallengeForm.reset();
      } else {
        this.toastr.error(this.messageHandler[response.message], 'Error');
      }
    }, (errorResult) => {
      this.errorHandling(errorResult);
    });
  }

  errorHandling(error) {
    try {
      this.isLoading = false;
      const errorObj = error ? JSON.parse(error) : '';
      this.toastr.error(errorObj.message, 'Error');
    } catch (error) {
      this.toastr.error(error.message, 'Error');
    }
  }

}
