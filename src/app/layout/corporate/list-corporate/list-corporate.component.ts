import { Component, OnInit, ViewEncapsulation, ElementRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import Swal from 'sweetalert2';
import { fromEvent } from 'rxjs';
import { debounceTime, map } from 'rxjs/operators';
import { AppServicesService } from './../../../shared/service/app-services.service';
import { constants } from './../../../constants/const';

@Component({
  selector: 'app-list-corporate',
  templateUrl: './list-corporate.component.html',
  styleUrls: ['./list-corporate.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ListCorporateComponent implements OnInit {
  data: any;
  page = 1;
  search: String;
  searchQuery: String = '';
  itemsPerPage: any = constants.recordsPageLimit;
  isLoading: Boolean = false;
  sort: String = 'name';
  currentUser: any;
  actions: Array<any>;
  status: Array<any>;
  openToggle: Boolean = false;
  messageHandler: any = constants.constMessages;
  items = [];
  corporate: any = [];
  pageLimit: any = constants.recordsPageLimit;

  requestDataCorporate: any;

  column: Array<any> = [
    { id: 'name', name: 'Company', sorting: true, type: 'string', display: 'left', order: 'neutral' },
    { id: 'contactPerson', name: 'Person', sorting: true, type: 'string', display: 'left', order: 'neutral' },
    { id: 'email', name: 'Email', class: 'lowercase', sorting: true, type: 'string', display: 'left', order: 'neutral' },
    { id: 'mobile', name: 'Mobile', sorting: false, type: 'number', display: 'center' },
    { id: 'address', name: 'Address', sorting: false, type: 'string', display: 'left' },
    { name: 'Status', type: 'status', sorting: false, display: 'center' },
    { name: 'Action', sorting: false, type: 'string', display: 'center' }
  ];

  constructor(
    private router: Router,
    private apiService: AppServicesService,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    this.currentUser = JSON.parse(localStorage.getItem('user'))._id;
    this.requestDataCorporate = {
      adminId: this.currentUser,
      page: 0,
      pageLimit: this.pageLimit,
      skip: 0
    };

    this.getCorporate(this.requestDataCorporate);

    this.status = [{
      action: (item) => this.changeStatus(item['_id'], item['is_active'])
    }];

    this.actions = [{
      parentClass: 'view',
      action: (item) => this.viewUser(item['_id']),
      title: 'View User',
      childClass: 'icofont-eye-alt'
    }, {
      parentClass: 'edit',
      action: (item) => this.editItem(item['_id']),
      title: 'Edit',
      childClass: 'icofont-ui-edit'
    }, {
      parentClass: 'delete',
      action: (item) => this.deleteItem(item['_id']),
      title: 'Delete',
      childClass: 'icofont-ui-delete'
    }];

    this.keyUpSearch();
  }

  keyUpSearch() {
    const searchBox = document.getElementById('searchInputForCorporate');
    if (searchBox) {
      const keyup$ = fromEvent(searchBox, 'keyup');
      keyup$.pipe(
        map((i: any) => i.currentTarget.value),
        debounceTime(1000)
      ).subscribe(searchQuery => {
        this.searchCorporate(searchQuery);
      });
    }
  }

  searchCorporate(searchQuery) {
    this.searchQuery = searchQuery;
    this.requestDataCorporate = {
      adminId: this.currentUser,
      searchQuery: searchQuery,
      page: 0,
      pageLimit: this.pageLimit,
      skip: 0
    }
    this.getCorporate(this.requestDataCorporate);
  }

  getCorporate(data) {
    this.apiService.post('/api/v1/corporates', data).subscribe((response: any) => {
      if (response.data && response.status === 200) {
        this.corporate = response.data;
      } else {
        this.toastr.error(this.messageHandler[response.message], 'Error');
      }
    }, (errorResult) => {
      this.errorHandling(errorResult);
    });
  }



  sortData(field, order) {
    this.column = this.column.map(x => field === x['id'] ? { ...x, 'order': order } : x);
    if (order === 'asc') {
      this.corporate.data = this.corporate.data.sort((a, b) => a[field].localeCompare(b[field]));
    } else {
      this.corporate.data = this.corporate.data.sort((a, b) => b[field].localeCompare(a[field]));
    }
  }

  onPageChange(page) {

  }

  paginate(event) {
    const reqData = {
      adminId: this.currentUser,
      searchQuery: this.searchQuery,
      page: event.page,
      pageLimit: this.pageLimit,
      pageCount: event.pageCount,
      skip: 0
    };
    this.getCorporate(reqData);
  }

  editItem(id) {
    this.router.navigate([`/company/edit-company/${id}`]);
  }

  viewUser(id) {
    this.router.navigateByUrl(`/users/company-users/${id}`);
  }

  changeStatus(id, status) {
    status = status ? false : true;
    const reqData = {
      id: id,
      status: status
    };
    this.apiService.post('/api/v1/corporate/change_status', reqData).subscribe((response: any) => {
      if (response.status === 200) {
        this.toastr.success('Status changed successfully', 'Success');
        this.getCorporate(this.requestDataCorporate);
      } else {
        this.toastr.error(this.messageHandler[response.message], 'Error');
      }
    });
  }

  deleteItem(id: any) {
    Swal.fire({
      title: 'Are you sure, this will delete all company data and users will not be able to login',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        this.apiService.delete('/api/v1/corporate/delete/', id).subscribe((response: any) => {
          if (response.status === 200) {
            this.toastr.success(this.messageHandler[response.message], 'Success');
            this.getCorporate(this.requestDataCorporate);
          } else {
            this.toastr.error(this.messageHandler[response.message], 'Error');
          }
        }, (errorResult) => {
          this.errorHandling(errorResult);
        });
      }
    });
  }

  errorHandling(error) {
    try {
      this.isLoading = false;
      const errorObj = error ? JSON.parse(error) : '';
      this.toastr.error(errorObj.message, 'Error');
    } catch (error) {
      this.toastr.error(error.message, 'Error');
    }
  }
}
