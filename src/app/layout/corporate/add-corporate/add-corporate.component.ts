import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { CustomValidation } from '../../../shared/service/custom-validators';
import { AppServicesService } from './../../../shared/service/app-services.service';
import { constants } from './../../../constants/const';


@Component({
  selector: 'app-add-corporate',
  templateUrl: './add-corporate.component.html',
  styleUrls: ['./add-corporate.component.css']
})
export class AddCorporateComponent implements OnInit {

  addCorporateForm: FormGroup;
  isLoading: Boolean = false;
  isEdit: Boolean = false;
  currenUser: any;
  corporateId: any;
  email: any;
  departments: any = [];
  messageHandler: any = constants.constMessages;

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private apiService: AppServicesService,
    private toastr: ToastrService
  ) {}

  get check() { return this.addCorporateForm.controls; }

  ngOnInit() {
    this.currenUser = JSON.parse(localStorage.getItem('user'))._id;
    this.createForm();
    this.route.paramMap.subscribe(paramsData => {
      if (paramsData.get('id')) {
        this.isEdit = true;
        this.corporateId = paramsData.get('id');
        this.getCorporate();
        this.getDepartments(this.currenUser);
        this.addCorporateForm.controls['email'].disable();
      } else {
        this.isEdit = false;
      }
    });
  }

  getDepartments(adminId) {
    this.apiService.get('/api/v1/departments/', adminId).subscribe((response: any) => {
      if (response && response.status === 200) {
        response.data.map((row: any) => {
          const obj = {
            label : row.name,
            value : row._id
          };
          this.departments.push(obj);
        });
      } else {
        this.toastr.error(this.messageHandler[response.message], 'Error');
      }
    }, (errorResult) => {
      this.errorHandling(errorResult);
    });
  }

  getCorporate() {
    this.apiService.get('/api/v1/corporate/', this.corporateId).subscribe((response: any) => {
      if (response && response.status === 200) {
        this.email = response.data.email;
        this.addCorporateForm.setValue({
          name: response.data.name,
          email: response.data.email,
          mobile: response.data.mobile,
          address: response.data.address,
          contactPerson: response.data.contactPerson
        });
      } else {
        this.toastr.error(this.messageHandler[response.message], 'Error');
      }
    }, (errorResult) => {
      this.errorHandling(errorResult);
    });
  }


  createForm() {
    this.addCorporateForm = this.fb.group({
      name: ['', [Validators.required, Validators.pattern('[a-zA-Z][a-zA-Z ]+[a-zA-Z]$')]],
      email: ['', [Validators.required]],
      mobile: ['',  [Validators.required, Validators.pattern(/^[0-9]*$/)]],
      address: ['', [Validators.required]],
      contactPerson: ['', [Validators.required, Validators.pattern('[a-zA-Z][a-zA-Z ]+[a-zA-Z]$')]]
    }, {
      validator: CustomValidation.isValidEmail
    });
  }

  keyPress(event: any) {
    const pattern = /[0-9\+\-\ ]/;
    const inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }

  resetForm() {
    this.addCorporateForm = this.fb.group({});
    this.createForm();
  }

  submitForm() {
    const reqData = {
      adminId : this.currenUser,
      email : this.addCorporateForm.value.email.toLowerCase(),
      name : this.addCorporateForm.value.name,
      mobile : this.addCorporateForm.value.mobile,
      address : this.addCorporateForm.value.address,
      contactPerson : this.addCorporateForm.value.contactPerson
    };
    if (this.isEdit) {
      reqData['corporateId'] = this.corporateId;
      reqData['email'] = this.email;
    }
    this.apiService.post('/api/v1/corporate/add', reqData).subscribe( (response: any) => {
      if (response.status === 200) {
        this.toastr.success(this.messageHandler[response.message], 'Success');
        this.router.navigate(['/admin/company/']);
        this.addCorporateForm.reset();
      } else {
        this.toastr.error(this.messageHandler[response.message], 'Error');
      }
    }, (errorResult) => {
      this.errorHandling(errorResult);
    });
  }

  errorHandling(error){
    try {
      this.isLoading = false;
      const errorObj = error ? JSON.parse(error) : '';
      this.toastr.error(errorObj.message, 'Error');
    } catch (error) {
      this.toastr.error(error.message, 'Error');
    }
  }

}