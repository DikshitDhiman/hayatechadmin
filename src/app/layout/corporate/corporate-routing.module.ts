import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddCorporateComponent } from './add-corporate/add-corporate.component';
import { ListCorporateComponent } from './list-corporate/list-corporate.component';


const routes: Routes = [{
  path: '',
  children: [
    {
      path: '',
      component: ListCorporateComponent
    },
    {
      path: 'add-company',
      component: AddCorporateComponent
    },
    {
      path: 'edit-company/:id',
      component: AddCorporateComponent
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CorporateRoutingModule { }
