import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PaginatorModule } from 'primeng/paginator';
import { MultiSelectModule } from 'primeng/multiselect';

import { SharedModule } from '../../shared/shared.module';
import { CorporateRoutingModule } from './corporate-routing.module';
import { ListCorporateComponent } from './list-corporate/list-corporate.component';
import { AddCorporateComponent } from './add-corporate/add-corporate.component';
import { PreloaderModule } from '../../shared/components/preloader/preloader.module';

@NgModule({
  declarations: [
    ListCorporateComponent,
    AddCorporateComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    CorporateRoutingModule,
    MultiSelectModule,
    PaginatorModule,
    SharedModule,
    PreloaderModule
  ]
})
export class CorporateModule { }