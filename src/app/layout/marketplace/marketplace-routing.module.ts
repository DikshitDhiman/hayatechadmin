import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListMarketplaceComponent } from './list-marketplace/list-marketplace.component';
import { AddMarketplaceComponent } from './add-marketplace/add-marketplace.component';

const routes: Routes = [{
  path: '',
  children: [
    {
      path: '',
      component: ListMarketplaceComponent
    },
    {
      path: 'add-marketplace',
      component: AddMarketplaceComponent
    },
    {
      path: 'edit-marketplace/:id',
      component: AddMarketplaceComponent
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MarketplaceRoutingModule { }
