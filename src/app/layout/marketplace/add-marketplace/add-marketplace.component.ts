import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { ImageCroppedEvent } from 'ngx-image-cropper';
import { AppServicesService } from './../../../shared/service/app-services.service';
import { environment } from './../../../../environments/environment';
import { constants } from './../../../constants/const';
@Component({
  selector: 'app-add-marketplace',
  templateUrl: './add-marketplace.component.html',
  styleUrls: ['./add-marketplace.component.css']
})
export class AddMarketplaceComponent implements OnInit {
  imageChangedEvent: any = '';
  croppedImage: any = '';
  addMarketplaceForm: FormGroup;
  isLoading: Boolean = false;
  currentUser: any;
  categories: any = [];
  vendors: any = [];
  isEdit: Boolean = false;
  isSubmitted: Boolean = false;
  isSelectedImg: Boolean = false;
  isImageEdit: Boolean = false;
  productId: any;
  vendorId: any;
  siteURL: String =  environment.siteURL;
  messageHandler: any = constants.constMessages;

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private apiService: AppServicesService,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    this.currentUser = JSON.parse(localStorage.getItem('user'));
    const reqData = {
      role: this.currentUser.role,
      roleId: this.currentUser._id,
      page: 0,
      skip: 0
    };
    this.createForm();
    this.getCategories(reqData);
    if (this.currentUser.role === 'admin') {
      this.getVendors(reqData);
    }

    this.route.paramMap.subscribe(paramsData => {
      if (paramsData.get('id')) {
        this.isEdit = true;
        this.productId = paramsData.get('id');
        this.getProducts();
        this.addMarketplaceForm.controls['vendorId'].disable();
      } else {
        this.isEdit = false;
      }
    });
  }

  createForm() {
    this.addMarketplaceForm = this.fb.group({
      name: ['', [Validators.required, Validators.pattern('[a-zA-Z][a-zA-Z ]+[a-zA-Z]$')]],
      rewardId: ['', Validators.required],
      quantity: ['', Validators.required],
      shortDesc: ['', Validators.required],
      description: [''],
      token: ['', Validators.required],
      vendorId: [''],
      base64Data: ['', Validators.required]
    });
  }


  getProducts() {
    this.apiService.get('/api/v1/product/', this.productId).subscribe((response: any) => {
      if (response && response.status === 200 && response.data) {
        this.vendorId = response.data.vendorId['_id'];
        this.addMarketplaceForm.setValue({
          name: response.data.name,
          quantity: response.data.quantity,
          shortDesc: response.data.shortDesc,
          description: response.data.description,
          token: response.data.token,
          vendorId: (response.data.vendorId['_id']) ? response.data.vendorId['_id'] : '',
          rewardId: (response.data.rewardId['_id']) ? response.data.rewardId['_id'] : '',
          base64Data: this.siteURL + response.data.image
        });
        setTimeout( () => {
          this.addMarketplaceForm.controls['description'].setValue(response.data.description);
        }, 1000);
      }
    }, (errorResult) => {
      this.errorHandling(errorResult);
    });
  }

  getCategories(data) {
    this.apiService.post('/api/v1/categories', data).subscribe((response: any) => {
      if (response.status === 200 && response.data) {
        this.categories = response.data;
        if (this.categories.data[0]) {
            this.addMarketplaceForm.controls.rewardId.setValue(this.categories.data[0]._id);
        }
      } else {
        this.toastr.error(this.messageHandler[response.message], 'Error');
      }
    }, (errorResult) => {
      this.errorHandling(errorResult);
    });
  }

  getVendors(data) {
    this.apiService.post('/api/v1/vendors/', data).subscribe( (response: any) => {
      if (response.data && response.status === 200) {
        this.vendors = response.data.data;
        if (this.vendors[0]) {
          this.addMarketplaceForm.controls.vendorId.setValue(this.vendors[0]._id);
        }
      } else {
        this.toastr.error(this.messageHandler[response.message], 'Error');
      }
    }, (errorResult) => {
      this.errorHandling(errorResult);
    });
  }

  keyPress(event: any) {
    const pattern = /[0-9\+\-\ ]/;
    const inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode !== 3 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }

  resetForm() {
    this.addMarketplaceForm = this.fb.group({});
    this.createForm();
  }

  onUploadChange(event: any): void {
    this.imageChangedEvent = event;
  }

  imageCropped(event: ImageCroppedEvent) {
    this.croppedImage = event.base64;

    if (this.croppedImage) {
      this.isSelectedImg = true;
      this.isImageEdit = true;
      this.addMarketplaceForm.controls.base64Data.setValue(this.croppedImage)
    }
  }

  /* onUploadChange(evt: any) {
    const file = evt.target.files[0];
    if (file) {
      const reader = new FileReader();
      reader.onload = this.handleReaderLoaded.bind(this);
      reader.readAsBinaryString(file);
      this.isSelectedImg = true;
    }
  } */

  handleReaderLoaded(e) {
    this.isImageEdit = true;
    this.addMarketplaceForm.controls.base64Data.setValue('data:image/png;base64,' + btoa(e.target.result));
  }

  submitForm() {
    this.isSubmitted = true;
    
    if (this.currentUser.role === 'admin') {
      this.addMarketplaceForm.value.adminId = this.currentUser._id;
      this.addMarketplaceForm.value.vendorId = this.addMarketplaceForm.value.vendorId;
      this.addMarketplaceForm.value.addedBy = 'admin';
    } else {
      this.addMarketplaceForm.value.vendorId = this.currentUser._id;
    }
    if (this.isEdit) {
      this.addMarketplaceForm.value.productId = this.productId;
      this.addMarketplaceForm.value.vendorId = this.vendorId;
    }
    if (this.isImageEdit) {
      this.addMarketplaceForm.value.base64Data = this.addMarketplaceForm.value.base64Data;
    } else {
      this.addMarketplaceForm.value.base64Data = '';
    }
    this.apiService.post('/api/v1/product/add', this.addMarketplaceForm.value).subscribe( (response: any) => {
      if (response.data && response.status === 200) {
        this.toastr.success(this.messageHandler[response.message], 'Success');
        this.router.navigate(['/admin/marketplace/']);
        this.addMarketplaceForm.reset();
      } else {
        this.toastr.error(this.messageHandler[response.message], 'Error');
      }
    }, (errorResult) => {
      this.errorHandling(errorResult);
    });
  }

  errorHandling(error: any) {
    try {
      this.isLoading = false;
      const errorObj = error ? JSON.parse(error) : '';
      this.toastr.error(errorObj.message, 'Error');
    } catch (error) {
      this.toastr.error(error.message, 'Error');
    }
  }
}
