import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { ToastrService } from 'ngx-toastr';
import Swal from 'sweetalert2';
import { fromEvent } from 'rxjs';
import { debounceTime, map } from 'rxjs/operators';
import { AppServicesService } from './../../../shared/service/app-services.service';
import { environment } from './../../../../environments/environment';
import { constants } from './../../../constants/const';

@Component({
  selector: 'app-list-marketplace',
  templateUrl: './list-marketplace.component.html',
  styleUrls: ['./list-marketplace.component.css']
})
export class ListMarketplaceComponent implements OnInit {

  data: any[];
  marketPlace: any = [];
  search: String;
  page = 1;
  searchQuery: String = '';
  isLoading: Boolean = false;
  siteURL: String = environment.siteURL;
  currentUser: any;
  itemsPerPage: any = constants.recordsPageLimit;
  totalRecords: any;
  openToggle: Boolean = false;
  sort: String = 'name';
  actions: Array<any>;
  status: Array<any>;
  messageHandler: any = constants.constMessages;
  items = [];
  column: Array<any> = [
    { id: 'image', name: 'Image', sorting: false, type: 'image', display: 'left' },
    { id: 'name', name: 'Name', sorting: true, type: 'string', display: 'left', order: 'neutral' },
    { id: 'rewardId', child: 'name', name: 'Reward Category', sorting: false, type: 'object', display: 'left', order: 'neutral' },
    { id: 'vendorId', child: 'companyName', name: 'Vendor', sorting: false, type: 'object', display: 'left', order: 'neutral' },
    { id: 'shortDesc', name: 'Discount Summary', sorting: false, type: 'string', display: 'left', order: 'neutral' },
    { id: 'quantity', name: 'Quantity', sorting: false, type: 'number', display: 'left', order: 'neutral' },
    { id: 'token', name: 'Token', sorting: false, type: 'number', display: 'left', order: 'neutral' },
    { name: 'Status', type: 'status', sorting: false, display: 'center' },
    { name: 'Action', sorting: false, type: 'string', display: 'center' }
  ];
  pageLimit: any = constants.recordsPageLimit;
  requestData: any;

  constructor(
    private router: Router,
    private apiService: AppServicesService,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    this.currentUser = JSON.parse(localStorage.getItem('user'));

    this.requestData = {
      role: this.currentUser.role,
      roleId: this.currentUser._id,
      page: 0,
      pageLimit: this.pageLimit,
      skip: 0
    };
    this.getMarketplace(this.requestData);

    this.status = [{
      action: (item) => this.changeStatus(item['_id'], item['is_active'])
    }];

    this.actions = [{
      parentClass: 'edit',
      action: (item) => this.editItem(item['_id']),
      title: 'Edit',
      childClass: 'icofont-ui-edit'
    }, {
      parentClass: 'delete',
      action: (item) => this.deleteMarketplace(item['_id']),
      title: 'Delete',
      childClass: 'icofont-ui-delete'
    }];


    setTimeout(() => {
      this.keyUpSearch();
    }, 2000);
  }

  keyUpSearch() {
    const searchBox = document.getElementById('searchInputForMarketplaces');
    if(searchBox){
      const keyup$ = fromEvent(searchBox, 'keyup');
      keyup$.pipe(
        map((i: any) => i.currentTarget.value),
        debounceTime(1000)
      ).subscribe(searchQuery => {
        this.searchMarketplaces(searchQuery);
      });
    }
  }

  searchMarketplaces(searchQuery) {
    this.searchQuery = searchQuery;
    this.requestData = {
      role: this.currentUser.role,
      roleId: this.currentUser._id,
      searchQuery: searchQuery,
      page: 0,
      pageLimit: this.pageLimit,
      skip: 0
    };
    this.getMarketplace(this.requestData);
  }


  getMarketplace(data) {
    this.apiService.post('/api/v1/products', data).subscribe((response: any) => {
      if (response.status === 200 && response.data) {
        this.marketPlace = response.data;
      } else {
        this.toastr.error(this.messageHandler[response.message], 'Error');
      }
    }, (errorResult) => {
      this.errorHandling(errorResult);
    });
  }


  sortData(field, order) {
    this.column = this.column.map(x => field === x['id'] ? { ...x, 'order': order } : x);
    if (order === 'asc') {
      this.marketPlace.data = this.marketPlace.data.sort((a, b) => a[field].localeCompare(b[field]))
    } else {
      this.marketPlace.data = this.marketPlace.data.sort((a, b) => b[field].localeCompare(a[field]))
    }
  }


  paginate(event) {
    const reqData = {
      role: this.currentUser.role,
      roleId: this.currentUser._id,
      searchQuery: this.searchQuery,
      page: event.page,
      pageLimit: this.pageLimit, 
      pageCount: event.pageCount, 
      skip: 0
    };
    this.getMarketplace(reqData);
  }

  editItem(id) {
    this.router.navigate([`/marketplace/edit-marketplace/${id}`]);
  }

  changeStatus(id, status) {
    status = status ? false : true;
    const reqData = {
      id: id,
      status: status
    };
    this.apiService.post('/api/v1/product/change_status', reqData).subscribe((response: any) => {
      if (response.status === 200) {
        this.toastr.success('Status changed successfully', 'Success');
        this.getMarketplace(this.requestData);
      } else {
        this.toastr.error(this.messageHandler[response.message], 'Error');
      }
    });
  }

  deleteMarketplace(id: any) {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        this.apiService.delete('/api/v1/product/delete/', id).subscribe((response: any) => {
          if (response.status === 200) {
            this.toastr.success(this.messageHandler[response.message], 'Success');
            this.getMarketplace(this.requestData);
          } else {
            this.toastr.error(this.messageHandler[response.message], 'Error');
          }
        }, (errorResult) => {
          this.errorHandling(errorResult);
        });
      }
    });
  }


  switchToggle() {
    this.openToggle = !this.openToggle;
  }

  errorHandling(error) {
    try {
      this.isLoading = false;
      const errorObj = error ? JSON.parse(error) : '';
      this.toastr.error(errorObj.message, 'Error');
    } catch (error) {
      this.toastr.error(error.message, 'Error');
    }
  }
}