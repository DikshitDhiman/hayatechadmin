import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SanitizeTextModule } from './../../shared/pipes/sanitize-text.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PaginatorModule } from 'primeng/paginator';
import { CKEditorModule } from 'ngx-ckeditor';
import { ImageCropperModule } from 'ngx-image-cropper';
import { SharedModule } from '../../shared/shared.module';
import { MarketplaceRoutingModule } from './marketplace-routing.module';
import { AddMarketplaceComponent } from './add-marketplace/add-marketplace.component';
import { ListMarketplaceComponent } from './list-marketplace/list-marketplace.component';

@NgModule({
  declarations: [AddMarketplaceComponent, ListMarketplaceComponent],
  imports: [
    CommonModule,
    MarketplaceRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    PaginatorModule,
    CKEditorModule,
    SanitizeTextModule,
    SharedModule,
    ImageCropperModule
  ]
})
export class MarketplaceModule { }
