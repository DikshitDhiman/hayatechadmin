import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Ng2GoogleChartsModule } from 'ng2-google-charts';
import { ChartsModule } from 'ng2-charts';
import { ChartistModule } from 'ng-chartist';
import { CarouselModule } from 'ngx-owl-carousel-o';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { GeneralComponent } from './general/general.component';
import { ChartComponent } from './chart/chart.component';
import { CountToModule } from 'angular-count-to';

import { WidgetsRoutingModule } from './widgets-routing.module';

@NgModule({
  declarations: [GeneralComponent, ChartComponent],
  imports: [
    CommonModule,
    CountToModule,
    FormsModule,
    ReactiveFormsModule,
    Ng2GoogleChartsModule,
    ChartistModule,
    CarouselModule,
    ChartsModule,
    NgbModule,
    WidgetsRoutingModule
  ]
})
export class WidgetsModule { }
