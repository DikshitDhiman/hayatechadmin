import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';

import { ToastrService } from 'ngx-toastr';
import { CookieService } from 'ngx-cookie-service';

import { AppServicesService } from './../../shared/service/app-services.service';

type UserFields = 'email' | 'password';
type FormErrors = { [u in UserFields]: string };

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {



  public loginForm: FormGroup;
  public formErrors: FormErrors = {
    'email': '',
    'password': '',
  };
  public errorMessage: any;
  public isLoading: Boolean = false;
  private _sessionId: string;

  constructor(
    public fb: FormBuilder,
    private route: Router,
    private apiService: AppServicesService,
    private toastr: ToastrService,
    private cookieService: CookieService) {
  }

  ngOnInit() {
    this.createForm();
  }

  createForm() {
    this.loginForm = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required]
    });
  }
  // Simple Login
  login(value: any) {
    const reqData = {
      email: this.loginForm.value.email,
      password: this.loginForm.value.password,
      user_type: 'backend'
    };
    this.apiService.post('/api/auth/login', reqData).subscribe((response: any) => {
      if (response.status === 200) {
        const userData = response.data;
        userData['emailVerified'] = true;
        localStorage.setItem('user', JSON.stringify(userData));
        this.route.navigateByUrl('/admin');
        this.loginForm.reset();
      } else {
        localStorage.setItem('user', null);
        JSON.parse(localStorage.getItem('user'));
        this.toastr.error('Something went wrong', 'Error');
      }
    }, (errorResult) => {
      this.errorHandling(errorResult);
    });
    // this.authService.SignIn(this.loginForm.value['email'], this.loginForm.value['password']);
  }

  errorHandling(error) {
    try {
      this.isLoading = false;
      const errorObj = error ? JSON.parse(error) : '';
      this.toastr.error(errorObj.message, 'Error');
    } catch (error) {
      this.toastr.error(error.message, 'Error');
    }
  }

}
